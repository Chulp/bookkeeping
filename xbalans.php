<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/
$module_name 	= 'xbalans';
$version		= '20240222';
$project 		= "Resultaat verwerking"; 
$main_file 		= "booking";
$sub_file 		= "schema";
$default_template = '/balans.lte';

/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffb::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* 4 file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref  [ 98 ] = LOAD_DBBASE . "_".$sub_file;

/* 5 settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "group" );

/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* 9 specific default values */
$oFC->page_content [ 'DATEHIGH' ] = ( date( "Y", time() ) ) . "-12-31";
$oFC->page_content [ 'DATELOW' ] = date( "Y-m-d", mktime(0, 0, 0, date("m")-6, '01', date("Y")));
$oFC->page_content [ 'updatemarker' ] = true; // wijzigen mogelijk
$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = false;
$oFC->page_content [ 'REFERENCE_ACTIVE2' ] = false;
$oFC->page_content [ 'KOPREGELS' ] = '';

/* 8 Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 get saved values */ 
$oFC->gsm_memorySaved ( ); 

if ($oFC->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( "post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC , $selection ) , __LINE__ . __FUNCTION__ ); /* debug */

/* default periode corrected by input */
$oFC->page_content [ 'DATEHIGH' ] = ( date( "Y", time() ) ) . "-12-31";
$oFC->page_content [ 'DATELOW' ] = date( "Y-m-d", mktime(0, 0, 0, date("m")-6, '01', date("Y")));

// if ( LOAD_MODE == "x" ) 
	$oFC->page_content [ 'DATELOW' ] = ( date ( "Y", $oFC->gsm_preloadDataB ('b{OLDEST}') ) ) . "-01-01";

$oFC->page_content [ 'SELECT_VAN' ] = ( $oFC->memory [ 2 ] > 0 ) ? $oFC->memory [ 2 ] : date ( "Y", time ( ) )."-01-01";
$oFC->page_content [ 'SELECT_TOT' ] = date ( "Y", strtotime ( $oFC->page_content [ 'SELECT_VAN' ] ) ) . "-12-31";
if  ( isset ( $_POST [ 'gsmc_start_date' ] ) ) 
		$oFC->page_content [ 'SELECT_VAN' ] = $oFC->gsm_sanitizeStringD ( $_POST [ 'gsmc_start_date' ], 'y{' . date ( "Y", time() ) . '-01-01;2000-01-01;' . $oFC->page_content [ 'DATEHIGH' ] . '}'); 
		$oFC->page_content [ 'SELECT_TOT' ] = date ( "Y", strtotime ( $oFC->page_content [ 'SELECT_VAN' ] ) ) . "-12-31";
	
if  ( isset ( $_POST [ 'gsmc_end_date' ] ) ) 
		$oFC->page_content [ 'SELECT_TOT' ] = $oFC->gsm_sanitizeStringD ( $_POST [ 'gsmc_end_date' ], 'y{' . date ( "Y", time() ) . '-12-31;' . $oFC->page_content [ 'SELECT_VAN' ] .';' . $oFC->page_content [ 'DATEHIGH' ] . '}'); 

$oFC->page_content [ 'SELECT_PDF' ] = ( isset ( $_POST [ 'gsmc_pdf' ] ) ) ? true : false;

/* history period */ 
if ( date ( "Y", strtotime ( $oFC->page_content [ 'SELECT_VAN' ] ) ) <  date ( "Y", strtotime ( $oFC->page_content [ 'DATELOW' ] ) ) ) {
	/* go to history file  based on entry of start date period */
	$localHulpC = date ( "Y", strtotime ( $oFC->page_content [ 'SELECT_VAN' ] ) );
	$check_query = "SHOW TABLE STATUS LIKE '" . LOAD_DBBASE . "_" . $localHulpC . "_" . $main_file . "'";
	$results = array(); 
	$database->execute_query( 
		$check_query, 
		true, 
		$results);
		
/* debug * / $debughulp = array ($results ) ; Gsm_debug ( $debughulp , __LINE__ . __FUNCTION__ ); /* debug */ 

	if ( count($results) > 0) { 
		$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_" . $localHulpC . "_" . $main_file;
		$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" . $localHulpC . "_" . $sub_file;
		$oFC->page_content [ 'updatemarker' ] = false; 
	} else {
		$localHulpC = date ( "Y", strtotime ( $oFC->page_content [ 'DATE' ] ) );
	}
	$oFC->page_content [ 'SELECT_VAN' ] = $localHulpC . "-01-01";
	$oFC->page_content [ 'SELECT_TOT' ] = $localHulpC . "-12-31";
	$oFC->page_content [ 'DATELOW' ] = $oFC->page_content [ 'SELECT_VAN' ];
	$oFC->page_content [ 'DATEHIGH' ] = $oFC->page_content [ 'SELECT_TOT' ];

}

$oFC->page_content [ 'SELECT_LIST' ] = ( isset ( $_POST [ 'gsmc_overzicht' ] ) && $_POST [ 'gsmc_overzicht' ] == 1 ) ? 'R' : 'B';
$oFC->page_content [ 'SELECT_DET' ] = ( isset ( $_POST [ 'gsmc_detail' ] ) ) ? true : false;
$oFC->memory[1] = $oFC->page_content [ "SELECT_LIST" ];
$oFC->memory[2] = $oFC->page_content [ "SELECT_VAN" ];
$oFC->memory[3] = $oFC->page_content [ "SELECT_TOT" ];
$oFC->memory[4] = $oFC->page_content [ "SELECT_DET" ];
$oFC->page_content [ 'SELECT_REK' ] = $oFC->memory[5];
$budget =  false;


/* printing keywords */
$oFC->search_mysql = "";
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$selection = strtolower ( $selection );
	/* rekening nummer selection */
	if ( strlen ( $selection ) > 1 ) {
		$oFC->page_content [ 'SELECT_REK' ] =  $oFC->gsm_sanitizeStringV ( $selection, 'v{0;1000;9999}');
		/* debug */ Gsm_debug ( array ($selection, $oFC->page_content [ 'SELECT_REK' ] ), __LINE__ . __FUNCTION__ ); /* debug */
		$oFC->memory[5] = $oFC->page_content[ 'SELECT_REK' ];		
		$oFC->page_content [ 'PARAMETER' ] = $oFC->page_content[ 'SELECT_REK' ];
	}  
	/* selection */
	$selection = "";
}
/* selection */

/* which job to do */
if ( isset ( $_POST [ 'command' ] ) ) {
	switch ( $_POST [ 'command' ] ) {
		case "Save":
			if ($oFC->sips) {
				$oFC->description .= date ( "H:i:s " ) . __LINE__ . $oFC->language [ 'TXT_ERROR_SIPS' ]. NL;
				break;
			}
			if ( !isset ( $first ) ) { // bijlage naam
				$save_project = ( isset ( $_POST [ 'edit_project' ] ) 
					&& strlen ( $_POST [ 'edit_project' ] ) > 2 ) 
						? $_POST [ 'edit_project' ] 
						: "bijl";
				$save_recid = ( isset ( $_POST [ 'edit_recid' ] ) ) 
					? $_POST [ 'edit_recid' ] 
					: "";
				if (isset ( $_POST [ 'edit_name' ] ) ) {
					$temp = explode ( " ", $_POST [ 'edit_name' ] );
					$save_name = (isset ( $temp [ 0 ] ) 
						&& strlen ( $temp [ 0 ] ) > 2 ) 
							? $temp [ 0 ] 
							: $save_recid;
				}
				$first = true;
			}
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 1 ); //disconnect
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 2 ); //connect
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 3 ); // name/project ->calc
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 4 , $oFC->page_content['SELECT_VAN'], $oFC->page_content['SELECT_TOT']); // booking ->calx
			// attachment
			$oFC->description .= $oFC->gsm_uploadFile ( $oFC->setting [ 'mediadir' ] . "/current/" , "%s%s%s%s%s", $save_name, $save_recid, $save_project );
			$oFC->recid = $_POST [ 'edit_recid' ];
			if ($oFC->recid > 0)	$oFC->page_content[ "REFERENCE_ACTIVE2" ]= true;
			break;
		case "New":
			if ($oFC->sips) {
				$oFC->description .= date ( "H:i:s " ) . __LINE__ . $oFC->language [ 'TXT_ERROR_SIPS' ]. NL;
				break;
			}
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 8 ,  $oFC->page_content["SELECT_VAN"], $oFC->page_content["SELECT_TOT"] );
			
			$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = true;
			if ( isset ( $_POST [ 'edit_project' ] ) ) {
				$temp = explode ( " ", $_POST [ 'edit_project' ] );
				$save_project = ( isset ( $temp [ 0 ] ) 
					&& strlen ( $temp [ 0 ] ) > 2 ) 
						? $temp [ 0 ] 
						: "bijl";	
			}
			$save_recid = ( isset ( $oFC->recid ) ) 
				? $oFC->recid 
				: $_POST [ 'edit_recid' ] ?? "";
			if ( isset ( $_POST [ 'edit_name' ] ) ) {
				$temp = explode ( " ", $_POST [ 'edit_name' ] );
				$save_name = ( isset ( $temp [ 0 ] ) 
					&& strlen ( $temp [ 0 ] ) > 2 ) 
						? $temp [ 0 ] 
						: $save_recid;
			}
			if ( $oFC->recid > 0 ) $oFC->page_content [ "REFERENCE_ACTIVE2" ] = true;
/* debug * / Gsm_debug ( array ($save_name, $save_recid, $save_project, $oFC,  ), __LINE__ . __FUNCTION__ ); /* debug */
			$oFC->description .= $oFC->gsm_uploadFile ( $oFC->setting [ 'mediadir' ]."/current/", "%s%s%s%s%s", $save_name, $save_recid, $save_project );
			break;
		case "Select":
			$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = true;
			$oFC->page_content [ 'REFERENCE_ACTIVE2' ] = false;
			break;
		default:
			$oFC->description .= date ( "H:i:s " ) . __LINE__ ."  post: " . $_POST[ 'command' ] . NL;
			break;
	} 
} elseif ( isset( $_GET [ 'command' ] ) ) {
	switch ( $_GET [ 'command' ] ) {
		case "Select":
			if ( $oFC->recid > 0 ) $oFC->page_content [ "REFERENCE_ACTIVE2" ] = true;
			break;
		case "Rmfile":
			$LocalHulpC = sprintf ( "%s%s/current/%s" , LEPTON_PATH, $oFC->setting [ 'mediadir' ], $_GET[ 'file' ]);
			if ( file_exists ( $LocalHulpC ) ) {
				unlink ( $LocalHulpC );
				$oFC->description .= date ( "H:i:s " ) . __LINE__ . " Removed: " . $_GET[ 'file' ] . NL;
			}
			if ( $oFC->recid > 0) $oFC->page_content["REFERENCE_ACTIVE2"]= true;
			break;
		default:
			$oFC->description .= date ( "H:i:s " ) . __LINE__ ."  get: " . $_GET[ 'command' ] . NL;
			break;
	} 
} else {
	$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = false;
	$oFC->page_content [ 'REFERENCE_ACTIVE2' ] = false;
}

/* debug * /  Gsm_debug (array ( $oFC ), __LINE__ . __FUNCTION__ ); /* debug */

// ook als pdf ? bepaal filename
if ( $oFC->page_content['SELECT_PDF'] ) {
//	$oFC->pdf_filename = $oFC->gsm_sanitizeStrings ( sprintf('%s %s %s %s.pdf', 
	
	$oFC->setting ['pdf_filename']  = $oFC->gsm_sanitizeStrings ( sprintf('%s %s %s %s.pdf', 
		$oFC->setting [ 'owner' ], 
		$oFC->page_content['SELECT_TOT' ], 
		strtolower(str_replace('.', '', $oFC->setting [ 'droplet'] [ LANGUAGE. '2' ])), 
		($oFC->page_content [ 'SELECT_LIST' ] == 'B' ) ? "balans" : "resultaat"), 
		"s{FILE}");
	require_once ( $oFC->setting [ 'includes' ] . 'classes/' . 'pdf.inc' );
}

// preload data
$oFC->setting ['rekeningArray']= $oFC->gsm_preloadDataB ( "b{REKALL}" ); // preload data
$oFC->setting ['rekeningTypeArray'] = $oFC->gsm_preloadDataB ( "b{REKTYPE}" ); // preload data
		
/* debug * /  Gsm_debug (array ( $oFC->setting ['rekeningArray'], $oFC->setting ['rekeningTypeArray'] , $oFC ), __LINE__ . __FUNCTION__ ); /* debug */

// initialise pdf
$oFC->pdf_text = '';
$oFC->pdf_data = array( );
$TEMPLATE2 = '<tr %1$s><td>%2$s</td><td>%3$s</td><td>%4$s</td><td class="right aligned">%5$s</td><td class="right aligned">%6$s</td><td class="right aligned">%7$s</td></tr>';
$TEMPLATE3 = '<tr %1$s><td>%2$s</td><td>%3$s</td><td>%4$s</td><td align="right" colspan="3">%5$s</tr>';

switch ( $oFC->page_content [ 'SELECT_LIST' ] ) { 
	case 'R':
		$oFC->page_content['FORMULIER'] = sprintf('%s %s periode: %s - %s', 
			$oFC->language ['ACC_VENW'], 
			$oFC->setting [ 'droplet'] [ LANGUAGE. '2' ], 
			$oFC->page_content['SELECT_VAN' ], 
			$oFC->page_content['SELECT_TOT' ]);
		$oFC->page_content['KOPREGELS'] .= sprintf($TEMPLATE3, $oFC->language ['line_color'][4],
			"",
			"<strong>Resultaat</strong>" ,
			"<strong>" . $oFC->setting [ 'droplet'] [ LANGUAGE. '2' ] . "</strong>",
			sprintf ('<strong>Periode: %s - %s</strong>', 
				$oFC->page_content['SELECT_VAN' ], 
				$oFC->page_content['SELECT_TOT' ]));
		$oFC->page_content['KOPREGELS'] .= sprintf($TEMPLATE2, 
			$oFC->language ['line_color'][4], 
			"", 
			"rek", 
			'omschrijving', 
			'budget', 
			( $oFC->page_content [ 'SELECT_DET' ] ) ? 'cum': '', 
			( $oFC->page_content [ 'SELECT_DET' ] ) ? 'totaal' : 'bedrag');
		$oFC->page_content['DESCRIPTION'] .= $oFC->gsm_rekeningdisplayB ( 2, 
			$oFC->page_content [ 'SELECT_REK' ],
			$oFC->page_content [ 'SELECT_DET' ],
			$oFC->page_content [ 'SELECT_PDF' ], 
			$oFC->page_content [ 'SELECT_VAN' ], 
			$oFC->page_content [ 'SELECT_TOT' ], 
			$budget );
		$oFC->cols=array(15, 90, 1, 20, 20, 30);
		$oFC->pdf_header = array("Rek", 
			"Omschrijving", 
			'', 
			'Budget',  
			( $oFC->page_content [ 'SELECT_DET' ] ) ? 'Cum': '', 
			( $oFC->page_content [ 'SELECT_DET' ] ) ? 'Totaal': 'Bedrag' );
		break;
	default:
	case 'B':
		$oFC->page_content['FORMULIER']= sprintf('%s %s per %s', 
			$oFC->language ['ACC_BAL'], 
			$oFC->setting [ 'droplet'] [ LANGUAGE. '2' ], 
			$oFC->page_content['SELECT_TOT' ]);
		$oFC->page_content['KOPREGELS'] .= sprintf ( $TEMPLATE3,  
			$oFC->language ['line_color'][4],
			"",
			"<strong>Balans</strong>" ,
			"<strong>" . $oFC->setting [ 'droplet'] [ LANGUAGE. '2' ] . "</strong>",
			sprintf ('<strong>Per %s</strong>', 
				$oFC->page_content['SELECT_TOT' ]));
		$oFC->page_content['KOPREGELS'] .= sprintf ( $TEMPLATE2,  
			$oFC->language ['line_color'][4], 
			"", 
			"rek", 
			'omschrijving', 
			'', 
			( $oFC->page_content [ 'SELECT_DET' ] ) ? 'cum': '', 
			( $oFC->page_content [ 'SELECT_DET' ] ) ? 'totaal' : 'bedrag');
		$oFC->page_content [ 'DESCRIPTION' ] .= $oFC->gsm_rekeningdisplayB (1, 
			$oFC->page_content [ 'SELECT_REK' ],
			$oFC->page_content [ 'SELECT_DET' ],
			$oFC->page_content [ 'SELECT_PDF' ], 
			$oFC->page_content [ 'DATELOW' ],  
			$oFC->page_content [ 'SELECT_TOT' ], 
			$budget );
		$oFC->cols=array ( 15, 90, 1, 20, 20, 20);
		$oFC->pdf_header = array("Rek", 
			"Omschrijving", 
			'', 
			'', 
		( $oFC->page_content [ 'SELECT_DET' ] ) ? 'Cum': '', 
		( $oFC->page_content [ 'SELECT_DET' ] ) ? 'Totaal': 'Bedrag' );

		break;
}

if ( isset ( $oFC->setting ['pdf_filename']  ) ) { // add header and trailer and write pdf file
	$pdf = new PDF();
	global $owner;
	$owner = $oFC->setting [ 'owner' ];
	global $title;
	$title = $project ;
	$run = date( DATE_FORMAT, time() ) . " " . date( TIME_FORMAT, time() );
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->ChapterTitle( 1, $oFC->page_content ['FORMULIER'] ); 
	$oFC->pdf_text .= "\n\n" . $oFC->setting [ 'droplet' ] [ LANGUAGE . '0' ];
	$oFC->pdf_text .= "\n\n" . $oFC->setting [ 'pdf_filename' ] . "\n";
	$oFC->pdf_text .= $oFC->language [ 'pdf' ][0] . str_replace( "_", " ", $run ) . "\n";
	if ( strlen ( $oFC->page_content [ 'SELECT_REK' ] ) > 3 ) $oFC->pdf_text .= "\n" . "Selected Rekening: " . $oFC->page_content [ 'SELECT_REK' ];
	if ( isset ( $oFC->filter_options ) &&  strlen( $oFC->filter_options ) > 2 )  $oFC->pdf_text .= "\n" . "Options : " .$oFC->filter_options;

	if ($oFC->setting [ 'debug' ] == "yes" ){	
		$oFC->pdf_text .= sprintf ( "\n %s \n", $oFC->language [ 'pdf' ][ 3 ]) ;
		foreach ($oFC->version as $key => $value) $oFC->pdf_text .= $key . "_" . $value . "\n";
	}	
	// pdf output
	$pdf->DataTable( $oFC->pdf_header, $oFC->pdf_data, $oFC->cols );
	$pdf->ChapterBody( $oFC->pdf_text );
	$pdflink = sprintf ( "%s%s/%s/pdf/%s", LEPTON_PATH, MEDIA_DIRECTORY, LOAD_MODULE, $oFC->setting ['pdf_filename'] );
	$pdf->Output ( $pdflink, 'F' );
	$oFC->description .= sprintf( '%1$s %2$s <a target="_blank" href="%4$s"><img src="' . sprintf ( "%s/modules/%s%s/img/pdf_16.png", 
		LEPTON_URL, LOAD_MODULE, LOAD_SUFFIX ).'" alt="pdf document">%3$s</a>', date ( "H:i:s " ), __LINE__ , $oFC->setting ['pdf_filename'], 
			str_replace ( LEPTON_PATH, LEPTON_URL, $pdflink ) ).NL;
}

if ( $oFC->page_content["REFERENCE_ACTIVE2"]) {
/* debug * /  Gsm_debug (array ( $oFC->recid ), __LINE__ . __FUNCTION__ ); /* debug */
	$oFC->page_content["BOOKINGEN"] = $oFC->gsm_RekeningDisplayBookB ( 1, $oFC->recid);
	$oFC->page_content [ 'SELECTION' ] = $oFC->gsm_opmaakSel ( array ( 2, 8 ) );
}

/* the selection options */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'SELECTION' ] = "";
		$oFC->page_content [ 'TOEGIFT' ] = ""; 
		foreach ($oFC->language [ 'DUMMY' ] as $pay => $load ) $oFC->page_content [ 'TOEGIFT' ] .=  $load . NL; 
		break;
	default: 
		break;
} 

/* output processing */
/* memory save * /
$oFC->page_content [ 'MEMORY' ] = $oFC->gsm_memorySaved ( 3 ); /* end save values */

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
//		$template_name = '@'.LOAD_MODULE.LOAD_SUFFIX.'/back.lte';
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>