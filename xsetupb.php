<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

// start
$module_name 	= 'xsetupb';
$version		= '20240218';
$project		= "Onderhoud Booking module";
$main_file 		= "booking";
$sub_file 		= "schema";
$default_template = '/display.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffb::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* file references */
$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_" . $main_file;
$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" .$sub_file;

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "label");

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;


/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
			$oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}


// spif test
$_SESSION[ 'page_h' ] = $oFC->page_content[ 'HASH' ]; 

// get memory values
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" )  Gsm_debug (array ($_POST, $_GET ?? "", $oFC->page_content, $oFC , $selection ), __LINE__ . __FUNCTION__ );

/* selection */
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}

/* sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
		$oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

// some job to do
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		default:
			// escape route 
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		default:
			// escape route 
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} //$_GET[ 'command' ]
} else {
	/* so standard display / first run */
	/* debug * / 
	$oFC->file_ref [ 99 ] = str_replace ( "_go_" , "_go_2020_" , $oFC->file_ref [ 99 ]);
	$oFC->file_ref [ 98 ] = str_replace ( "_go_" , "_go_2020_" , $oFC->file_ref [ 98 ]);
	/* debug */
	
	/* first run */
	$oFC->page_content [ 'P1' ] = true;
	$oFC->page_content [ 'MODE' ] = 9;
	
	/* Default settings */
	$job = array ();
	
	if ( !isset ( $oFC->setting [ 'rekening' ] ) ) {
		$main_parameter = '5600';
		$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			'rekening', 
			$main_parameter);
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' rekening keyword setting added ' . $main_parameter . NL;
		$oFC->setting [ 'remove' ] = $main_parameter;
	}	
	$oFC->setting [ 'rekening' ]	= 5600;	
	
	if (!isset ( $oFC->setting [ 'label' ] ) ) {
		$main_parameter = 'Intern';
		$job [] = sprintf ( "INSERT INTO  `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('label', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			$main_parameter, 
			'Intern');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' label setting ' . $main_parameter . NL;
		$oFC->setting [ 'label' ] = 'Intern';
	}
	
	if (!isset ( $oFC->setting [ 'mediadir' ] ) ) {
		$main_parameter = 'mediadir|gsmoffb';
		$job [] = sprintf ( "INSERT INTO  `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			$main_parameter, 
			'/media/booking');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'mediadir' . NL;
		$oFC->setting [ 'mediadir' ] = '/media/booking';
	}
	
	if (!isset ( $oFC->setting [ 'datadir' ] ) ){
		$main_parameter = 'datadir|gsmoffb';
		$job [] = sprintf ( "INSERT INTO   `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			$main_parameter,  
			'/current' );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'datadir' . NL;
		$oFC->setting [ 'datadir' ] = '/current';
	}

	
	if ( !isset ( $oFC->setting [ 'zoek' ] [ $main_file ] ) ) {
		$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('zoek', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			$main_file, 
			'|type|ref|');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema  added  ' . $main_file . NL;
		$oFC->setting [ 'zoek' ] [ $main_file ] = "|type|ref|";
	}
	
	if ( !isset ( $oFC->setting [ 'zoek' ] [ $sub_file ] ) ) {
		$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('zoek', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			$sub_file, 
			'|type|ref|');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema  added  ' . $sub_file . NL;
		$oFC->setting [ 'zoek' ] [ $sub_file ] = "|type|ref|";
	}
	/* create databases 1 */
	
	$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 99 ] );
	/* check which fields are present in the main file */	
	$result = array ( );
	$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 99 ] ),
		true, 
		$result );
	
	/* add fields not present / change fields */
	$localHulpA = array();
	foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];

	if ( isset ( $localHulpA[ 'type' ] ) && strstr ( $localHulpA[ 'type' ], "var" )) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `type` `type` int(11) NOT NULL DEFAULT '0'", $oFC->file_ref [ 99 ] );
	}
	if ( isset ( $localHulpA[ 'boekstuk' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `boekstuk` `content_short` varchar(255) NOT NULL DEFAULT ''", $oFC->file_ref [ 99 ] );
		$localHulpA[ 'content_short' ] = true;
	}
	if ( isset ( $localHulpA[ 'project' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `project` `ref` varchar(63) NOT NULL DEFAULT ''", $oFC->file_ref [ 99 ] );
		$localHulpA[ 'content_long' ] = true;
	}
	if ( !isset ( $localHulpA[ 'amt_tegen2' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt_tegen2` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 99 ] );
	}	
	if ( !isset ( $localHulpA[ 'tegen2_rekening' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `tegen2_rekening` decimal(8,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 99 ] );
	}	
	if ( !isset ( $localHulpA[ 'tegen2_id' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `tegen2_id` int(11) NOT NULL DEFAULT '0' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'amt_tegen1' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt_tegen1` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 99 ] );
	}	
	if ( !isset ( $localHulpA[ 'tegen1_rekening' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `tegen1_rekening` decimal(8,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 99 ] );
	}	
	if ( !isset ( $localHulpA[ 'tegen1_id' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `tegen1_id` int(11) NOT NULL DEFAULT '0' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'amt_debet' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt_debet` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'debet_rekening' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `debet_rekening` decimal(8,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'debet_id' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `debet_id` int(11) NOT NULL DEFAULT '0' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'booking_date' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `booking_date` date NOT NULL DEFAULT '0000-00-00' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	/* achtual upgrade */
	if ( isset ( $job ) && count( $job ) > 0 ) {
		
		foreach( $job as $key => $query ) {
			$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $query . NL;
			$database->simple_query ( $query );
		} 
		$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' Upgrade function' . NL; 
	}
	/* upgraded */
	
	/* create databases  2 */
	$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 98 ] );

	/* check which fields are present in the main file */	
	$result = array ( );
	$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 98 ] ),
		true, 
		$result );
	/* debug * / Gsm_debug ( $result, __LINE__ . __FUNCTION__ ); 
	
	/* add fields not present / change fields */
	$localHulpA = array();
	foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];
	$job = array ();
	if ( isset ( $localHulpA[ 'rekening_type' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `rekening_type` `type` char(3) NOT NULL DEFAULT '2B'", $oFC->file_ref [ 98 ] );
	}
	if ( isset ( $localHulpA[ 'type' ] ) && strstr ( $localHulpA[ 'type' ], "var" )) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `type` `type` char(3) NOT NULL DEFAULT '2B'", $oFC->file_ref [ 98 ] );
	}
	if ( isset ( $localHulpA[ 'rekeningnummer' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `rekeningnummer` `ref` decimal(8,2) NOT NULL DEFAULT '1000.00'", $oFC->file_ref [ 98 ] );
	}
	if ( isset ( $localHulpA[ 'ref' ] ) && strstr ( $localHulpA[ 'ref' ], "char" ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `ref` `ref` decimal(8,2) NOT NULL DEFAULT '1000.00'", $oFC->file_ref [ 98 ] );
	}
	if ( !isset ( $localHulpA[ 'content_short' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_short` varchar(255) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 98 ] );
	}
	if ( !isset ( $localHulpA[ 'amtbudget_b' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amtbudget_b` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 98 ] );
	}
	if ( !isset ( $localHulpA[ 'amtbudget_a' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amtbudget_a` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 98 ] );
	}
	if ( !isset ( $localHulpA[ 'date_balans' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `date_balans` date NOT NULL DEFAULT '2022-01-01' AFTER `name`", $oFC->file_ref [ 98 ] );
	}
	if ( !isset ( $localHulpA[ 'amtbalans' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `amtbalans` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 98 ] );
	}
	/* achtual upgrade */
	if ( isset ( $job ) && count( $job ) > 0 ) {
		
		foreach( $job as $key => $query ) {
			$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $query . NL;
			$database->simple_query ( $query );
		} 
		$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' Upgrade function' . NL; 
	}
	/* upgraded */
}

/* Additional functions */
if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) require_once ( $place[ 'includes'] . 'repair.php' );
 
/* output processing */
/* memory save * /
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( ); 

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
//		$template_name = '@'.LOAD_MODULE.LOAD_SUFFIX.'/back.lte';
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>