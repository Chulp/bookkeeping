<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php


/* change history
 * 20230704 for Lepton 7.0 
 */

if ( !file_exists ( LEPTON_PATH.MEDIA_DIRECTORY.'/booking') ) 
	LEPTON_core::make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/booking'); 

if ( !file_exists ( LEPTON_PATH. MEDIA_DIRECTORY.'/gsmoff' ) ) 
	LEPTON_core::make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/gsmoff'); 

if ( !file_exists ( LEPTON_PATH. MEDIA_DIRECTORY.'/gsmoff/pdf' ) ) 
	LEPTON_core::make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/gsmoff/pdf'); 

/* booking
 
$table_fields=" 
	`id` int(11) NOT NULL,
	`name` varchar(255) NOT NULL DEFAULT '',
	`zoek` varchar(255) NOT NULL DEFAULT '',
	`booking_date` date NOT NULL DEFAULT '0000-00-00',
	`debet_id` int(11) NOT NULL DEFAULT '0',
	`debet_rekening` decimal(8,2) NOT NULL DEFAULT '0.00',
	`amt_debet` decimal(9,2) NOT NULL DEFAULT '0.00',
	`tegen1_id` int(11) NOT NULL DEFAULT '0',
	`tegen1_rekening` decimal(8,2) NOT NULL DEFAULT '0.00',
	`amt_tegen1` decimal(9,2) NOT NULL DEFAULT '0.00',
	`tegen2_id` int(11) NOT NULL DEFAULT '0',
	`tegen2_rekening` decimal(8,2) NOT NULL DEFAULT '0.00',
	`amt_tegen2` decimal(9,2) NOT NULL,
	`project` varchar(12) NOT NULL DEFAULT '0',
	`id_root` int(11) NOT NULL DEFAULT '0',
	`boekstuk` varchar(255) NOT NULL DEFAULT '',
	`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)";

LEPTON_handle::install_table("mod_go_booking", $table_fields);
		
/* schema		

$table_fields=" 
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL default '',
	`zoek` varchar(255) NOT NULL default '',
	`rekeningnummer` decimal(8,2)  NOT NULL default '1000.00',
	`rekening_type` char(3) NOT NULL default '',
	`active` int(3) NOT NULL default '1',
	`omschrijving` varchar(255) NOT NULL default '',
	`amtbalans` decimal(9,2) NOT NULL default '0',
	`date_balans` date NOT NULL default '0000-00-00',
	`amtbudget_a` decimal(9,2) NOT NULL default '0',
	`amtbudget_b` decimal(9,2) NOT NULL default '0',
	`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)";

LEPTON_handle::install_table("mod_go_schema", $table_fields);
		
/* project
 
$table_fields=" 
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(12) NOT NULL default '',
	`zoek` varchar(255) NOT NULL default '',
	`active` int(3) NOT NULL default '1',
	`omschrijving` varchar(255) NOT NULL default '',
	`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)";

LEPTON_handle::install_table("mod_go_project", $table_fields);

*/
?>