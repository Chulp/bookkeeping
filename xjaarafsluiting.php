<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

 
/* module id*/ 
$module_name 	= 'xjaarafsluiting';
$version		= '20240222';
$project		= "Jaar Afsluiting";
$main_file 		= "booking";
$sub_file 		= "schema";
$default_template = '/jaarafsl.lte';

/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffb::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* 4 file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref  [ 98 ] = LOAD_DBBASE . "_".$sub_file;

/* 5 settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity" );

/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* 8 Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 get saved values */ 
$oFC->gsm_memorySaved ( ); 

if ($oFC->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( "post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC , $selection ) , __LINE__ . __FUNCTION__ );

// selection
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= "WHERE `zoek` LIKE '" . $help . "'";
	$oFC->page_content [ 'PARAMETER' ] = $selection;
	$oFC->selection = $selection;
	$oFC->page_content [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content[ 'PARAMETER' ] );
}

/* selection search*/
$oFC->search_mysql = "";
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `zoek` LIKE '" . $help . "'";
	$oFC->page_content [ 'PARAMETER' ] = $selection;
	$oFC->page_content [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content[ 'PARAMETER' ] );
}  
$selection = "";
/* end selection*/

/* sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= $oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

// some job to do
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		default:
			$oFC->description .= __LINE__ . " post: " . $_POST[ 'command' ] . NL;
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	}
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		case "remove":
			$files = array();
			$files[ ] = $oFC->file_ref[98]; // rekening
			$files[ ] = $oFC->file_ref[99]; // booking
			$job = array( );
			$job[] = "DROP TABLE IF EXISTS `{filename}_roll`"; // verwijder oude roll back file
			$errors = array( );
			foreach ($files as $key=>$value) {
				foreach ($job as $jobkey=>$jobvalue) {
					$jobvalue1= str_replace ("{filename}", $value, $jobvalue);
					$database->simple_query( $jobvalue1);
				}
			}
			$oFC->description .= __LINE__ . " Rollback database if any removed".NL;
			break;
		case "resultaat":
			// creer archief sectie
			$dir_to = LEPTON_PATH . $oFC->setting [ 'mediadir' ]."/" . $oFC->setting [ 'owner' ].$_GET['date'];
			$oFC->gsm_existDir ( $dir_to , true);
			// create balans en resultaat files (pdf)
			$oFC->setting ['rekeningArray'] = $oFC->gsm_preloadDataB ( "b{REKALL}" ); // preload data
			$oFC->setting ['rekeningTypeArray'] = $oFC->gsm_preloadDataB ( "b{REKTYPE}" ); // preload data
			require_once( $place['includes'] . 'classes/pdf.inc' );
			$oFC->gsm_RekeningArchiveDocB (1, $_GET['date'], $project);
			$oFC->gsm_RekeningArchiveDocB (2, $_GET['date'], $project);
			$oFC->gsm_RekeningArchiveDocB (5, $_GET['date'], $project);
			$oFC->gsm_RekeningArchiveDocB (4, $_GET['date'], $project);
			$oFC->gsm_RekeningArchiveDocB (3, $_GET['date'], $project);
			if (abs($oFC->cal['cum_resultaat']) < 0.001) {
				$oFC->description .= __LINE__ . ": Documents created".NL;
			} else {
				$oFC->description .= __LINE__ . ": Resultaat niet nul: archiving requires result allocation ".NL;
			}
			$oFC->page_content ['RAPPORTAGE'] .= $oFC->gsm_scanDir ($dir_to, 3 );
			break;
		case "details":
			$dir_from = LEPTON_PATH . $oFC->setting [ 'mediadir' ];
			if ($_GET [ 'archive' ] == "yes" ) {
				$dir_from .= "/" . $oFC->setting [ 'owner' ].$_GET['date'] ;
				$oFC->page_content ['RAPPORTAGE'] .= "ARCHIEF van ". $_GET['date'] . NL . "<hr>" .NL ;
			} else {
				$dir_from .= "/current";
				$oFC->page_content ['RAPPORTAGE'] .= "DOCUMENTEN open jaren  " . NL . "<hr>" .NL ;
			}
			$oFC->page_content ['RAPPORTAGE'] .= $oFC->gsm_scanDir ( $dir_from, 3 );
			break;
		case "move"	: // not used
			// creer archief sectie
			$dir_to = LEPTON_PATH . $oFC->setting [ 'mediadir' ];
			if ( !file_exists( $dir_to . "/" ) ) mkdir( $dir_to, 0777 ); // create directory if not existing
			$dir_to .= "/" . $oFC->setting [ 'owner' ].$_GET['date'];
			if ( !file_exists( $dir_to . "/" ) ) mkdir( $dir_to, 0777 ); // create directory if not existing
			$oFC->description .= $oFC->moveAttachments (1, $_GET['date'], $project, $dir_to );
			$oFC->description .= __LINE__ . " Attachments moved to archive".NL;
			break;
		case "archive":
			// creer archief sectie
			$dir_to = LEPTON_PATH . $oFC->setting [ 'mediadir' ]."/" . $oFC->setting [ 'owner' ].$_GET['date'];
			$oFC->gsm_existDir ( $dir_to , true);
			// create balans en resultaat files (pdf)
			$oFC->setting ['rekeningArray'] = $oFC->gsm_preloadDataB ( "b{REKALL}" ); // preload data
			$oFC->setting ['rekeningTypeArray'] = $oFC->gsm_preloadDataB ( "b{REKTYPE}" ); // preload data
			require_once( $place['includes'] . 'classes/pdf.inc' );
			$oFC->gsm_RekeningArchiveDocB (1, $_GET['date'], $project);
			$oFC->gsm_RekeningArchiveDocB (2, $_GET['date'], $project);
			$oFC->gsm_RekeningArchiveDocB (3, $_GET['date'], $project);
			$oFC->gsm_RekeningArchiveDocB (4, $_GET['date'], $project);
			$oFC->gsm_RekeningArchiveDocB (5, $_GET['date'], $project);
			if (abs($oFC->cal['cum_resultaat']) < 0.001) {
				$oFC->description .= __LINE__ . ": Final documents created".NL;
				// create rollback files
				$files = array();
				$files[] = $oFC->file_ref[98]; // rekening
				$files[] = $oFC->file_ref[99]; // booking
				$job = array( );
				$job[] = "DROP TABLE IF EXISTS `{filename}_roll`"; // verwijder oude rollback
				$job[] = "RENAME TABLE `{filename}` TO `{filename}_roll`" ; // move current set to roll back
				$job[] = "CREATE TABLE `{filename}` LIKE `{filename}_roll`" ; // create current set
				$job[] = "INSERT `{filename}` SELECT * FROM `{filename}_roll`" ; // fill current set
				$errors = array( );
				foreach ($files as $key=>$value) {
					foreach ($job as $jobkey=>$jobvalue) {
						$jobvalue1= str_replace ("{filename}", $value, $jobvalue);
						$results = $database->simple_query( $jobvalue1);
					}
				}
				if ( isset($errors) && count( $errors ) > 0 ) $admin->print_error( implode( "<br />n", $errors ), 'javascript: history.go(-1);' );
				$oFC->description .= __LINE__ . ": Rollback database created".NL;
				// copy to archive location
				$files = array();
				$files[] = $oFC->file_ref[98]; // rekening
				$files[] = $oFC->file_ref[99]; // booking
				$job = array( );		
				$job[] = "DROP TABLE IF EXISTS `{filename2}`"; // verwijder oude achive file
				$job[] = "CREATE TABLE `{filename2}` LIKE `{filename}_roll`" ; // create archive set set
				$job[] = "INSERT `{filename2}` SELECT * FROM `{filename}_roll`" ; // fill archive set
				$errors = array( );
				foreach ($files as $key=>$value) {
					$value2=str_replace("go", "go_".$_GET['date'], $value);
					foreach ($job as $jobkey=>$jobvalue) {
						$jobvalue1= str_replace ("{filename}", $value, $jobvalue);
						$jobvalue2= str_replace ("{filename2}", $value2, $jobvalue1);
						$results = $database->simple_query( $jobvalue2);
					}
				}
				if ( isset($errors) && count( $errors ) > 0 ) $admin->print_error( implode( "<br />n", $errors ), 'javascript: history.go(-1);' );
				$oFC->description .= __LINE__ . ": Database copied to archive".NL;
				// remove recent data from archive
				$hulp = str_replace("go", "go_".$_GET['date'], $oFC->file_ref[99]);
				$query = "DELETE FROM `" . $hulp . "` WHERE `booking_date` > '" . $_GET['date'].'-12-31' . "'";
				$results = $database->simple_query($query);
				$oFC->description .= __LINE__ . ": Recent records removed from archive".NL;
				// update actual system
				$oFC->description .= $oFC->gsm_RekeningOpeningBalB (1, $_GET['date'], $project);
				if ($oFC->cal['cum_bal_update']) {
					$oFC->description .= __LINE__ . " Balans (opening) values updated".NL;
					// transfer attachments to archive
					$oFC->description .= $oFC->gsm_RekeningMoveAttB (1, $_GET['date'], $project );
					$oFC->description .= __LINE__ . " Attachments moved to archive".NL;
					$query = "DELETE FROM `" . $oFC->file_ref[99] . "` WHERE `booking_date` <= '" . $_GET['date'].'-12-31' . "'";
					$results = $database->simple_query($query);
					$oFC->description .= __LINE__ . ": Archived records removed from actual system".NL;
				}
			} else {
				$oFC->description .= __LINE__ . ": Resultaat niet nul: archiving discontinued".NL;
			}
			break;
		case "rollback":
			$files = array();
			$files[] = $oFC->file_ref[98]; // rekening
			$files[] = $oFC->file_ref[99]; // booking
			$job = array( );
			$job[] = "DROP TABLE IF EXISTS `{filename}`"; // verwijder oude file
			$job[] = "RENAME TABLE `{filename}_roll` TO `{filename}`" ; // move current set to roll back
			foreach ($files as $key => $value) {
				$sql = "SHOW TABLES LIKE '".$value."_roll'";
				$results = array();  // the output buffer
				if ( $database->execute_query ( $sql, true, $results) && count($results) != 1) unset ( $files[$key]);
			}
			$errors = array( );
			foreach ($files as $key=>$value) {
				foreach ($job as $jobkey=>$jobvalue) {
					$jobvalue1= str_replace ("{filename}", $value, $jobvalue);
					$results = $database->simple_query( $jobvalue1);
				}
			}
			$oFC->description .= __LINE__ . " Rollback completed".NL;
			break;
		default:
			$oFC->description .= __LINE__ . " get: " . $_GET[ 'command' ] . NL;
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	}
} else {

}

/* the selection options */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'SELECTION' ] = "";
		$oFC->page_content [ 'DESCRIPTION' ] = ""; 
		$oFC->page_content [ 'TOEGIFT' ] = ""; 		
		foreach ($oFC->language [ 'DUMMY' ] as $pay => $load ) $oFC->page_content [ 'TOEGIFT' ] .=  $load . NL; 
		break;
	default: // default list
		$first_archive = true;
		$TEMPLATE2 = '<tr %1$s><td>%2$s</td><td>%3$s</td><td colspan ="3">%4$s</td><td>%5$s</td></tr>';
		$TEMPLATE3 = '<a href="' . LOAD_RETURN . '&command=balans&module='.$module_name.'&date=%1$s&archive=%2$s"><img src="' . LEPTON_URL . '/modules/gsmoffb/img/clock_16.png">Balans </a>';
		$TEMPLATE4 = '<a href="' . LOAD_RETURN . '&command=resultaat&module='.$module_name.'&date=%1$s&archive=%2$s"><img src="' . LEPTON_URL . '/modules/gsmoffb/img/clock_red_16.png">Resultaat </a>';
		$TEMPLATE5 = '<a href="' . LOAD_RETURN . '&command=details&module='.$module_name.'&date=%1$s&archive=%2$s"><img src="' . LEPTON_URL . '/modules/gsmoffb/img/sections_16.png">Details</a>';
		$TEMPLATE6 = '<a href="' . LOAD_RETURN . '&command=stand&module='.		$module_name.'&date=%1$s&archive=%2$s"><img src="' . 	LEPTON_URL . '/modules/gsmoffb/img/clock_acc_16.png">Standen</a>';
		$TEMPLATE7 = '<a href="' . LOAD_RETURN . '&command=archive&module='.	$module_name.'&date=%1$s&archive=%2$s"><img src="' . 	LEPTON_URL . '/modules/gsmoffb/img/clock_del_16.png">Archive</a>';
		$TEMPLATE8 = '<a href="' . LOAD_RETURN . '&command=rollback&module='.	$module_name.'&date=%1$s&rollback=%2$s"><img src="' . 	LEPTON_URL . '/modules/gsmoffb/img/clock_del_16.png">Rollback</a>';
		$TEMPLATE9 = '<a href="' . LOAD_RETURN . '&command=remove&module='.		$module_name.'&date=%1$s&rollback=%2$s"><img src="' . 	LEPTON_URL . '/modules/gsmoffb/img/clock_del_16.png">Remove Rollback</a>';
		$treshold = date("Y");
		$oFC->page_content ['KOPREGELS'] = sprintf( $TEMPLATE2, '', "jaartal", "type", "action", "", "", "");
		//van welke jaren er zijn er archieven
		$dir_from = LEPTON_PATH.$oFC->setting [ 'mediadir' ]."/";
		if ( is_dir (  $dir_from ) ) { 
			$dirs = array ( );
			if ( $handle = opendir ( $dir_from ) ) { 
				while ( false !== ( $file = readdir ( $handle ) ) ) { 
					if ( $file == '.' || $file == '..' ) continue;
					if ( is_dir ( $dir_from . $file ) && is_readable ( $dir_from . $file ) ) array_unshift ( $dirs, $file );
				}
			} 		
			closedir ( $handle );
		}
		$tableArch = array();
		foreach ( $dirs as $item ) {
			$Localhulp = str_replace ( strtoupper($oFC->setting [ 'owner' ]), "", strtoupper ($item));
			if ( $Localhulp != strtoupper ( $item ) ) $tableArch [ $Localhulp ] = sprintf ( '%s%s/%s', LEPTON_PATH ,$oFC->setting [ 'mediadir' ], $item ) ;   
		}	
		// bepalen welke jaren er zijn in de current file
		$table = $oFC->gsm_preloadDataB ( 'b{REKYEARS}' );
		ksort ( $table );
		foreach ($table as $key => $value) {
			if ($treshold > $key && $first_archive) {
				if (isset ( $tableArch [$key] ) ) unset ( $tableArch [$key] );
				$oFC->page_content ['DESCRIPTION'] .= sprintf( $TEMPLATE2, "",
					$key,
					"in current file",
					sprintf( $TEMPLATE4, $key, "yes")."&nbsp;".sprintf( $TEMPLATE5, $key, "no")."&nbsp;".sprintf( $TEMPLATE7, $key, "no"),
					"", "","");
				$first_archive = false;
			} elseif ($treshold >= $key ) {
				if (isset ( $tableArch [$key] ) ) unset ( $tableArch [$key] );
				$oFC->page_content ['DESCRIPTION'] .= sprintf( $TEMPLATE2, "", $key,
				"in current file",
				sprintf( $TEMPLATE5, $key, "no"),
				"","","");
			}
		}
		// bepalen welke jaren er zijn in closed data
		$table = $oFC->gsm_preloadDataB ( 'b{REKHIST}' );
		$first_archive = true;
		$roll_back_present = false;
		$files = array();
		$files[] = $oFC->file_ref[98]; // rekening
		$files[] = $oFC->file_ref[99]; // booking
		$job = array( );
		foreach ($files as $key => $value) {
			if (isset ( $tableArch [$key] ) ) unset ( $tableArch [$key] );
			$sql = "SHOW TABLES LIKE '".$value."_roll'";
			$results = array();
			$database->execute_query( $sql, true, $results, true, false );
			if(count($results) == 1) {
				$oFC->description .= __LINE__ . " Rollback present : ".$value.NL;
				$roll_back_present= true;
			}
		}
		foreach ($table as $key=> $value) {
			if (isset ( $tableArch [$key] ) ) unset ( $tableArch [$key] );
			$oFC->page_content ['DESCRIPTION'] .= 
				sprintf( $TEMPLATE2,
					"",
					$key,
					"in closed archive",
					sprintf( $TEMPLATE5, $key, "yes"),
					($first_archive && $roll_back_present ) ? sprintf( $TEMPLATE8, $key, "no")."&nbsp;".sprintf( $TEMPLATE9, $key, "no") : "",
						"", 
						"",
						"");
			$first_archive = false;
		}
		//van welke jaren er zijn er archieven
		$first_archive = true;
		$roll_back_present = false;
		krsort ( $tableArch );
		foreach ( $tableArch as $key=> $value) {
			$oFC->page_content ['DESCRIPTION'] .= 
				sprintf( $TEMPLATE2,
					"",
					$key,
					"archief aanwezig",
					sprintf( $TEMPLATE5, $key, "yes"),
					($first_archive && $roll_back_present ) ? sprintf( $TEMPLATE8, $key, "no")."&nbsp;".sprintf( $TEMPLATE9, $key, "no") : "",
						"", 
						"",
						"");
			$first_archive = false;
		}
		break;
} 


/* output processing */
/* memory save */
$oFC->page_content [ 'MEMORY' ] = $oFC->gsm_memorySaved ( 3 ); /* end save values */

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
//		$template_name = '@'.LOAD_MODULE.LOAD_SUFFIX.'/back.lte';
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>