<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
// function to remove old directories
function gsm_rmdir( $dir )	{
	if (empty($dir)) return false;
	if ( is_dir( $dir ) )  {
 		$files = scandir( $dir );
  			foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) gsm_rmdir( "$dir/$file" ); }
    		rmdir( $dir );
 	} else if ( file_exists( $dir ) ) {
    	unlink( $dir );
}	}

// functions to copy files and non-empty directories  
function gsm_xcopy( $src, $dst ) {
	// 	if ( file_exists( $dst ) ) { gsm_xrmdir( $dst ); }
 	if ( is_dir( $src ) )  {
   		if ( !file_exists( $dst ) ) mkdir( $dst, 0777);
    	$files = scandir( $src );
    	foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) xcopy( "$src/$file", "$dst/$file" ); }
  	} else if ( file_exists( $src ) ) { copy( $src, $dst ); }	
} 

if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) { 
	$xmode = strtoupper ($xmode);  
	if (strstr($xmode, "DETAIL")) {
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . ' Detail function is selected'.NL;
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__ . ' This module version  : 20240218';
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . ' selection :'. $xmode;
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . ' Functions detected: ';
		
		if (strstr ( $xmode, "DETAIL" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' DETAIL.';
		if (strstr ( $xmode, "IMAGE" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' IMAGE.';
		if (strstr ( $xmode, "INSTAL" ) )	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' INSTALL.';
		if (strstr ( $xmode, "REPAIR" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' REPAIR.';
		if (strstr ( $xmode, "LOGGING" ) ) 	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' LOGGING.';
		if (strstr ( $xmode, "REMOVE" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' REMOVE.';
		if (strstr ( $xmode, "EXPL" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' EXPL.';
		
		
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' company :'. $oFC->setting [ 'droplet' ] [ LANGUAGE . '0' ];	
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Directories involved :';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'collectdir' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'datadir' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'mediadir' ] ; 
		
		foreach ( $oFC->setting [ 'entity' ] as $key => $value ) 
			$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' entity : '. $key . " | " .$value ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' owner :'. $oFC->setting [ 'owner' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' allowed :'. implode("|", $oFC->setting [ 'allowed' ] ) ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' menu :'. $FC_SET [ 'SET_menu' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' function :'. $FC_SET [ 'SET_function' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' rekening :'. $oFC->setting [ 'rekening' ] ;		

		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' zoek booking :' .  $oFC->setting [ 'zoek' ] [ 'booking' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' zoek schema :' .  $oFC->setting [ 'zoek' ] [ 'schema' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' label :' . implode("|", array_keys (  $oFC->setting [ 'label' ] ) );
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' aantal regels : ' . $oFC->setting [ 'qty_max' ]  ;

		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Tables involved :';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->file_ref [ 99 ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->file_ref [ 98 ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' end settings :'.NL;
	} 

/* install */
	if (strstr ( $xmode, "INSTAL" ) ) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start Install function: Frontend for this module will be created / re-created';
		// determine directories to be used.
		$dir_from = LEPTON_PATH.'/modules/'.LOAD_MODULE.LOAD_SUFFIX.'/frontend/' .LOAD_MODULE.LOAD_SUFFIX;
		$dir_to = LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/'.LOAD_MODULE.LOAD_SUFFIX;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Frontend files (from install package) will be moved to default template.';
		// copy and replace function
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' from : '.$dir_from;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' to : '.$dir_to;
		if ( file_exists ( $dir_to ) ) {
			$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' save previous to  : '.$dir_to."_bck";
			$oFC->gsm_copyFile( $dir_to, 1, $dir_to."_bck" );
		}
		$oFC->gsm_copyFile( $dir_from, 1, $dir_to );
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' End Install function'.NL;
	} 	

/* images */		
	if (strstr ( $xmode, "IMAGE" ) ) {	
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start Image function';
		$dir_from = LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/img/';
		$dir_to = LEPTON_PATH.'/modules/'.LOAD_MODULE.LOAD_SUFFIX.'/img/';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Frontend image directory will be moved to image directory (to overwrite install package).';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' from : '.$dir_from;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' to : '.$dir_to;
		$oFC->gsm_copyFile( $dir_from, 3, $dir_to );
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' End Image function'.NL;
	}  

	
	if ( strstr ( $xmode, "xROLLBACK" ) ) {
		// swap copy with main file
		$oFC->description .= date ( "H:i:s " ). __LINE__  . " rollback: swap backup/ production files: start".NL;
		$job= array();
		$oFC->description .= date ( "H:i:s " ). __LINE__  . "clear temporary space ".NL;
		$job[] = "DROP TABLE IF EXISTS `" . $oFC->file_ref   [ 99 ] . "_bck`";
		$job[] = "DROP TABLE IF EXISTS `" . $oFC->file_ref   [ 98 ] . "_bck`";
		$job[] = "DROP TABLE IF EXISTS `" . $oFC->file_ref   [ 97 ] . "_bck`";
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . "move backup files to temporary space ".NL;
		$job[] = "RENAME TABLE `" . $oFC->file_ref   [ 99 ] . "2` TO `".$oFC->file_ref   [ 99 ] . "_bck`";
		$job[] = "RENAME TABLE `" . $oFC->file_ref   [ 98 ] . "2` TO `".$oFC->file_ref   [ 98 ] . "_bck`";;
		$job[] = "RENAME TABLE `" . $oFC->file_ref   [ 97 ] . "2` TO `".$oFC->file_ref   [ 97 ] . "_bck`";
		$oFC->description .= date("H:i:s "). __LINE__  . "move production files to backup location ".NL;
		$job[] = "RENAME TABLE `" . $oFC->file_ref   [ 99 ] . "`  TO `".$oFC->file_ref   [ 99 ] . "2`";
		$job[] = "RENAME TABLE `" . $oFC->file_ref   [ 98 ] . "`  TO `".$oFC->file_ref   [ 98 ] . "2`";
		$job[] = "RENAME TABLE `"  .$oFC->file_ref   [ 97 ] . "`  TO `".$oFC->file_ref   [ 97 ] . "2`";
		$oFC->description .= date("H:i:s "). __LINE__  . "move files from temporary  space file to production location ".NL;		
		$job[] = "RENAME TABLE `" . $oFC->file_ref   [ 99 ] . "_bck` TO `" . $oFC->file_ref   [ 99 ] . "`";
		$job[] = "RENAME TABLE `" . $oFC->file_ref   [ 98 ] . "_bck` TO `" . $oFC->file_ref   [ 98 ] . "`";
		$job[] = "RENAME TABLE `" . $oFC->file_ref   [ 97 ] . "_bck` TO `" . $oFC->file_ref   [ 97 ] . "`";
		if ( isset ( $job ) && count ( $job ) > 0 ) {
			foreach ( $job as $key => $query ) $database->simple_query ( $query );
		}
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . "  rollback: swap backup / production files:  completed" . NL;
	}

	if (strstr($xmode, "xCOPY")) {
		// copy files
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . "copy production to backup files: start" . NL;
		$job= array();
		$oFC->description .= date ( "H:i:s " ). __LINE__  . "removebackup files if any " . NL;
		$job[] = "DROP TABLE IF EXISTS `".$oFC->file_ref  [99]."2`";
		$job[] = "DROP TABLE IF EXISTS `".$oFC->file_ref  [98]."2`";
		$job[] = "DROP TABLE IF EXISTS `".$oFC->file_ref  [97]."2`";
		$oFC->description .= date("H:i:s "). __LINE__  . "create empty backup files ".NL;
		$job[] = "CREATE TABLE `".$oFC->file_ref  [99]."2` LIKE `".$oFC->file_ref  [99]."`";
		$job[] = "CREATE TABLE `".$oFC->file_ref  [98]."2` LIKE `".$oFC->file_ref  [98]."`";
		$job[] = "CREATE TABLE `".$oFC->file_ref  [97]."2` LIKE `".$oFC->file_ref  [97]."`";
		$oFC->description .= date("H:i:s "). __LINE__  . "copy data to backup files ".NL;
		$job[] = "INSERT `".$oFC->file_ref  [99]."2` SELECT * FROM `".$oFC->file_ref  [99]."`";
		$job[] = "INSERT `".$oFC->file_ref  [98]."2` SELECT * FROM `".$oFC->file_ref  [98]."`";
		$job[] = "INSERT `".$oFC->file_ref  [97]."2` SELECT * FROM `".$oFC->file_ref  [97]."`";
		if ( isset($job) && count( $job ) > 0 ) {
			foreach( $job as $key => $query) $database->simple_query( $query);
		}
		$oFC->description .= date("H:i:s "). __LINE__  . "copy production to backup files :completed".NL;
	}

// remove data
	if (strstr($xmode, "xREMOVE")) {
		$nremoved=0;
		$job=array();
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . ' Start remove function' . NL;
		$check_query = "SELECT * FROM `" . $oFC->file_ref  [ 99 ]."`  ORDER BY `area`";
		$results = array();
		if ( $database->execute_query( $check_query, true, $results) && count($results) == 0) $oFC->description .= $oFC->MOD_GSMOFF['TXT_ERROR_DATA']. NL; 
		foreach ($results as $result) { 
			if ( ($result[ 'date' ] > $result['keeptill'] && strpos($xmode, 'EXPIRE') !== false ) || 
				(strpos($result['project'], 'weg') !== false && strpos( $result [ 'keywords' ], 'recycle' ) !== false)) {
				$path = $result [ 'area' ]. $result[ 'location' ]. $result[ 'name' ]; 
				$location = LEPTON_PATH. $path;  
				if (file_exists($location )) {
					unlink ($location);
					$nremoved++;
					$oFC->description .= $nremoved . ' : '. $path . NL;
		}	}	}
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Documents removed : '.$nremoved.NL; 	
		$xmode .= "REPAIR" ;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Database repair is initiated'.NL; 
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' End remove function' . NL;
	} 

// repair database
	if (strstr($xmode, "xREPAIR")) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start repair function' . NL;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Attachment correction is initiated'.NL; 		
		$from = $oFC->setting [ 'mediadir' ] . $oFC->setting [ 'datadir' ];
		$dir_from = LEPTON_PATH. $from;
		$tableBijlage = $oFC->gsm_scanDir ($dir_from, 1  );		
		foreach (	$tableBijlage as $pay => $load) {
			if ( substr ($pay, 0,2 ) != $oFC->setting [ 'owner' ] )
			{
				rename ($load, $dir_from. "/" . $oFC->setting [ 'owner' ]  . substr ($pay, 2 )) ;
			}	
		}		
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Copy attempt existing database' . NL;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Database repair is initiated'.NL; 
		$oFC->description .= NL.date ( "H:i:s " ) . __LINE__  . ' End repair function' . NL;
	} 
	
/* remove logging */
	if ( strstr ( $xmode, "LOG" ) ) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start logging function' . NL;
		$dir_to_remove = sprintf ( "%s%s/" , LEPTON_PATH, $oFC->setting [ 'collectdir' ] ?? "/geen data/"  );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' remove attempt ' .$dir_to_remove. NL;
		if ( file_exists ( $dir_to_remove ) ) {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' remove ' . $dir_to_remove . NL;
			gsm_rmdir ( $dir_to_remove );
		}
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' create empty directory' . NL;
		if ( !file_exists ( $dir_to_remove ) ) mkdir ( $dir_to_remove, 0777 );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' empty directry creation completed' . NL;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' End logging function' . NL;
	} 

	if ( strstr ( $xmode, "EXPL" ) ) {
		$oFC->page_content [ 'TOEGIFT' ] .= '<table class="ui very basic collapsing celled table">';
		$oFC->page_content [ 'TOEGIFT' ] .= '<tr class="active"><td>d_xxxx_</td><td><strong>function</strong></td></tr>';
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>INSTALL </td><td> frontend files are created: customized values are overwritten bij default values !!</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>IMAGE </td><td> image directory copied from frontend</td></tr>";
//		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>REPAIR </td><td> repair database</td></tr>";
//		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>REMOVE </td><td> remove data (e_mail = recycle).</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>LOGGING </td><td> empty the logging</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>DETAIL </td><td> detailed data</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "</table>";		
	}
}
?>