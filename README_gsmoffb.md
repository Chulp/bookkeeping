# taxonomy
Maintenance of reference tables for other Gsmoff applications.

This document is relevant for `Gsmoffb` / `Bookkeeping`  version 7.0.0 and up unless indicated otherwise..

## Download
It is recommended use install/update to the latest available version listed. 
note verion 7.0.0 and upwards are tested in combination with Lepton 7.0 

## Datastructures

### Taxonomy group

Basically this defines the various tables contained in this taxonomy table

	* groep = taxonomy groep
	* reference = various table names 
	* omschrijving = description
	* toelichting = additional info

Explenation of the structure of reference
example
 	a. setting	allowed			 	pdf|jpg|mp4|html|zip
the setting of the allowed parameter is given

	b. setting	allowed|gsmoffa		pdf|jpg|mp4|html|zip|
the setting of the allowed parameter in the gsmoffa module  (a is default)

	c. setting	allowed|gsmoffa|1	pdf|jpg|mp4|html|zip|
the setting of the allowed parameter for people logged in  (b or c are default)

	d. setting	allowed|gsmoffa|2	pdf|jpg|mp4|html|zip|
the setting of the allowed parameter for people with editing rights (b or c are default)

Explenation of the structure of description
example
 	a. setting 	size				6000000 	
Just a single value 600000

	b. setting	allowed				pdf|jpg|mp4|html|zip|
A set of values separated by pipes (|) 

#### For taxonomy (gsmofft) use

the following is suggested

	1. setting 	mediadir|gsmofft 	/media/taxonomy 	
	2. setting 	owner				XX 	
	3. setting 	qty_max				60 
	3. setting 	size				6000000 	
	4. zoek		taxonomy			|type|ref|

#### other modules use

	1. setting 	jpgwidth			200
	2. entity 	PV					PV - Besloten
	3. entity 	XX 					XX - Standaard
	4. setting	allowed			 	pdf|jpg|mp4|html|zip
	5. droplet	EN1 - EN15     		(NL0 or NL15 )
	6. droplet	EN20 - EN21    		(NL20 or NL21 )
	7. setting  debug          		yes 	
	8. setting	collectdir 			/media/logging 

Note the standard setting debug yes is to be changed for production use 
	 
##  Status

This additional function provides:

	 1. login behaviour of the various registered persons
	 2. survey of access rights to the varions pages
	 3. creation of CSV files of the datatables used in the Gsm series of applications

note this status functions are only available when the gsmoffl / Login module is available

## Droplets

The installation comes with a number of Droplets.
In fact the standard officetegel template assumes the precense of some these Droplets
 
### The droplet Gsm_taxo
 
The call [[Gsm_taxo?blocks=0&set=&item=]] 

Parameters 

	 * block The parameter prepended with the language abbreviation (NL , EN) is the type in the taxonomy file (if item is not provided as a parameter.
	 * set : the group of the Taxonoy file 
	 * item  :the type of the taxonomy file
	 example [[Gsm_taxo?set=vat&item=hoog]] 
	 example [[Gsm_taxo?blocks=10]] 
	 example [[Gsm_taxo?item=NL10]] 

This droplet makes the droplet Gsm_ref depricated

### The droplet Gsm_taxi

The call [[Gsm_taxi?blocks=0&set=&item=]]

Parameters

 * block The parameter prepended with the language abbreviation (NL , EN) is the type in the taxonomy file (if item is not provided as a parameter.
 * set : the group of the Taxonoy file 
 * item  :the type of the taxonomy file
 example [[Gsm_taxi?set=vat&item=hoog]] 
 example [[Gsm_taxi?blocks=10]] 
 example [[Gsm_taxi?item=NL10]] 
The gsm_taxi droplet (not available in all versions) is intended to be upwards compatible with the gsm_taxo droplet.

Special functions of this droplet
* set : 1 	gives a greeting depending on the time of the day
example [[Gsm_taxi?set=1]]
This droplet function makes the droplet Gsm_ref depricated

* set : all gives a survey of the available Parameters
example [[Gsm_taxi?set=all]]
n.b. this droplet uses the template @gsmofft/taxo_sheet_1.lte

### The droplet Gsm_autoplay

### The droplet Gsm_bestel_voorbeeld

### The droplet Gsm_calendar
This month with repeats 

the call [[Gsm_calendar?set=month&blocks=20]]

Parameters

 * block The template to be used (examples 20, 21, 22)
 * set : The format "year", "month", "week" or "number"
 * item  :the type of the taxonomy file
 example [[Gsm_calendar?blocks=20&set=month]]  	monthly view template 20 
 example [[Gsm_calendar?blocks=20&set=week]] 	weekly view template 20
 example [[Gsm_calendar?blocks=22&set=year]]	yearly view only public no repeats
 example [[Gsm_calendar?blocks=21&set=10]]		next 10 entries 

Testing with added text fields
<img src="[CUSTOM_CONTENT]" border ="0" alt="[CUSTOM_NAME]" /> for  field 4 and 5 (image)
<a href="[CUSTOM_CONTENT]"> link </a> for  field 3 toe enter link (text)
[CUSTOM_CONTENT] for  field 1 and 2  to enter text (text)

### The droplet Gsm_groet

gives a greeting depending on the time of the day
This droplet is depricated as [[Gsm_taxi?set=1]] delivers same

### The droplet Gsm_loc

### The droplet Gsm_log


Gives logging of the input or logging parameters

example 	[[Gsm_log?set=input]] 	For the input parameters $_POST and $_GET and file upload
example		[[Gsm_log?set=logging]] For the system paraeters the session parameters and logging file details

available from taxonomy 1.0.8
This will make the droplets Gsm_var and Gsm_post depricated

### The droplet Gsm_login

### The droplet Gsm_loginbox

### The droplet Gsm_lorem

### The droplet Gsm_mediadoc

droplet to give access to the media in a certain directory  

the call  [[Gsm_mediadoc?mediadir=/archive&project=XX&width=&message=200&login=no&seq=s]] 

 - mediadir=/archive 	to change the media subdirectory
 - project=XX 			add another subdirectory
 - width=200			display width (for jpg only
 - message=No data		message when the directory is empty
 - login=no				yes will allow only logged in persons to access the data
 - seq=s				display mode seq=r is reversed sequence. card gives different layout methods

e.g. [[Gsm_mediadoc?mediadir=/assets/layout/castordoc]] 

e.g. [[Gsm_mediadoc?project=XX1]]  	so default directory and subdirectory XX1

e.g. [[Gsm_mediadoc?seq=card]] 		different layout

### The droplet Gsm_mod

### The droplet Gsm_pdf

### The droplet Gsm_title


Warning
note taxonomy tables with equal combination of type and ref will face automatical adaption of the ref value
 
## Warning

note taxonomy tables with equal combination of type and ref will face automatical adaption of the ref value 