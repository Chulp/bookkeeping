<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$MOD_GSMOFFB = array(
	'OWN' => "MOD_GSMOFFB",
	'LANG' => "NL",
	'VERS' => "v20240430",
		
	'active' => array ( 
		'0' => 'niet actief', 
		'1' => 'actief'),
			
	'DUMMY' => array (
		'0' => 'Geen functionaliteit. Database niet geinitialiseerd, geen rechten of foute instellingen',
		'1' => 'Dummy module zonder functionaliteit is gestart',
		'2' => 'Controleer of de initiele routines zijn uitgevoerd'),
		
	'grootboek' => array (
		'0' => 'Kl 0: Niet gealloceerd',
		'1' => 'Kl 1: Eigen vermogen en langlopende schulden',
		'2' => 'Kl 2: Vaste activa en langlopende vorderingen ',
		'3' => 'Kl 3: Voorraden en bestellingen ',
		'4' => 'Kl 4: Kortlopende schulden en vorderingen ',
		'5' => 'Kl 5: Liquide middelen en opvraagbare beleggingen',
		'6' => 'Kl 6: Kosten',
		'7' => 'Kl 7: Opbrengsten',
		'8' => 'Kl 8: Tussen rekeningen',
		'9' => 'kl 9: Niet in de balans opgenomen rechten en verplichtingen',
		'11' => 'Totaal Activa',
		'12' => 'Totaal Passiva',
		'13' => 'Totaal Uitgaven',
		'14' => 'Totaal Inkomsten',
		'15' => 'Resultaat'	),
		
	'layout' => array ( 
		'aanv0' =>	'<p>aanvulling %s voor %s ( %s ) door %s </p><p>%s</p><hr />',
		'show0' =>	'<div class="ui text container">', 
		'show9' =>	'</div>',
		'show1' =>	'',
		'castor0' =>	'--'),
		
	'line_color' => array( 
		0 => '', 
		1 => 'bgcolor="#eeeeee"', 
		2 => 'bgcolor="#dddddd"', 
		3 => 'bgcolor="#cccccc"', 
		4 => 'bgcolor="#bbbbbb"'),
		
	'pdf'	=> array ( 
		'0' => "Document created on : ", 
		'1' => "Aantal regels verwerkt: ",
		'2' => "Selected options: ",
		'3' => "Modules versions: " ),
		
	'PDF_TAIL' => array(
		'TOTAL' => 		"Total records : ",
		'MAILED' => 	"Records mailed : ",	
		'POSTED' => 	"Records to be posted : ",
		'UNSELECTED' => "Records unselected : ",
		'SELECTED' => 	"Shares selected : ",
		'NOT_SELECTED' => "Shares not selected : ",
		'MAILING' => 	"Mailing on : ",
		'SELECTION' => 	"Selection : ",
		'EVERYBODY' => 	" Iedereen ",
		'EVERY_MAIL' => " Iedereen met mailadres ",
		'ALL_MEMBERS' =>" Alle leden ",
		'REMINDER' => 	" Reminder ",
		'REFERENCED' => " Referenced ",
		'SHAREHOLDERS' => " Aandeelhouders ",
		'STANDEN' => " Afhankelijk van standen "),

	'type' => array ( 
		'1B' => 'Activa', 
		'2B' => 'Passiva', 
		'3R' => 'Uitgaven', 
		'4R' => 'Inkomsten'),
		
	'type_sign' => array(
		'1B' => 1,
		'2B' => -1,
		'3R' => 1,
		'4R' => -1),
		
	'tbl_icon' => array ( 
		1 =>'View', 
		2 =>'Return', 
		3 =>'Add',
		4 =>'Save',  
		5 =>'Save (as new)', 
//		6 =>'Remove', 
//		7 =>'Calculate',
//		8 =>'Check',
		9 =>'Select', 
//		10 =>'+',
		11 =>'Print', 
		12 =>'Set',
//		13 =>'reserved',
//		14 =>'Next',
//		15 =>'Test',
//		16 =>'Mail',
		17 =>'Process', 
		18 =>'Invoicing', 
//		19 =>'Balans', 
//		20 =>'Result' ,
		21 =>'Verwerkt'
	),  

	'TXT_ADRES' => array (
		'DAT0'	=> 'dat0 (geb) :',
		'DAT1'	=> 'dat1 (van) :',
		'DAT2'	=> 'dat2 (tot) :',
		'REF0'	=> 'ref0 (deel) :',
		'REF1'	=> 'ref1 (bank) :',
		'REF2'	=> 'ref2 (card) :',
		'INFO'	=> '-- info --'	),
		
	'TXT_COMP' => array (
		'1' => 'Consumer/Private Person',
		'2' => 'VAT free organsiation',
		'3' => 'Company',
		'4' => 'Partnership',
		'5' => 'Limited Partnership',
		'6' => 'Cooperative',
		'7' => 'Private Company',
		'8' => 'Limited Company',
		'9' => 'Organisation under VAT regime'),
		
	'TXT_MEMBER' => array (
		0	=> '---',
		1	=> 'member',
		9	=> 'ex-member' ), 
		
	'TXT_TYPE'  => array (
		0	=> 'unknown',
		1	=> 'by post',
		2	=> 'by mail',
		3	=> 'on-line'),
			
	'ACC_BAL' 			=> 'Balans',
	'ACC_RES' 			=> 'Resultaat',
	'ACC_TOT' 			=> 'Totaal',
	'ACC_VENW' 			=> 'Resultaat rekening',
	'DATABASE UPDATE' 	=> ' Database records adapted : ',
	'TXT_ACC' 			=> array ( 
		1 => 'Openings Balans', 
		2 => 'active'),
	'TXT_ACTIVE_DATA'	=> ' Actief record gevonden' ,	
	'TXT_CONSISTENCY'	=> ' Oeps consistency controle',
	'TXT_DATABASE_NEW'	=> ' Initial record added ',
	'TXT_DIR_CREATION' 	=> ' Directory aangemaakt',
	'TXT_ERROR_ADRES'	=> ' Oeps name and / or address data missing',
	'TXT_ERROR_DATA' 	=> ' Oeps geen data ',  
	'TXT_ERROR_DATABASE' => ' Oeps inconsisten database field  ',
	'TXT_ERROR_INIT'	=> ' Oeps systeem niet geinitialiseerd en/of lege database ',
	'TXT_ERROR_SIPS'	=> ' Oeps sips active ',
	'TXT_ERROR_PAGE'	=> ' Oeps unexpected situation ',		
	'TXT_LOGIN' 		=> ' Login',
	'TXT_LOGIN_ERROR' 	=> ' Not a valid e-mail address or existing already or password too short.',
	'TXT_LOGIN_NOW' 	=> ' Uw login data is aangepast. Login met uw nieuwe gegevens. ',
	'TXT_LOGIN_REGISTER' => ' Register / Change Password',
	'TXT_LOGIN_SETT'	=> ' Correct Login Settings  ',
	'TXT_LOGIN_VERIFY' 	=> ' Verificatie ',
	'TXT_MAINTENANCE' 	=> ' Maintenance ', 
	'TXT_NO_ACCESS'		=> '(Partner) Access not available ',
	'TXT_REC_CHANGE'	=> ' Aantal records aangepast : ',	
	'TXT_REMOVE_REF'	=> 'weg',
	'TXT_REMOVE_KEYWORD'=> 'recycle',
	'TXT_SETUP' 		=> ' Setup ',

	'SETzoek'  => array (
		'castor'	=> 	'|id|prefix|project|keywords|filetype|',
		'adres'		=> 	'|id|adresid|name|email|',
		'standen'	=> 	'|id|name|omschrijving|',
		'files'		=> 	'|id|prefix|project|keywords|filetype|',
		'products'	=> 	'|id|prefix|project|keywords|filetype|',
		'cursus'	=> 	'|id|prefix|project|keywords|filetype|',
		'kalender'	=> 	'|id|prefix|project|keywords|filetype|',
		'project'	=> 	'|id|name|omschrijving|',
		'schema'	=> 	'|id|name|rekeningnummer|' ),		
		

);
		
?>