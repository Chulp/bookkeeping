<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
class GeneralRoutines extends LEPTON_abstract {
/* * / 		public LEPTON_database $database;
/* * / 		public LEPTON_admin $admin;
/* B  */ 	public $cal 			= array();	/* just for calculation */
/* B */ 	public $cols 			= array();	/* aray with displayable tekst */
/* L * / 	public $field_content 	= array();	/* field contents */
			public $file_ref		= array();	/* array of file references */
			public $memory		 	= array();	/* memory parameters */
			public $page_content 	= array();	/* output parameters for the twig output. */
			public $paging 			= array();	/* paging for more as one datapage */
/* B */ 	public $pdf_data 		= array();	/* pdf data */
			public $setting 		= array();	/* settings array */
			public $user 			= array();	/* usr data array */
			public $version 		= array();	/* array of version numbers of the used modules */
			public $description 	= '';	/* aray with displayable tekst */
			public $nodata 			= 0; 	/* function switch */
/* B */		public $pdf_header 		= '';	/* */
/* B */		public $pdf_text		= '';  	/* pdf data */
/* * / 		public $protocol 		= '';  	/* https or http */
			public $recid 			= 0;	/* record id */
			public $search_mysql	= '';	/* selection converted to search parameter */
/* * / 		public $search_mysql9 	= '';	/* selection converted to search parameter */
			public $selection 		= '';  	/* input field selection */
			public $sips 			= false;	/* sips conclusion */

	static $instance;
	
    /* Initialize */
    public function initialize() {
		$this->version ['class'] = "20240922";	
		/* no alternative main functions 
		/* $this->database = LEPTON_database::getInstance();	
		
		 /* input processing */
		if ( !isset ( $_SESSION [ 'page_h' ] )
			|| !isset ( $_POST[ 'sips' ] )
			|| ( $_SESSION [ 'page_h' ] <> $_POST [ 'sips' ] ) ) $this->sips = true;
			
		/* record id */ 
		if ( isset ( $_POST [ 'recid' ] ) && $_POST [ 'recid' ] > 0 ) { 
			$this->recid = $_POST [ 'recid' ]; 
			unset ( $_GET[ 'recid' ] );
		}
		if ( isset ( $_GET [ 'recid' ] ) && $_GET [ 'recid' ] > 0 ) $this->recid = $_GET [ 'recid' ];
		
		/* system state */ 
		$this->setting [ 'protocol'] = ( isset ( $_SERVER[ 'HTTPS' ] ) && ( $_SERVER [ 'HTTPS' ] == 'on' ) ) ? "https" : "http";
		
		/* system state */ 
		$this->user [ 'ip' ] = $_SERVER[ 'REMOTE_ADDR' ];
		$this->user [ 'device'] = $_SERVER [ 'HTTP_USER_AGENT' ];
		$this->user [ 'privileged' ] = ( isset ( $_SESSION [ 'USER_ID' ] ) && is_numeric ( $_SESSION [ 'USER_ID' ] ) ) ? 1 : 0;
		
		/* reference to taxonomy data */
		$this->file_ref [ 1 ] = LOAD_DBBASE . "_". "taxonomy";
		
		$this->gsm_initTool ();	
	
		/* 	$this->admin = LEPTON_admin::getInstance('Pages','Start',false,false); */	
    }
	
	public function gsm_accessRec ( 
		// ============================	
		&$fieldArr,			/* update fields */
		&$recid,			/* record id */
		$func = 	1, 		/* 1= update (remove unchanged fields) 2= new */
		$name = 	"-",	/* name dB table */
		$detect = 	"gsm_", /* prefix  relevant post fields */
		$mandatory = "id"	/* verplicht veld voor update */
		/* return de fields van het record */
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20240426";  
		$DATfilter  = sprintf ("y{%s;%s;%s}", 
			"0000-00-00", 
			date( "Y", time() )-100 . "-01-01", 
			date( "Y", time() ) +2  . "-12-31"); 
		$AMTfilter = sprintf ("v{%s;%s;%s}", 
			0, 
			-1000000, 
			1000000) ;
		$AANfilter = sprintf ("a{%s;%s;%s}", 
			0, 
			-1000000, 
			1000000) ;
		$TIMfilter = "t{12:00}";	
		$fileref = LOAD_DBBASE . "_" . str_replace ( LOAD_DBBASE . "_" , '',$name);  //gsm 20230101 change
		$okeDel = false; $okeUpd = false; $okeIns = false;
		if ( isset ( $_POST [ $detect . $mandatory ] ) || count ( $fieldArr ) > 2 ) { $okeUpd = true; $okeIns = true; }
		if ( $func == 1 ) { $okeDel = false; $okeUpd = true; $okeIns = false; }
		if ( $func == 2 ) { $okeDel = false; $okeUpd = false; $okeIns = true; }
		if ( $func == 3 ) { $okeDel = true; $okeUpd = false; $okeIns = false; }
		if ( !is_numeric ( $recid ) || strlen ( $recid ) < 1 ) { $okeDel = false; $okeUpd = false; } //	recid leeg
		/* this field must be there in the input to have update or insert */
		if ( !isset ( $_POST [ $detect . $mandatory ] ) ) { $okeUpd = false; }
		$local_array = array ();
		if ( $this->setting [ 'debug' ] == "---" )  Gsm_debug ( array ( 'data' => $fieldArr, 'recid' => $recid, 'func' => $func,  'DB table' => $fileref, 'detect' => $detect, 'verplicht' => $mandatory, 'POST' => $_POST , 'upd' =>$okeUpd, 'ins' =>$okeIns, 'del' =>$okeDel), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );
		/* valid id read record */
		$result = array ( );
		$sql 	= "SELECT * FROM `" . $fileref . "` WHERE `id` = '" . $recid  . "'";
		$sql2 	= "SELECT * FROM `" . $fileref . "` ORDER BY `updated` DESC, `id` DESC LIMIT 1";
		$database->execute_query ( 
			$sql, 
			true, 
			$result );
		if ( count ( $result ) == 0 ) { 
			/*  recid niet ok */
			$okeDel = false; $okeUpd = false; 
			$database->execute_query ( 
				$sql2, 
				true, 
				$result );
		} 
		if ( count ( $result ) == 0 ) return $local_array;
		$local_array = current ( $result );	
		// verwerking input
		foreach ( $_POST as $pay => $load ) { 
			if ( substr ( $pay, 0, 4 ) == $detect && !isset ( $fieldArr [ substr ( $pay, 4 ) ] ) ) {
				$fieldArr [ substr ( $pay, 4 ) ] = htmlentities ( trim ( $load ?? '' ) );
		}	} 
		if ( $okeUpd ) { 
			// case of update something changed / remove not changed
			foreach ( $fieldArr as $pay => $load ) { 
				if ( isset ( $local_array [ $pay ] ) && $local_array [ $pay ] == $fieldArr [ $pay] ) unset ( $fieldArr [ $pay ] ); 
		}	}
		// check numeric fields
		$change = array ();
		foreach ( $fieldArr as $pay => $load ) { 
			if ( substr ( $pay, 0, 3 ) == "amt") $change [ $pay ] = $this->gsm_sanitizeStringV ( $load, $AMTfilter );
			if ( substr ( $pay, 0, 3 ) == "aan") $change [ $pay ] = $this->gsm_sanitizeStringV ( $load, $AANfilter );
			if ( substr ( $pay, 0, 3 ) == "dat") $change [ $pay ] = $this->gsm_sanitizeStringD ( $load, $DATfilter );
			if ( substr ( $pay, 0, 3 ) == "tim") $change [ $pay ] = $this->gsm_sanitizeStringD ( $load, "t{12:00}" );
		}
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array ( 'data' => $fieldArr, 'up' => $change ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$fieldArr = array_merge ( $fieldArr, $change );
		$local_array = array_merge ( $local_array, $fieldArr );
		if ( $okeIns )  $fieldArr = $local_array; 
		if ( isset ( $fieldArr [ 'id' ] ) ) unset ( $fieldArr [ 'id' ] );
		if ( !empty ( $fieldArr ) && ( $okeUpd || $okeIns ) ) {	// Is er wat te schrijven
			/* unique test */
			if ( $okeIns ) $recid = 0;
			if ( $okeUpd ) { // update
				$database->build_and_execute ( 
					"update",
					$fileref,
					$fieldArr,
					"`id` = '" . $recid . "'" );
				$result = array ( );
				$database->execute_query ( $sql2, true, $result );
				$local_array = current ( $result );
			}
			if ( $okeIns ) { // insert
				unset ( $fieldArr [ 'updated' ] ); // gsm 20230102 corr 
				$database->build_and_execute ( 
					"insert",
					$fileref,
					$fieldArr );
				// readback
				$result = array ( );
				$database->execute_query ( $sql2, true, $result );
				if ( count ( $result ) > 0 ) {  
					$hulp = current ( $result );
					if ( $mandatory  == 'id' || $fieldArr [ $mandatory ] == $hulp [ $mandatory ] ) $local_array = $hulp; 
					$recid = $local_array [ 'id' ];
			}	}	
		} elseif ( empty ( $fieldArr ) && $okeDel  ) {	
			$database->simple_query ( sprintf ( "DELETE FROM `%s` WHERE `id`= '%s'" , $fileref, $recid ) );
		}
		return $local_array;
	}
	
	public function gsm_accessSql ( 
		// ============================	
		$input, 	// array in, returnvalue in a database Insert or Update. 
		$func = 2   // 1 = for "INSERT INTO `".$table1."` ".$content;  2 =  for "UPDATE `".$table3."` SET ".$content." WHERE .....
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20230808";  
		$part1 = '';
		$part2 = '';
		$first = true;
		$TEMP0 = ' ( %s ) VALUES ( %s ) ';
		$TEMP1 = ' %s ';
		switch ( $func ) { 
			case 1:
				foreach ( $input as $key => $value ) { 
					if ( $first ) { 
						$first = false; $part1 .= "`" . $key . "`";
						if ( $value === "NULL" ) { $part2 .= "NULL"; } else { $part2 .= "'" . $value . "'"; } 
					 } else { 
						$part1 .= ", `" . $key . "`";
						if ( $value === "NULL" ) { $part2 .= ", NULL"; } else { $part2 .= ", '" . $value . "'"; } 
				}	} 
				$local_content = sprintf ( $TEMP0, $part1, $part2 );
				break;
			default:
				foreach ( $input as $key => $value ) { 
					if ( $first ) { 
						$first = false; $part1 .= "`" . $key . "` = ";
						if ( $value === "NULL" ) { $part1 .= "NULL"; } else { $part1 .= "'" . $value . "'";	 } 
					 } else { $part1 .= ", `" . $key . "` = ";
						if ( $value === "NULL" ) { $part1 .= "NULL"; } else { $part1 .= "'" . $value . "'"; } 
				}	 } 
				$local_content = sprintf ( $TEMP1, $part1 );
				break;
		 } 
		return $local_content;
	 } 

	public function gsm_adresDet ( 
		// ============================
		$page_id , 	//	to populate user array, output in $this->user { } 
		$owner	= "XX" 	//	privileged 0 not logged in, 1 logged in, 2 access rights, 3 edit right
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20241028";  
		$this->user [ 'ref' ] = $owner;
		$this->user [ 'id' ] = 0;
		if ( $this->user [ 'privileged' ] > 0 ) { 
			$this->user [ 'id' ] = $_SESSION [ 'USER_ID' ];
			$this->user [ 'group' ] = $_SESSION [ 'GROUPS_ID' ];
			$this->user [ 'groep' ] = $_SESSION [ 'GROUP_NAME' ] [ $_SESSION [ 'GROUPS_ID' ] ];
			/* persoonsgegevens */
			$results = array ( ); // the output buffer
			$TEMPLATE = "SELECT * FROM " . TABLE_PREFIX . "pages WHERE page_id = '%s'";  
			/* check rechten */
			$database->execute_query ( 
				sprintf ( $TEMPLATE, $page_id ), 
				true, 
				$results );
			if ( count ( $results ) > 0 ) { 
				$row = current ( $results );
				$help_admin_groups = explode ( ',', $row [ 'admin_groups' ] );
				$help_viewing_groups = explode ( ',', $row [ 'viewing_groups' ] );
				foreach ( $help_viewing_groups as $key => $value ) { if ( $value == $this->user [ 'group' ] ) $this->user [ 'privileged' ] = 2; } 
				foreach ( $help_admin_groups as $key => $value ) { if ( $value == $this->user [ 'group' ] ) $this->user [ 'privileged' ] = 3; } 
				$results = array ( );
				$TEMPLATE = "SHOW TABLE STATUS LIKE '%s'";
				/* is er extended adres data lees  adresbestand */
				$database->execute_query ( 
					sprintf ( $TEMPLATE, LOAD_DBBASE . '_adres' ), 
					true, $results );
				if ( count ( $results ) > 0 ) { /* login module existing */
					$results = array ( );
					$TEMPLATE = "SELECT * FROM " . LOAD_DBBASE . '_adres' . " WHERE adresid = '%s'";
					// haal extended adresdata op 
					$database->execute_query ( 
						sprintf ( $TEMPLATE, $this->user [ 'id' ] ) , 
						true, 
						$results );
					if ( count ( $results ) > 0 ) { 
						$row = current ( $results );
						if (isset ($this->field_content ) )$this->field_content = $row; 
						$this->user [ 'name' ] = str_replace ( "|", " ", $row [ 'name' ] );
						$this->user [ 'email' ] = $row [ 'email' ];
						$this->user [ 'adres' ] = str_replace ( "|", ", ", str_replace ( ",", " ", $row [ 'adres' ] ?? "onbekend" ) );
						$this->user [ 'ref' ] =	( isset ( $row [ 'ref' ] ) 
							&& isset ( $this->setting [ 'entity' ] [ substr ( $row [ 'ref' ] , 0, 2 ) ] ) )
							? substr ( $row [ 'ref' ] , 0, 2 ) . $row [ 'id' ] 
							: $this->setting [ 'owner' ]. $row [ 'id' ];					
						$this->user [ 'tel' ] = str_replace ( "|", " / ", $row [ 'contact' ] );
						$this->user [ 'comp' ] = $row [ 'comp' ];
					 } 
				 } else { 
					$results = array ( );
					/* haal beperkte adresdata op */
					$TEMPLATE = "SELECT * FROM " . TABLE_PREFIX . "users  WHERE user_id = '%s'";
					$database->execute_query ( 
						sprintf ( $TEMPLATE, $this->user [ 'id' ] ) , 
						true, 
						$results );
					if ( count ( $results ) > 0 ) { 
						$row = current ( $results );
						$this->user [ 'name' ] = $row [ 'display_name' ];
						$this->user [ 'email' ] = $row [ 'email' ];
						$this->user [ 'adres' ] = '';
						$this->user [ 'ref' ] = $this->setting [ 'owner' ];
						$this->user [ 'tel' ] = '';
						$this->user [ 'comp' ] = '';
		}	}	}	} 
		$this->page_content [ 'PRIVILEGED' ] = $this->user [ 'privileged' ];
		return $this->user;
	 } 

	public function gsm_copyFile ( 
		// ============================
		$dir, 
		$func=1,  //1 copy files 2 remove files 3 move files
		$dir_to = '' 
		// ============================
		) {
		$this->version [ __FUNCTION__ ] = "20230808";  
		/* function to remove old directories */
		if ( $func  == 2 ) { 
			if ( is_dir ( $dir ) ) { 
				$files = scandir ( $dir );
				foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) $this->gsm_copyFile ( $dir . "/" . $file, 2 ); } 
				rmdir ( $dir );
			} elseif ( 
				file_exists ( $dir ) ) { unlink ( $dir ); 
			} 
		/* move files */
		} elseif ( $func == 3 ) { 
			if ( !file_exists ( $dir_to ) ) mkdir ( $dir_to, 0777 );
			if ( is_dir ( $dir ) ) { 
				$files = scandir ( $dir );
				foreach ( $files as $file ) { 
					if ( $file != "." && $file != ".." ) {
						if (is_file ( $dir_to . $file ) ) unlink ( $dir_to . $file );
						if (is_file ( $dir . $file ) ) copy ( $dir . $file, $dir_to . $file );
			}	 }	 }	
		} else { 
			/* function to copy files remove all old files */
			if ( file_exists ( $dir_to ) ) { $this->gsm_copyFile ( "$dir_to", 2 ); } // remove old directories if present
			if ( is_dir ( $dir ) ) { 
				mkdir ( $dir_to, 0777 );
				$files = scandir ( $dir );
				foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) $this->gsm_copyFile ( $dir."/".$file, 1, $dir_to. "/". $file ); } 
			} elseif ( file_exists ( $dir ) ) { copy ( $dir, $dir_to ); } 
	} 	} 
	 	
	public function gsm_existDb ( 
		// ============================
		/* create db table table if the file does not yet exist  default or from sql file */
		$project, 		// name sql file
		$name = "-",	// name dB table 
		$prefix = "-"	// default LOAD_DBBASE . "_".
		// ============================
		) { 
		global $database; 
		$returnvalue = '';
		$this->version [ __FUNCTION__ ] = "20240426";   
		$LocalHulpA = ( $name != "-" ) ? $name : $project;
		$fileref = ( $prefix != "-" ) 
			? $prefix . "_" . $LocalHulp 
			: LOAD_DBBASE . "_" . str_replace ( LOAD_DBBASE . "_" , '', $LocalHulpA );
		/* debug * /  Gsm_debug ( array(  $project, $name, $prefix, $returnvalue, $fileref ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$results = array ( );
		// does table exist
		$database->execute_query ( sprintf ( "SHOW TABLE STATUS LIKE '%s'", $fileref ), true, $results );
		if ( count ( $results ) < 1 ) { 
			/* actual addition of DB needed */
			$FileDirS = sprintf ("%s%s%s.sql" , $this->setting [ 'includes' ] , "install/" ,  $fileref);
			if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $FileDirS ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
			if ( file_exists ( $FileDirS ) ) {
				$sql = file_get_contents( $FileDirS );
				$database->simple_query ( $sql );
				$returnvalue .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . __FUNCTION__ .
					' upload '.  $project . ' completed'  . NL;
			} else {
				$sql = "CREATE TABLE IF NOT EXISTS `" . $fileref . "` ( 
					`id` int(11) NOT NULL auto_increment,
					`type` varchar(63) NOT NULL DEFAULT '',
					`ref` varchar(63) NOT NULL DEFAULT '',
					`name` varchar(255) NOT NULL default '',
					`content_short` varchar(255) NOT NULL default '',
					`zoek` varchar (255) NOT NULL default '',
					`active` int(3) NOT NULL DEFAULT '0',			
					`updated` timestamp NOT NULL DEFAULT current_timestamp ON UPDATE current_timestamp,
					PRIMARY KEY (`id`))
					ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
				$database->simple_query ( $sql );	
				$returnvalue .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . __FUNCTION__ .
					' default '.  $fileref . ' installed' . NL;
		}	} 
		return $returnvalue;
	}
	
	public function gsm_existDir ( 
		// ============================
		$directory, // directory to be checked for existence
		$index = false // index to be copied
		// ============================
		) { 
		$returnvalue = "";
		$this->version [ __FUNCTION__ ] = "20240426";  
		$dir = LEPTON_PATH . str_replace ( array( LEPTON_PATH, LEPTON_URL) , '', $directory );
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array( $directory, $dir ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( !file_exists ( $dir ) ) {
			$LocalHulpA = explode ( "/", str_replace ( array( LEPTON_PATH, LEPTON_URL) , '', $directory ) ) ;
			$FileDirS = LEPTON_PATH;
			foreach( $LocalHulpA as $payload ) {
				if ( strlen ( $payload) >0 ) {
					$FileDirS .= "/". $payload;
					if ( !file_exists ( $FileDirS ) ) {
						mkdir ( $FileDirS, 0777 ); 
						$returnvalue .= "<br />" . date ( "H:i:s " ) . __LINE__  . ' dir: '.  $FileDirS . ' created' ;
					}
					if ( $index && !file_exists ( $FileDirS. "index.php" ) ) {
						copy  ( ( dirname ( __FILE__ ) ) . '/index.php' , $FileDirS. "/index.php");
						$returnvalue .= "<br />" .  date ( "H:i:s " ) . __LINE__  . ' index: ' .  $FileDirS . ' created' ;
		}	}	}	}
		return $returnvalue;
	}
	
	public function gsm_guid (
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20230808";  
		if ( function_exists ( 'com_create_guid' ) === true ) { return trim ( com_create_guid ( ) , ' { } ' ); } 
		return sprintf ( '%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand ( 0, 65535 ) , mt_rand ( 0, 65535 ) , mt_rand ( 0, 65535 ) , mt_rand ( 16384, 20479 ) , mt_rand ( 32768, 49151 ) , mt_rand ( 0, 65535 ) , mt_rand ( 0, 65535 ) , mt_rand ( 0, 65535 ) );
	} 
	
	public function gsm_initTaxo ( 
		// ============================	
		// create a set of settings values from taxonomy file
		// format ref
		// key
		// key|gsmoff_
		// key|gsmoff_|n
		$application,
		$privileged,
		$Set_file,
		$group = 'setting'
		// ============================
		) {  
		global $database;
		$this->version [ __FUNCTION__ ] = "20240426";  
		// 20230727 addition of group parameter
		$arrayARY = array( "entity", "allowed" );
		foreach ( $Set_file as $pay => $load ) $this->setting [ str_replace ( "SET_" , "", $pay) ] = $load ;
		$this->gsm_existDb ( $this->file_ref  [ 1 ] );
		/* database present */
		$TEMP1  = "SELECT `ref`, `name` FROM `%s` WHERE `type` LIKE '%s' AND `active` = 1 ORDER BY `zoek` ASC ";
		$results = array ( );
		$database->execute_query ( 
			sprintf ($TEMP1, $this->file_ref  [ 1 ], $group ), 
			true, 
			$results );
		foreach ( $results as $row ) {
			$SET = false;
			$LocalHulp = explode ( "|" , $row [ 'ref' ] );
			if ( isset ( $LocalHulp [ 2 ] ) ) {
				$key = $LocalHulp [ 0 ];
				$app = $LocalHulp [ 1 ];
				$prv = $LocalHulp [ 1 ];
				if ($group == 'setting' ) {
					if ( $LocalHulp [ 1 ] == $application && $LocalHulp [ 2 ] < $privileged ) $this->setting  [ $key ]= $row [ 'name' ];
				} else {
				}
			} elseif ( isset ( $LocalHulp [ 1 ] ) ){
				$key = $LocalHulp [ 0 ];
				$app = $LocalHulp [ 1 ];
				if ($group == 'setting' ) {
					if ( $LocalHulp [ 1 ] == $application ) $this->setting [ $key ] = $row [ 'name' ];
				} else {
					if ( $LocalHulp [ 1 ] == $application ) $this->setting [ $group ] [ $key ] = $row [ 'name' ];
				}
			} else {
				$key = $LocalHulp [ 0 ];
				if ($group == 'setting' ) {
					if ( !isset ( $this->setting [ $key ] ) ) $this->setting [ $key ] = $row [ 'name' ];
				} else {
					if ( !isset ( $this->setting [ $group ] [ $key ] ) ) $this->setting [ $group ] [ $key ]= $row [ 'name' ];
		}	}	}
		foreach ( $arrayARY as $payload) {
			if ( isset ( $this->setting [ $payload ] ) ) {
				if ( !is_array ( $this->setting [ $payload ] ) ) {
					$this->setting [ $payload ] = explode ( "|",$this->setting [ $payload ] );
	}	}	}	}
	
		public function gsm_initTool ( 
		// ============================	
		// create a set of inital values for the twig output
		// ============================
		) {  
//		global $database;
		$this->version [ __FUNCTION__ ] = "20240424";  
		$this->page_content [ 'COLOR' ] = 'blue';
		$this->page_content [ 'ICON' ] = 'settings icon';
		$this->page_content [ 'SUB_HEADER' ] = "____";
		$this->page_content [ 'MESSAGE_CLASS' ] = '';
		$this->page_content [ 'STATUS_MESSAGE' ] = '';
		$this->page_content [ 'MESSAGE' ] = '';
		$this->page_content [ 'FORM_CLASS' ] = '';
		$this->page_content [ 'RETURN' ] = LOAD_RETURN;
		$this->page_content [ 'MODE' ] = 0;
		$this->page_content [ 'DATE' ] = date ( "Y-m-d", time ( ) );
		$this->page_content [ 'TIME' ] = date ( "H:i", time ( ) );
		$this->page_content [ 'HASH' ] = sha1( MICROTIME() . $_SERVER[ 'HTTP_USER_AGENT' ] );
		$this->page_content [ 'RECID' ] = $this->recid;
		$this->page_content [ 'MEMORY' ] = '';
		$this->page_content [ 'DESCRIPTION' ] = '';
		$this->page_content [ 'SELECTION' ] = '';
		$this->page_content [ 'RAPPORTAGE' ] = '';
		$this->page_content [ 'TOEGIFT' ] = '';
		$this->page_content [ 'SIPS' ] = $this->sips;
		$this->page_content [ 'PARAMETER' ] = ( isset( $_POST[ 'selection' ] ) ) ? $_POST[ 'selection' ] : "";
		$this->page_content [ 'PRIVILEGED' ] = $this->user [ 'privileged' ];
		$this->page_content [ 'P1' ] = false;
		$this->page_content [ 'OPTIONAL' ] = '';
		$this->page_content [ 'LOAD' ] = LOAD_MODE;
		
		/* paging */	
		$this->page_content [ 'POSITION' ] = 0;
	}
	
	public function gsm_mail ( 
		// ============================
		$template, 
		$toArr, 
		$parseArr = '', 
		$func=1, 
		$subject='', 
		$content='', 
		$attachments=array ( )
		// ============================
		) { 
		// func 1= mail to persons in toArr
		// func 2= mail to master bcc toArr	
		// tw0 calls to this function adds up the to addreses warning
		$this->version [ __FUNCTION__ ] = "20240426";  
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array (	'template' => $template, 'toArr' => $toArr, 'parseArr' => $parseArr, 'func'=> $func, 'subject' => $subject,	'content' => LOAD_OLINE, 'attachments' => $attachments), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$returnvalue='<div class="ui icon message"><i class="inbox icon"></i><div class="content">';
		$oke=true;
		if ( $toArr =='' ) { 
			$oke=false;
			$returnvalue .= '<div class="header">Mail adres missing</div>';
		 } 
		$myMail = new LEPTON_mailer ( ); //lepton version 3 code 
		if ( !LOAD_OLINE ) $returnvalue .= '<h3 class="ui header">Start Mail content</h3>';
		// From
		$myMail->CharSet = DEFAULT_CHARSET;	
		$myMail->setFrom ( $this->setting [ 'droplet' ] [ LANGUAGE. '10' ], $this->setting [ 'droplet' ] [ LANGUAGE. '4' ] );
		$returnvalue .= '<h3 class="ui header">from: '. $this->setting [ 'droplet' ] [ LANGUAGE. '10' ] .'</h3>';
		$myMail->addReplyTo ( $this->setting [ 'droplet' ] [ LANGUAGE. '10' ], $this->setting [ 'droplet' ] [ LANGUAGE. '4' ] );
		// To
		$temp = is_array ( $toArr ) ? $toArr : array ( $toArr ); // ensure data is always array
		switch ( $func ) { 
			case 1:
				//to persons in toArr
				foreach ( $temp as $key => $value ) $myMail->addAddress ( $value );
				$returnvalue .= '<h3 class="ui header">to: '. $value.'</h3>';
				break;
			case 2: //to master bcc to persons in toArr
				$myMail->IsHTML ( true );
			default:
				$myMail->addAddress ( $this->setting [ 'email' ], $this->setting [ 'master' ] );
				foreach ( $temp as $key => $value ) $myMail->addBCC ( $value );
				$returnvalue .= '<h3 class="ui header">to: '. $this->setting [ 'email' ].'</h3>';
				$returnvalue .= '<h3 class="ui header">bcc: '. $value.'</h3>';
				break;
		 } 
		// template ophalen 
		if ( $subject=='' ) { // template is a php file with two fields $mail_subject and $mail_content
			include ( $this->setting [ 'frontend' ]."templates/" . LANGUAGE ."/". $template );
		 } else { 
			$mail_subject = $subject;
			$mail_content = $content;
		 } 
		$mail_subject = Gsm_prout ( $mail_subject , $parseArr ); // The Subject of the message.
		$myMail->Subject =  $mail_subject;
		$mail_content = Gsm_prout ( $mail_content , $parseArr );
		$myMail->Body = $mail_content; // Clients that can read HTML will view the normal Body.
		$textbody = wordwrap ( $mail_content, 60, '<br />', true );
		$textbody = strip_tags ( $textbody  ); 
		$textbody = str_replace ( "\t","",$textbody ); 
		while ( strpos ( $textbody,"\n\n\n" ) !== false ) $textbody = str_replace ( "\n\n\n","\n\n",$textbody ); 
		while ( strpos ( $textbody,"\r\n\r\n\r\n" ) !== false ) $textbody = str_replace ( "\r\n\r\n\r\n","\r\n\r\n",$textbody ); 
		$myMail->AltBody = $textbody; // This body can be read by mail clients that do not have HTML email
		$returnvalue .= '<h3 class="header">re: '. $mail_subject.'</h3>';
		$returnvalue .= '<h3 class="ui header">tx: </h3>'. $mail_content;	
		$myAtt = is_array ( $attachments ) ? $attachments : array ( $attachments ); // ensure data is always array
		// Attachments 
		if ( count ( $myAtt ) ) { 
			foreach ( $myAtt as $key => $value ) { 
				$mail->addAttachment ( $key, $value ); 
				$returnvalue .= '<h3 class="ui header">att: '. $value. '</h3>';
		 }	 } 
		if ( !LOAD_OLINE || !$oke ) { 
			$returnvalue .= '<div class="header"> Not mailed !! </div></div></div>';
			if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array (	$returnvalue, $oke ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		 } else { 
			if ( !$myMail->send ( ) ) { 
				$returnvalue .= sprintf ( '<div class="ui icon message"><i class="notched circle loading icon"></i><div class="content"><div class="header">Not mailed !! </div><p>%s</p></div></div>', $myMail->ErrorInfo );
			}	else  {
				$returnvalue = "";
			}
		} 
		return $returnvalue; 
	 }	
			 
	public function gsm_memorySaved (
		// ============================
		$func = 1 		// 1= retrieve  2 pack and save  3 pack for post
		// ============================
		) { 
		/* *
		 *  Read an save data $this->memory [ 1 ] ~ $this->memory [ 5 ]
		 *	location collectdir
		 *  func 0 retrieve session relevant directory or post
		 *  func 1 retrieve from post or session relevant directory
		 *  func 2 compact the data and transfer via $Post command
		 *  func 3 compact the data save for the module during the session
		 *  
		 */
		$this->version [ __FUNCTION__ ] = "20240616";  
		$FileDirS = sprintf ( "%s%s/%s/%s_%s" , LEPTON_PATH, $this->setting [ 'collectdir' ], date ( "Y-m-d", time ( ) ), $this->setting [ 'owner' ], session_id ( ) );
		$FileMEMORY = sprintf ( "%s/%s.html", $FileDirS , $this->page_content [ 'MODULE' ] );
		if ($func == 0 ) {
			if ( file_exists ( $FileMEMORY ) ) { 
				$FileData = file ( $FileMEMORY ); 
				$this->memory = explode ( "|" , $FileData [ 0 ] );
			} else { 
				// case after midnight
				$FileDirS = sprintf ( "%s%s/%s/%s_%s" , LEPTON_PATH, $this->setting [ 'collectdir' ], date ( "Y-m-d", strtotime ( "yesterday" ) ), $this->setting [ 'owner' ], session_id ( ) );
				$FileMEMORY = sprintf ( "%s/%s.html", $FileDirS , $this->page_content [ 'MODULE' ] );
				if ( file_exists ( $FileMEMORY ) ) { 
					$FileData = file ( $FileMEMORY ); 
					$this->memory = explode ( "|" , $FileData [ 0 ] );
				} elseif ( isset ( $_POST [ 'memory' ] ) &&  substr ( $_POST [ 'memory' ], 0, 5 ) == "Begin" ) { 
					$this->memory = explode ("|" , $_POST[ 'memory' ] ); 
				} 
			}
			$FileData = $FileData [ 0 ] ?? '';
		}
		if ($func == 1) {
			if ( isset ( $_POST [ 'memory' ] ) &&  substr ( $_POST [ 'memory' ], 0, 5 ) == "Begin" ) { 
				$this->memory = explode ("|" , $_POST[ 'memory' ] );
			} else {
				if ( file_exists ( $FileMEMORY ) ) { 
					$FileData = file ( $FileMEMORY ); 
					$this->memory = explode ( "|" , $FileData [ 0 ] );
				} else { 
					// case after midnight
					$FileDirS = sprintf ( "%s%s/%s/%s_%s" , LEPTON_PATH, $this->setting [ 'collectdir' ], date ( "Y-m-d", strtotime ( "yesterday" ) ), $this->setting [ 'owner' ], session_id ( ) );
					$FileMEMORY = sprintf ( "%s/%s.html", $FileDirS , $this->page_content [ 'MODULE' ] );
					if ( file_exists ( $FileMEMORY ) ) { 
						$FileData = file ( $FileMEMORY ); 
						$this->memory = explode ( "|" , $FileData [ 0 ] );
			}	}	}
			$FileData = $FileData [ 0 ] ?? '';
		} else {
			$FileData = sprintf ( "Begin|%s|%s|%s|%s|%s|Einde", $this->memory [ 1 ] ?? '', $this->memory [ 2 ] ?? '', $this->memory [ 3 ] ?? '', $this->memory [ 4 ] ?? '', $this->memory [ 5 ] ?? ''); 
			if ( $func == 3) {
				$this->gsm_existDir ( $FileDirS );
				file_put_contents ( $FileMEMORY, $FileData );
		}	}
		if ( isset ( $this->memory [ 6 ] ) && $this->memory [ 6 ] != "Einde" ) $this->description .= $this->language [ 'TXT_ERROR_MEMORY' ] ;
		for ( $i = 1; $i < 6 ; $i++ ) { if ( !isset ( $this->memory [ $i ] ) ) $this->memory [ $i ] = 0; }
		return $FileData;
	}
	
	public function gsm_opmaakSel ( 
		// ============================
		$funcArr,  			// view save add delete print reset Select
		$parameter = "-",	// parameter bij select
		$pdf = "-",  		// file name pdf file
		$pos = "-",  		// position in de file
		$aantal = "-",  	// afmeting van de file 
		$max = "-", 		// max per pagina
		$text = "-",   		// test bij select / parameter invoer
		$dir = "-"   		// pdf directory
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426";  
		$temp = is_array ( $funcArr ) ? $funcArr : array ( $funcArr );
		$temp_para	= ( $parameter != "-" ) ? $parameter : "";
		$temp_pdf	= ( strlen ($pdf) > 3 ) ? $pdf : "";
		$temp_pos	= ( $pos != "-" ) ? intval ( $pos ) : 0;
		$temp_aant	= ( $aantal != "-" ) ? intval ( $aantal ) : 0;
		$temp_max	= ( $max != "-" ) ? intval ( $max ) : 0;
		$temp_text	= ( $text != "-" ) ? $text : "select";
		$temp_dir	= ( $dir != "-" ) 
			? LEPTON_URL . $dir . $temp_pdf 
			: sprintf ( "%s%s/%s/pdf/%s", LEPTON_URL, MEDIA_DIRECTORY, LOAD_MODULE, $pdf );
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $temp, $parameter, $pdf, $aantal, $max, $text, $temp_dir ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$TEMPL [ 0 ] = '<button class="%1$s ui button" name="command" value="%2$s" type="submit">%3$s</button>';
		$TEMPL [ 1 ] = '<div class="ui action input">
						<button class="%1$s ui button" name="command" type="submit" value="%2$s" type="submit">%3$s</button>
						<input type="text" name="selection" value="%4$s" placeholder="Parameter..." /><i class="info icon" data-tooltip="%5$s"><i class="info circle icon"></i></i>
					</div>';
		$TEMPL [ 2 ] = '<a target="125" href="%2$s"><img src="%3$s" alt="pdf document">%1$s</a>';
		$TEMPL [ 3 ] = '<div class="fields">
						<div class="field">
							<button class="ui button" name="command" value="down"><i class="angle left icon"></i></button>
						</div>
						<div class="field">
							<input type="hidden" name="n0" value="%1$s" />
							<input type="text" name="n1" size="3" value="%1$s" />
							<input type="hidden" name="n2" value="%3$s" />
						</div>
						<div class="field">
							<button class="ui basic button">tot %2$s van %3$s</button>
						</div>
						<div class="field">
							<button class="ui button" name="command" value="up"><i class="angle right icon"></i></button>
						</div>
					</div>';
		$temp_pos = $temp_pos + 1;
		$temp_end = $temp_pos + $temp_max -1 ;
		if ( $temp_end > $temp_aant ) $temp_end = $temp_aant;
		$returnvalue = '';
		if (in_array( 1, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"View" , 	$this->language [ 'tbl_icon' ] [ 1 ] ); 
		if (in_array( 2, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"Save" , 	$this->language [ 'tbl_icon' ] [ 4] ); 
//		if (in_array( 3, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "positive",	"Add" , 	$this->language [ 'tbl_icon' ] [ 3 ] ); 
//		if (in_array( 4, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "negative",	"Delete" , 	$this->language [ 'tbl_icon' ] [ 2 ] );
		if (in_array( 5, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "basic",		"Print" , 	$this->language [ 'tbl_icon' ] [ 11 ] );
		if (in_array( 6, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "negative",	"Reset" , 	$this->language [ 'tbl_icon' ] [ 2 ] );
		if (in_array( 7, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"Proces" , 	$this->language [ 'tbl_icon' ] [ 21 ] );
		if (in_array( 8, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "positive",	"New" , 	$this->language [ 'tbl_icon' ] [ 5 ] );
		if (in_array( 9, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "",			"Set" , 	$this->language [ 'tbl_icon' ] [ 18 ] );
		if (in_array( 10,$temp ) ) $returnvalue .= sprintf ( $TEMPL [ 1 ], "secondary",	"Select" , 	$this->language [ 'tbl_icon' ] [9], 
			$temp_para , 
			$temp_text ).NL.NL;
		if (in_array( 11, $temp ) && strlen ( $temp_pdf ) > 3 ) 
			$returnvalue .= sprintf ( $TEMPL [ 2 ], 
				$temp_pdf, 
				$temp_dir, 
				sprintf ( "%s/modules/%s%s/img/pdf_16.png", LEPTON_URL, LOAD_MODULE, LOAD_SUFFIX ) );
		if (in_array( 13, $temp ) && $temp_aant > $temp_max ) 
			$returnvalue .= sprintf ( $TEMPL [ 3 ], 
				$temp_pos, 
				$temp_end, 
				$temp_aant ).NL.NL; 
		return $returnvalue;
	}

	public function gsm_pagePosition ( 
		// ============================	
		// handling the paging $position = $this->page_content [ 'position' ]
		$func,			// up of down of sql
		$position, 		// position
		$aantal = 0,	// aantal records
		$max = 60,		// max setting
		$n0 = 0,		// return
		$n1 = 0			// return
		// ============================
		) {  
		global $database; 
		$this->version [ __FUNCTION__ ] = "20240428"; 
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array(  "func" => $func, "pos" => $position, "aantal" => $aantal, "max" => $max, "oud pos" => $n0, "new pos" => $n1 ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
 		$returnvalue = 0;
		switch ( $func ) {
			case "up":
				if ( $_POST[ 'n0' ] == $_POST[ 'n1' ]) {
					$position = $_POST[ 'n0' ] + $max  -1;
				} else {
					$position = $_POST[ 'n1' ] -1;
				}
				if ( $aantal > 0 && $position > ( $aantal + 1) ) $position  = 0;
				$returnvalue = $position;
				break;
			case "down":
				if ( $_POST[ 'n0' ] == $_POST[ 'n1' ]) {
					$position = $_POST[ 'n0' ] - $max -1;
				} else {
					$position = $_POST[ 'n1' ] -1;
				}
				if ( $position < 0 ) $position  = 0;
				$returnvalue = $position;
				break;
			case "sql":
				$returnvalue = "";
				if ( $position > $aantal ) $position = 0;
				if ( $aantal > $max ) $returnvalue = sprintf (" LIMIT %s, %s ", $position, $max );
				break;
		}
		return $returnvalue;
	}
		
	public function	gsm_pinCode ( 
		// ============================
		$input , 
		$func = 1, 
		$update = array ( )
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20240510";  
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array(  $input, $func, $update, $this->setting [ 'collectdir' ], $this->setting [ 'owner' ], session_id ( ) ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		// toegangs controle tfa 
		$returnvalue = false;
		$UserVolg = is_array ( $input ) ? $input : array ( $input );  // eerste parameter is mailadres tweede parameter is the name
		$job = is_array ( $update ) ? $update : array ( $update ); // jobs to execute when verification code is entered
		$FileDirS = sprintf ( "%s%s/%s/%s_%s" , LEPTON_PATH, $this->setting [ 'collectdir' ], date ( "Y-m-d", time ( ) ), $this->setting [ 'owner' ], session_id ( ) );
		$FileTFA = sprintf ( "%s/tfa.html", $FileDirS);
		$FilePAY = sprintf ( "%s/payload.html", $FileDirS);
		$FileINDto = sprintf ( "%s/index.php", $FileDirS);
		$FileINDfrom = sprintf ( "%s%s/index.php" , LEPTON_PATH, MEDIA_DIRECTORY );
		$Verif_string = "verificatiecode %s";
		switch ( $func ) { 
			case 1: // create file return true;
				/* create directory collectdir : owner sessionID
				 * if tfa file present get back the seq of verification number (first element 0) end
				 * create tfa seq , key, time, email, name
				 * add index file
				 * is er een job ? create payload file 
				 * send mail to gsm_mail en web_mail
				 */
				 $this->gsm_existDir ( $FileDirS );
				if ( file_exists ( $FileTFA ) ) { 
					// existing file pick up file return part 1 = seq number
					$tfa = file ( $FileTFA );
					$LhulpD = explode ( "|", current ( $tfa ) );
					$returnvalue = $LhulpD [ 0 ];
					// the input is the verification code ?? 
					if ( $UserVolg [ 0 ] == $LhulpD [ 1 ] ) {
						$returnvalue = true;
						// get he payload fle and execute the jobs
						if ( file_exists ( $FilePAY ) ) { 
							$job = file ( $FilePAY );
							if ( isset ( $job ) && count ( $job ) > 0 ) { 
								foreach ( $job as $key => $query ) $database->simple_query ( $query );
							 } 
							unlink ( $FilePAY );
						} 
						// remove blocking item and job
						if ( file_exists ( $FileTFA ) ) unlink ( $FileTFA );
						if ( file_exists ( $FileINDto ) ) unlink ( $FileINDto );
					}
				 } else { 
					// email mandatory
					$UserVolg [ 0 ] = $this->gsm_sanitizeStringS ( $UserVolg [ 0 ] , "s{EMAIL}" );
					// create file and mail
					if ( $UserVolg [ 0 ] !== false ) {
						$LhulpD = date ( 'U' );
						$LhulpE = implode ( "|", $UserVolg ); 
						$returnvalue = sprintf ( $Verif_string, substr ( $LhulpD, -2 ) );
						$payload = sprintf ( "%s|%s|%s|%s" , $returnvalue , substr ( $LhulpD, -6,5 ) , $LhulpD, $LhulpE );
						file_put_contents ( $FileTFA , $payload );
						// index file for extra safety
						if ( file_exists ( $FileINDfrom ) ) copy ( $FileINDfrom , $FileINDto );
						// write job
						if ( count ( $job ) > 0 ) foreach ( $job as $pay => $load ) file_put_contents ( $FilePAY, $load . "\r\n" , FILE_APPEND | LOCK_EX );
						// mail
						$LhulpF = explode ( "|", $payload );
						$parseArr = array ( );
						$parseArr [ 'GSM_EMAIL' ] = $LhulpF [ 3 ];
						$parseArr [ 'GSM_NAME' ] = $LhulpF [ 4 ] ?? $LhulpF [ 3 ];
						$parseArr [ 'WEB_MASTER' ] = $this->setting [ 'master' ];
						$parseArr [ 'WEB_EMAIL' ] = $this->setting [ 'email' ];
						$parseArr [ 'WEB_SITE' ] = LEPTON_URL;
						$parseArr [ 'GSM_LINK' ] = sprintf ( "%s : %s " , $LhulpF [ 0 ] , $LhulpF [ 1 ] );
						$parseArr [ 'WEB_TIMESTAMP' ] = date ( "Y-m-d H:i" );
						$template="mail_verificatie.php";
						$this->description .= $this->gsm_mail ( $template, $parseArr [ 'GSM_EMAIL' ] , $parseArr );
						$this->description .= $this->gsm_mail ( $template, $parseArr [ 'WEB_EMAIL' ] , $parseArr );
					}
				} 
				break;
			case 2: // existing file pick up file return part 1 = seq number
				$returnvalue = '';
				if ( file_exists ( $FileTFA ) ) { 
					$tfa = file ( $FileTFA );
					$LhulpF = explode ( "|", current ( $tfa ) );
					$returnvalue = $LhulpF [ 0 ];
				 } 
				break;
			case 3: // get payload
				if ( file_exists ( $FileTFA ) ) { 
					$tfa = file ( $FileTFA );
					$LhulpF = explode ( "|", current ( $tfa ) );
					$returnvalue = $LhulpF;
				}
				break;
			case 4: // mail again
				if ( file_exists ( $FileTFA ) ) { 
					$tfa = file ( $FileTFA );
					$LhulpF = explode ( "|", $tfa );
					$parseArr = array ( );
					$parseArr [ 'GSM_EMAIL' ] = $LhulpF [ 3 ];
					$parseArr [ 'GSM_NAME' ] = $LhulpF [ 4 ] ?? $LhulpF [ 3 ];
					$parseArr [ 'WEB_master' ] = $this->master;
					$parseArr [ 'WEB_EMAIL' ] = $this->standard_email;
					$parseArr [ 'WEB_SITE' ] = LEPTON_URL;
					$parseArr [ 'GSM_LINK' ] = sprintf ( "%s : %s " , $LhulpF [ 0 ] , $LhulpF [ 1 ] );
					$parseArr [ 'WEB_TIMESTAMP' ] = date ( "Y-m-d H:i" );
					$template="mail verificatie.php";
					$this->description .= $this->gsm_mail ( $template, $parseArr [ 'GSM_EMAIL' ] , $parseArr );
					$this->description .= $this->gsm_mail ( $template, $parseArr [ 'WEB_EMAIL' ] , $parseArr );
				}
				break;	
			case 9: // remove files
				$returnvalue = '';
				if ( file_exists ( $FilePAY ) )	unlink ( $FilePAY );
				if ( file_exists ( $FileTFA ) )	unlink ( $FileTFA );
				if ( file_exists ( $FileINDto ) ) unlink ( $FileINDto );
				break;
		}
		return $returnvalue;
	}
	
	public function gsm_print ( 
		// ============================	
		$query = "-", 
		$project = "-", 
		$selection = "-", 
		$func = 1, 
		$loc ="-"
		// ============================
		) { 
		global $database;
		$returnvalue ='';
		$this->version [ __FUNCTION__ ] = "20240810";  
		if ( $func > 4 ) {
			require_once ( $this->setting [ 'includes' ] . 'classes/' . 'pdf2.inc' );
		} else {
			require_once ( $this->setting [ 'includes' ] . 'classes/' . 'pdf.inc' );
		}
		$pdf = new PDF ( );
		global $owner;
		$owner = $this->setting [ 'owner' ];
		global $title;
		$title = ucfirst ( $project );
		$location = ( $loc == '-' )
			? sprintf ( '%s/%s/pdf' , MEDIA_DIRECTORY, LOAD_MODULE )
			: $loc; // gsm 20230501
		$run = date ( DATE_FORMAT, time ( ) ) . " " . date ( TIME_FORMAT, time ( ) );
		$check = $this->setting [ 'includes' ] . $this->page_content [ 'MODULE' ] . '_print' . $func . '.php';
		$checkv = $this->setting [ 'includes' ] . "v" . substr( $this->page_content [ 'MODULE' ],1 ) . '_print' . $func . '.php';
		$check_legacy = $this->setting [ 'includes' ] . $this->page_content [ 'MODULE' ] . '_data' . $func . '.inc';
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $query, $project, $selection, $func, $loc, $owner,$title, LEPTON_PATH, MEDIA_DIRECTORY , LOAD_MODULE), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );
		if ( file_exists ( $check ) ) { 
			require_once ( $check ); 
			$pdflink = sprintf ( "%s%s/%s", LEPTON_PATH, $location, $this->setting [ 'pdf_filename' ] );  
			$pdf->Output ( $pdflink, 'F' );
			return $this->setting [ 'pdf_filename' ];
		/* support backend */
		} elseif ( file_exists ( $checkv ) ) { 
			require_once ( $checkv ); 
			$pdflink = sprintf ( "%s%s/%s", LEPTON_PATH, $location, $this->setting [ 'pdf_filename' ] ); 
			$pdf->Output ( $pdflink, 'F' );
			return $this->setting [ 'pdf_filename' ];
		/* legacy */
		} elseif ( file_exists ( $check_legacy ) ) { 
			require_once ( $check_legacy ); 
			$this->description .= date ( "H:i:s " ) . __LINE__  . ' legacy : '.  $check_legacy  . NL;	
			$pdflink = sprintf ( "%s%s/%s", LEPTON_PATH, $location, $this->setting [ 'pdf_filename' ] );  // gsm 20230501
			$pdf->Output ( $pdflink, 'F' );
			return $this->setting [ 'pdf_filename' ];
		/* end legacy */
		} else {
			$this->description .= date ( "H:i:s " ) . __LINE__  . ' missing : '.  $check  . NL;	
			return $returnvalue;
		}	
	}

	public function gsm_sanitizeStringD ( 
		// ============================
		$input, 	// gsm_sanitizeStringD ( $value, 'y{1970-01-01;1940-01-01;2018-03-11}' )
		$filter 	// gsm_sanitizeStringD ( $value, "t{12:00}" )
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426"; 
		if ( $this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $input, $filter ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( !preg_match ( '#(y|t|f)#i', $filter, $match ) ) { 
			$message = $this->language [ 'TXT_ERROR_DATA' ] . "Filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
			die ( $message );
		 } 
 
		$temp = is_array ( $input ) ? $input : array ( $input );
		$filter_type = strtolower ( $match [ 1 ] );
		switch ( $filter_type ) { 
			case 'y':// date filter ( $input can be single value or array ) // standard 20200202
				// gsm_sanitizeStringD ( "10-3-2018", 'y{1970-01-01;1940-01-01;2018-03-11}' ) 
				// gsm_sanitizeStringD ( 00:00, "t{12:00}" );
				// check if optional filter values are supplied ( default, min, max ) 
				// dates > 100 years ago are not supported
				$advanced_filter = ( preg_match ( '#(y)\{([-0-9]+);([-0-9]+);([-0-9 ]+)\}#i', $filter, $match ) );
				$datephp8byp = -1600000000; $datephp8bypYMD = date ( "Y-m-d", $datephp8byp );
				if ( $advanced_filter ) { 
					if ($match [ 2 ] < date ( $datephp8bypYMD ) ) $match [ 2 ] = $datephp8bypYMD;
					$default =	strtotime ( $match [ 2 ] );
					$lowerlimit = strtotime ( $match [ 3 ] );
					$upperlimit = strtotime ( $match [ 4 ] );
				} 
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					// force date conversion
					if ($temp [ $key ] < $datephp8bypYMD ) 	$temp [ $key ] = $datephp8bypYMD;
					$originalDate = strtotime ( $temp [ $key ] );		
					// check if value is within min/max range, if not use default value
					if ( $advanced_filter ) { 
						$originalDate = ( $originalDate >= $lowerlimit && $originalDate <= $upperlimit ) ? $originalDate : $default;			
					 } 
					// to required format
					$temp [ $key ] = ( $originalDate > $datephp8byp ) ? date ( "Y-m-d", $originalDate ) : "0000-00-00";	
				} 
				break;
			case 't':// time filter ( $input can be single value or array ) // standard 20200202
				// gsm_sanitizeStringD ( 00:00, "t{12:00}" );
				// loop over input values
				$advanced_filter = ( preg_match ( '#(t)\{([-0-9]+):([-0-9]+)\}#i', $filter, $match ) );
				foreach ( $temp as $key => $value ) { 
					// force date conversion split based on 
					$temp [ $key ] = preg_replace( '/[^0-9h:.,]/i', '', $temp [ $key ]);	
					$temp [ $key ] = preg_replace( '/[h:.,]/i', ':', $temp [ $key ]);
					$lcHulp = explode ( ":" , $temp [ $key ] );
					if ( isset ($lcHulp [0]) && is_numeric ($lcHulp [0])) {
						$lcHulp [0] = Substr ( "00" . $lcHulp [0] % 24, -2);
					} else {
						$lcHulp [0] = $match [2] ;
					}	
					if ( isset ($lcHulp [1]) && is_numeric ($lcHulp [1])) {
						$lcHulp [1] = Substr ( "00" . $lcHulp [1] % 60, -2);
					} else {
						$lcHulp [1] = $match [3] ;
					}	
					$temp [ $key ] = sprintf ("%s:%s", $lcHulp [0], $lcHulp [1] );
				} 
				break;
			case 'f':// date opmaak 
				if ( !preg_match ( '#(f)\{(.+)\}#i', $filter, $match ) ) {
					$message =  $this->language [ 'TXT_ERROR_DATA' ] . "String filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
					die ( $message );
				} 
				// get filter options from regular expression
				$filter_options = strtoupper ( $match [ 2 ] );
				// strings
				$monthUK = array ( "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" );
				$dayUK =  array ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" );
				$monthNL = array ( "jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "oct", "nov", "dec" );
				$dayNL = array ("Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag" );
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					if ( strpos ( $filter_options, 'MAAND' ) !== false ) { 
						$temp [ $key ] = date ("d M Y" ,strtotime ( $temp [ $key ] ) ); 
						$temp [ $key ] = str_replace( $monthUK, $monthNL, $temp [ $key ] );
					}
					if ( strpos ( $filter_options, 'DAG' ) !== false ) { 
						$temp [ $key ] = date ("D, d M Y" ,strtotime ( $temp [ $key ] ) ); 
						$temp [ $key ] = str_replace( $monthUK, $monthNL, $temp [ $key ] );
						$temp [ $key ] = str_replace( $dayUK, $dayNL, $temp [ $key ] );
					}
					if ( strpos ( $filter_options, 'D1' ) !== false ) { 
						$temp [ $key ] = date ("D" ,strtotime ( $temp [ $key ] ) ); 
						$temp [ $key ] = str_replace( $dayUK, $dayNL, $temp [ $key ] );
					}
				}
				break;
		} 
		$returnvalue = is_array ( $input ) ? $temp : $temp [ 0 ];
		return $returnvalue;
	}	

	public function gsm_sanitizeStringNA (
		// ============================
		$func=1, 
		$input='', 
		$input0='', 
		$input1='', 
		$input2='', 
		$input3=''
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20231028";  
		switch ( $func 	) { 
			case '1': //default val initial field for name and adres
				$returnvalue="|||";
				break;	
			case '2': //string naar array
				$temp = explode ( "|" , $input0 ?? " " );
				$returnvalue = array ( );
				for ( $x = 0; $x < 4; $x++ ) { $y=$x+1; $returnvalue [ $input.$y ] = isset ( $temp [ $x ] ) ? $temp [ $x ] : ''; } 
				break;
			case '3': //string naar string
				$returnvalue = sprintf ( "%s|%s|%s|%s", $input0, $input1, $input2, $input3 );
				break;
			case '4': //remove separator
				$returnvalue = str_replace ( " ", " ", str_replace ( "|"," ", $input ) );
				break;		
			default: 
				$message = $this->language [ 'TXT_ERROR_DATA' ] . "Func: <b>" . $func . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
				die ( $message );
				break;	
		 } 
		return $returnvalue;
	 } 
	
	public function gsm_sanitizeStringS ( 
		// ============================
		$input,		// gsm_sanitizeStringS ( $value, "s{TAGS|TOASC|VINP|VOUT|CLEAN|TRIM|EMAIL|NAME|FILE|PATH|SCHEMA|STRIP|DATUM|KOMMA|STOP|WHOLE|FULL|EURT|EURO|E128|STRONG}" )
		$filter 
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20241028";
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $input, $filter ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( !preg_match ( '#(s)#i', $filter, $match ) ) { 
			$message = $this->language [ 'TXT_ERROR_DATA' ] . "Filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
			die ( $message );
		}  
		$temp = is_array ( $input ) ? $input  : array ( $input );
		$filter_type = strtolower ( $match [ 1 ] );
		
		switch ( $filter_type ) { 
			case 's': // string filter 
				if ( !preg_match ( '#(s)\{(.+)\}#i', $filter, $match ) ) {
					$message =  $this->language [ 'TXT_ERROR_DATA' ] . "String filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
					die ( $message );
				} 
				// get filter options from regular expression
				$filter_options = strtoupper ( $match [ 2 ] );
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					if ( $temp [ $key ] === NULL ) $temp [ $key ] = '';
					if ( strpos ( $filter_options, 'TAGS' ) !== false ) $temp [ $key ] = str_replace ( "<br />", "\n", $temp [ $key ] );
					if ( strpos ( $filter_options, 'STRIX' ) !== false ) $temp [ $key ] = html_entity_decode ( $temp [ $key ] );				
					if ( strpos ( $filter_options, 'STRIP' ) !== false ) $temp [ $key ] = strip_tags ( html_entity_decode ( $temp [ $key ] ) );	
					if ( strpos ( $filter_options, 'TOASC' ) !== false ) { 
//						$good_characters = "a-zA-Z0-9\s`~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]";
						$good_characters = "a-zA-Z0-9\s~!@#$%&*()_+-={}|:<>?,.\/\\\[\]";
						$temp [ $key ] = preg_replace("/[^$good_characters]/", '', $temp [ $key ]);
						$temp [ $key ] = iconv("UTF-8", "ASCII", $temp [ $key ] );
					} 	
/* 	
					if ( strpos ( $filter_options, 'VINP' ) !== false ) 
						$temp [ $key ] = htmlspecialchars ( nl2br ( $temp [ $key ] ) );
					if ( strpos ( $filter_options, 'VOUT' ) !== false ) 
						$temp [ $key ] = strip_tags ( htmlspecialchars_decode ( $temp [ $key ] ) );
 */
					if ( strpos ( $filter_options, 'LOWER' ) !== false ) 
						$temp [ $key ] = strtolower ( trim ( $temp [ $key ] ) );				
					if ( strpos ( $filter_options, 'CLEAN' ) !== false ) 
						$temp [ $key ] = str_replace ( '  ', ' ', str_replace ( array ( '|', ';', '"', "'" ) , " ", $temp [ $key ] ) );
					if ( strpos ( $filter_options, 'TRIM' ) !== false ) $temp [ $key ] = trim ( $temp [ $key ] );
					if ( strpos ( $filter_options, 'EMAIL' ) !== false ) { 
						$temp [ $key ] = strtolower ( trim ( $temp [ $key ] ) );
						if ( empty ( $temp [ $key ] ) ) { $temp [ $key ] = false;
						 } elseif ( !filter_var ( $temp [ $key ] , FILTER_VALIDATE_EMAIL ) ) { $temp [ $key ] = false; } } 
					if ( strpos ( $filter_options, 'NAME' ) !== false ) { 
						$hulp = explode ( "@", $temp [ $key ] );
						$hulp2 = explode ( ".", $hulp [ 0 ] );
						$temp [ $key ] ='';
						foreach ( $hulp2 as $val1a => $val1b ) $temp [ $key ] .= ucfirst ( $val1b ) . " "; } 
					if ( strpos ( $filter_options, 'FILE' ) !== false ) { 
						$bad = array_merge ( array_map ( 'chr', range ( 0,31 ) ) , array ( " ", "&", ";", "'", "<", ">", ":", '"', "/", "\\", "|", "?", "*" ) );
						$temp [ $key ] = str_replace ( $bad, "_", $temp [ $key ] ); } 
					if ( strpos ( $filter_options, 'PATH' ) !== false ) { 
						$temp [ $key ] = str_replace ( '\\', '/', $temp [ $key ] ); } 
					if ( strpos ( $filter_options, 'SCHEMA' ) !== false ) { 
						$hlp1 = round ( $temp [ $key ] * 100 );
						if ( $hlp1 % 100 > 0 ) { 
							if ( $hlp1 % 10 > 0 ) { 
								$temp [ $key ] = number_format ( $temp [ $key ] , 2, '.', '' );
							 } else { $temp [ $key ] = number_format ( $temp [ $key ] , 1, '.', '' ); } 
						 } else { $temp [ $key ] = number_format ( $temp [ $key ] , 0, '.', '' ); } 
					 } 
					if (strpos ($filter_options, 'KLASSE' ) !== false) { 
						$temp [ $key ] = str_replace ("0", '', $temp [ $key ] );
					$temp [ $key ] = substr ( $temp [ $key ], 0, 1 );	}	
					if ( strpos ( $filter_options, 'DATUM' ) !== false ) { 
						$temp [ $key ] = date ("d M Y" ,strtotime ( $temp [ $key ] ) ); 
						$monthUK = array ( "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" );
						$monthNL = array ( "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "october", "november", "december" );
						$temp [ $key ] = str_replace( $monthUK, $monthNL, $temp [ $key ] );}
					if ( strpos ( $filter_options, 'KOMMA' ) !== false ) { $temp [ $key ] = number_format ( $temp [ $key ] , 2, ',', '' ); } 
					if ( strpos ( $filter_options, 'KOM1' ) !== false ) { $temp [ $key ] = number_format ( $temp [ $key ] , 1, ',', '' ); } 
					if ( strpos ( $filter_options, 'STOP' ) !== false ) { $temp [ $key ] = number_format ( $temp [ $key ] , 2, '.', '' ); } 
					if ( strpos ( $filter_options, 'WHOLE' ) !== false ) $temp [ $key ] = floor ( intval ( $temp [ $key ] ) ); 
					if ( strpos ( $filter_options, 'FULL' ) !== false ) { $temp [ $key ] = number_format ( $temp [ $key ] , 2, ',', '.' ); } 
					if ( strpos ( $filter_options, 'EURT' ) !== false ) { $temp [ $key ] = "Euro " . $temp [ $key ]; } 
					if ( strpos ( $filter_options, 'EURO' ) !== false ) { $temp [ $key ] = "€ " . $temp [ $key ]; } 
					if ( strpos ( $filter_options, 'E128' ) !== false ) { $temp [ $key ] = chr(128)." " . $temp [ $key ]; } 
					if ( strpos ( $filter_options, 'STRONG' ) !== false ) { $temp [ $key ] = "<strong>" . $temp [ $key ] . "</strong>"; } 

				}		
				break;	
		 } 
		$returnvalue = is_array ( $input ) ? $temp : $temp [ 0 ];
		return $returnvalue;
	}
	
	public function gsm_sanitizeStringV ( 
		// ============================
		$input, 	// gsm_sanitizeStringV ( $value, "v{0;-1000;10000}" )
		$filter 	// gsm_sanitizeStringV ( $value, "a{0;-1000;10000}" )
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426";
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $input, $filter ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( !preg_match ( '#(v|a)#i', $filter, $match ) ) {
			$message = $this->language [ 'TXT_ERROR_DATA' ] . "Filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
			die ( $message );
		} 
		$temp = is_array ( $input ) ? $input : array ( $input );
		$filter_type = strtolower ( $match [ 1 ] );
		switch ( $filter_type ) { 
			case 'v': // amount filter ( always 2 decimals with separator ) ( $input can be single value or array ) 
				// check if optional filter values are supplied ( default, min, max ) 
				$advanced_filter = ( preg_match ( '#(v)\{([-.0-9]+);([-.0-9]+);([-.0-9]+)\}#i', $filter, $match ) ); 
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					// decimal komma replaces decimale punt
					$plus = true;
					if ( strpos ( $value, '-' ) !== false ) $plus = false;
					$dotPos = strrpos ( $value, '.' );
					if ( $dotPos === false ) $dotPos = strrpos ( $value, '_' );
					$commaPos = strrpos ( $value, ',' );
					$sep = ( ( $dotPos > $commaPos ) && $dotPos ) 
						? $dotPos 
						: ( ( ( $commaPos > $dotPos ) && $commaPos ) 
							? $commaPos 
							: false ); 
					if ( !$sep ) { 
						$temp [ $key ] = floatval ( preg_replace ( "/[^0-9]/", '', $value ) ); 
					} else {
						$temp[$key] = preg_replace ( "/[^0-9]/", '', substr ( $value, 0, $sep ) ) . '.' . preg_replace ( "/[^0-9]/", '', substr ($value, $sep+1, strlen ( $value ) ) );
					}
					if ( !$plus ) $temp [ $key ] = $temp [ $key ] * -1;				
					if ( $advanced_filter ) { 
						$temp [ $key ] = ( $temp [ $key ] >= $match [ 3 ] && $temp [ $key ] <= $match [ 4 ] ) ? $temp [ $key ] : $match [ 2 ];
					 } 
					$temp [ $key ] = number_format ( round ( $temp [ $key ] , 2 ) , 2, '.' , '' ); 
				} 
				break;
			case 'a': // amount filter ( ) ( $input can be single value or array ) 
				// check if optional filter values are supplied ( default, min, max ) 
				$advanced_filter = ( preg_match ( '#(a)\{([-.0-9]+);([-.0-9]+);([-.0-9]+)\}#i', $filter, $match ) ); 
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					// decimal komma replaces decimale punt
					$plus = true;
					if ( strpos ( $value, '-' ) !== false ) $plus = false;
					$dotPos = strrpos ( $value, '.' );
					if ( $dotPos === false ) $dotPos = strrpos ( $value, '_' );
					$commaPos = strrpos ( $value, ',' );
					$sep = ( ( $dotPos > $commaPos ) && $dotPos ) 
						? $dotPos 
						: ( ( ( $commaPos > $dotPos ) && $commaPos ) 
							? $commaPos 
							: false ); 
					if ( !$sep ) { 
						$temp [ $key ] = floatval ( preg_replace ( "/[^0-9]/", '', $value ) ); 
					} else {
						$temp[$key] = preg_replace ( "/[^0-9]/", '', substr ( $value, 0, $sep ) ) . '.' . preg_replace ( "/[^0-9]/", '', substr ($value, $sep+1, strlen ( $value ) ) );
					}
					if ( !$plus ) $temp [ $key ] = $temp [ $key ] * -1;				
					if ( $advanced_filter ) { 
						$temp [ $key ] = ( $temp [ $key ] >= $match [ 3 ] && $temp [ $key ] <= $match [ 4 ] ) ? $temp [ $key ] : $match [ 2 ];
					 } 
					$temp [ $key ] = number_format ( round ( $temp [ $key ] , 0 ) , 0, '.' , '' ); 
				} 
				break;
		 } 
		$returnvalue = is_array ( $input ) ? $temp : $temp [ 0 ];
		return $returnvalue;
	}

	public function gsm_scanDir ( 
		// ============================
		$dir_from, 		// 
		$func = 1, 		// 
		$qty = 50, 		// 
		$allow_sub = 'all', 	//
		$allow_ext = 'all', 	//
		$ref = '' 		//
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426";  
		$fileArray = array ( );
		$exept_all = 0;
		$dir_from = LEPTON_PATH. str_replace ( LEPTON_PATH, "", $dir_from);
		if ( !is_array ( $allow_sub ) || $allow_sub == 'all' ) $exept_all = 1;
		if ( !is_array ( $allow_sub ) || $allow_sub != 'all' ) $exept_all = 2;
		if ( is_array ( $allow_sub ) ) $exept_all = 0;
		if  ($allow_ext == 'all' || $allow_ext == '') { $allow_ext = $this->setting [ 'allowed' ];
		} else { $allow_ext = is_array ( $allow_ext ) ? $allow_ext : array ( $allow_ext ); }
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( "func" => $func, "dir" => $dir_from, "qty" => $qty, "allow_sub" => $allow_sub, "allow_ext" =>  $allow_ext, 	"except_all" => $exept_all, "ref" => $ref ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$n = 0;
		if ( is_dir ( $dir_from ) ) { 
			$dirs = array ( $dir_from );
			while ( NULL !== ( $dir = array_pop ( $dirs ) ) ) { 
				if ( $handle = opendir ( $dir ) ) { 
					while ( false !== ( $file = readdir ( $handle ) ) ) { 
						if ( $file == '.' || $file == '..' ) continue;
							$path = $dir . '/' . $file;
							$ext = strtolower ( substr ( $file, strrpos ( $file, '.' ) + 1 ) );
							if ( is_dir ( $path ) && is_readable ( $path ) && $exept_all == 1 ) { array_unshift ( $dirs, $path );
							 } elseif ( is_dir ( $path ) && is_readable ( $path ) && $exept_all == 2 ) { // do nothing
							 } elseif ( is_dir ( $path ) && is_readable ( $path ) && in_array ( $file, $allow_sub ) ) { array_unshift ( $dirs, $path );
							 } elseif ( in_array ( $ext, $allow_ext ) ) { 
								if ( $qty > 0 && $n < $qty ) { $fileArray [ $file ] = $path; $n++; 
								 } else { 
									$hulpref=explode ( "_", $file ); 
									$h1 = strtoupper ( $hulpref [ 0 ] );
									if ( is_array ( $ref )){
										if (in_array ( substr ( $h1, 0, 2 ) , $ref )) { $fileArray [ $file ] = $path; $n++; }
									} else {
										if ( strtoupper ( $ref ) == $h1 ) { $fileArray [ $file ] = $path; $n++; }
										if ( strtoupper ( $ref ) == ( substr ( $h1, 0, 2 ) ) ) { $fileArray [ $file ] = $path; $n++; 
										} elseif ( strtoupper ( $ref ) == strtoupper( substr ( $file, 0, strlen ( $ref ) ) ) ) { $fileArray [ $file ] = $path; $n++; } 
									}// 2021-06-09
					 }	 }	 }	 } 
					closedir ( $handle );
			}	 } 
			if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( $fileArray ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
			if ( $func == 1 ) return $fileArray;
			krsort ( $fileArray );
			if ( $func == 2 ) return $fileArray;
			if ( $func == 3 ) { 
				$returnvalue = '';
				$nodisplay = true;
				$height = " ";
				$prev = '';
				$clear = $this->language [ 'layout' ] [ 'show1' ] ?: '';
				$full = false;
				if ( count ( $fileArray ) >0 ) $returnvalue .= $this->language [ 'layout' ] [ 'show0' ] ?: '';
				if ( $this->setting [ 'jpgwidth' ] < 201 ) $full = true;
				foreach ( $fileArray as $key => $value ) { 
					$ext = strtolower ( substr ( $key, strrpos ( $key, '.' ) ) );
					if ( $ext == ".pdf" ) { 
						if ( $nodisplay || $prev == ".jpg" ) $returnvalue .= $clear;
						$keyh = str_replace ( ".pdf", "", $key );
						$returnvalue .= '<p><a class="pdf" href="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' " target= "_124"> ' . $keyh . ' </a></p>';
						$nodisplay = false;
						$prev = ".pdf"; 
					 } elseif ( $ext == ".zip" ) { 
						if ( $nodisplay || $prev == ".jpg" ) $returnvalue .= $clear;
						$keyh = str_replace ( ".zip", "", $key );
						$returnvalue .= '<p><a class="zip" href="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' "> ' . $keyh . '</a></p>';
						$nodisplay = false;
						$prev = ".zip"; 
					 } elseif ( $ext == ".mp4" ) { 
						if ( $nodisplay || $prev == ".jpg" ) $returnvalue .= $clear;
						$keyh = str_replace ( ".mp4", "", $key );
						$returnvalue .= '<p><a class="mp4" href="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' "> ' . $keyh . '</a></p>';
						$nodisplay = false;
						$prev = ".mp4"; 
					 } elseif ( $ext == ".html" ) { 
						$returnvalue .= $clear;
						ob_start ( );
						include $value;
						$returnvalue .= ob_get_clean ( );
						$returnvalue .= $clear;
						$nodisplay = false;
						$prev = ".html"; // skip no data message
					 } elseif ( $ext == ".jpg" ) { 
						if ( $nodisplay || $prev != ".jpg" ) $returnvalue .= $clear;
						$keyh = str_replace ( ".jpg", "", $key );
						if ( $full ) { $returnvalue .= '<a href="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' "><img src="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' " width="' . $this->setting [ 'jpgwidth' ] . '" height="' . $height . '" target="_124"/></a>';
					 } else { $returnvalue .= '<p><img src="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' " width="' . $this->setting [ 'jpgwidth' ] . '" height="' . $height . '" vspace="5"/></p>'; } 
					$nodisplay = false;
					$prev = ".jpg"; 
				 }	
			 } 
			if ( count ( $fileArray ) >0 ) $returnvalue .= $this->language [ 'layout' ] [ 'show9' ] ?: '';
			return $returnvalue;
	}	 } 
	
	public function gsm_selectOption ( 
		// ============================
		$inputArr, 		// array in optionally select one return is the result array
		$select = '-', 
		$func = 1 , 	// func = 1 key value, 2 = part of value 3 = value value empty = 1 add / 2 add selected 4 dropdownvalue  5 dropdown key
		$empty = '' 
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426";  // function 4 en 5 added
		$input = is_array ( $inputArr ) ? $inputArr : explode ( "|" ,$inputArr );
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array ( "func" => $func, "input" => $input, "select" => $select, "empty" => $empty ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( $select == '' ) $select = '-';
		$layOut [ 0 ] = '<option value="%s" >%s</option>';
		$layOut [ 1 ] = '<option value="%s" selected >%s</option>';
		$layOut [ 4 ] = '<div class="menu"><div class="item">';
		$layOut [ 5 ] = '</div><div class="item">';
		$layOut [ 6 ] = '</div></div>';
		$returnvalue = '';
		switch ( $func ) { 
			case 1:
			case 2:
			case 3:
				$tobeDone = true;
				foreach ( $input as $key => $value ) { 
					$pos = strpos ( $value, sprintf ( "%s", $select ) );
					if ( $tobeDone && $func == 1 && ( $select == $key || $select == $value ) ) { 
						$returnvalue .= sprintf ( $layOut [ 1 ] , $key, $value ); $tobeDone = false;
					 } elseif ( $tobeDone && $func == 2 && $pos !== false ) { 
						$returnvalue .= sprintf ( $layOut [ 1 ] , $key, $value ); $tobeDone = false; 
					 } elseif ( $tobeDone && $func == 3 && $pos !== false ) { 
						$returnvalue .= sprintf ( $layOut [ 1 ] , $value, $value ); $tobeDone = false; 
					 } else { $returnvalue .= sprintf ( $layOut [ 0 ] , ( $func == 3 ) ? $value : $key, $value ); } 
				} 
				if ( $tobeDone && strlen ( $empty ) > 1 ) 
					$returnvalue = sprintf ( $layOut [ 1 ], "-", $empty ) . $returnvalue;
				break;
			case 4:
				$returnvalue = $layOut [ 4 ]. implode ( $layOut [ 5 ] , $input ) . $layOut [ 6 ];
				break;
			case 5:
				$returnvalue = $layOut [ 4 ]. implode ( $layOut [ 5 ] , array_keys ( $input ) ) . $layOut [ 6 ];
				break;
		}
		return $returnvalue;
	}
		
	public function gsm_uploadFile ( 
		// ============================	
		$location = "-",   	// location 
		$format = "-", 		// filename / format
		$descr = "-", 		// filename
		$recid = "-", 		// volgnummer / recid
		$type = "-", 		// parameter in filenaam
		$user = false 		// prefix van user
		// ============================
		) { 
		$returnvalue = false;
		$this->version [ __FUNCTION__ ] = "2024042";  
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( $location, $format, $descr, $recid, $type, $user, $this->setting ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		
		/* controle velden */
		$location = ( $location == "-" ) ? $location = LEPTON_PATH . '/media' : LEPTON_PATH . str_replace ( LEPTON_PATH, "", $location );
		if ( $format == "-" ) $format = '%2$s%5$s';
		if ( $recid == "-" ) $recid = $this->recid ?? 0;
		$recid = ( $recid != '' && is_numeric ( $recid ) && $recid > 0 ) ? $recid : '';
		$prefix = ( $user && strlen ( $this->user [ 'ref' ] ) > 1 ) ? substr ( $this->user [ 'ref' ] ,0 ,2 ) : $this->setting [ 'owner' ] ;
		$this->gsm_existDir ( $location , true );
		$oke=false;
		if ( is_numeric( $recid ) && isset ( $_FILES [ 'doc_uploaded' ][ 'error' ] ) && $_FILES[ 'doc_uploaded' ][ 'error' ] == 0 ) {
			/* there is an attachment and we know how to store it */
			$help = explode( ".", $_FILES[ 'doc_uploaded' ][ 'name' ] );
			$extension = end ( $help );
			if ( in_array ( $extension, $this->setting [ 'allowed' ] ) && $_FILES[ 'doc_uploaded' ][ 'size' ] < $this->setting [ 'size' ] ) $oke = true; // file test
			if ( $oke ) {
				if ( $type == "-" ) $type ='';
				if ( $descr == "-" ) $descr = $help[0];
				if ( $descr == "-" && isset ( $_POST[ 'bijl_oms' ] ) ) $descr = strtolower( $_POST[ 'bijl_oms' ] );
				$guid6	= strtolower( substr( $this->gsm_guid(), 0, 6 ) );	
				$filename = sprintf ( $format, 
/* 1 */				$prefix, 
/* 2 */				$recid, 
/* 3 */				"_".date( "Y-m-d"). $guid6 ,
/* 4 */				$type ,
/* 5 */				$descr);
				$filename = str_replace( "-", "_",str_replace( " ", "_", $filename . "." . $extension ));
				$filename = $this->gsm_sanitizeStringS ( $filename , "s{FILE}" ); 
				move_uploaded_file( $_FILES[ 'doc_uploaded' ][ 'tmp_name' ], $location . $filename ) ;
				$returnvalue = date ( "H:i:s " ) . __LINE__  . " Uploaded : ". $filename.NL;
				unset ( $_FILES[ 'doc_uploaded' ][ 'error' ]);
		}  } 
		return $returnvalue;
	}
	
}

class gsmoffb extends GeneralRoutines {
	
	public function gsm_A_accessExport (
		// ============================
		$func = 1, 
		$fileref = '', 
		$location ='' 
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20230316";  ;
		$temp = array();
		$query = "SHOW TABLE STATUS LIKE '" . $fileref . "%'";
		$results = array();
		$database->execute_query( $query, true, $results);
		$i = 0;
		foreach ($results as $row) {
			$rown = array();
			$query = "CHECK TABLE `" . $row['Name']."`";
			$database->execute_query( $query, true, $rown);
			foreach ($rown as $row2) { 
				$row [ 'Table' ] = $row2 [ 'Table' ];
				$row [ 'Msg_type' ] = $row2 [ 'Msg_type' ];
				$row [ 'Msg_text' ] = $row2 [ 'Msg_text' ];
				if ($row2['Msg_text'] != "OK"){
					$database->execute_query( "REPAIR TABLE `" . $row['Name']."`", true, $rown);
					$row [ 'Msg_text' ] = "repair initiated";
				}
				if (!strstr($row['Name'], "_bck")) { 
					ini_set('max_execution_time', 600);
					if ($row['Engine']=="MyISAM") {
						$query = "OPTIMIZE TABLE `" . $row['Name']."`";
						$rown = array();
						$database->execute_query( $query, true, $rown);
					}
					/* 
					 * start sql output
					 */	
					$run = date("Ymd_His");
					if ($func == 1 ) $filename_sql = $location.str_replace(".", "_", $row['Name'])."_00000000_000000t.sql";
					if ($func != 1 ) $filename_sql = $location.str_replace(".", "_", $row['Name'])."_".$run."t.sql";
					$output = "# "."\n".
					"# Office tools  Database Backup"."\n".
					"# Run ".$run."\n".
					"# "."\n";
					/* 
					 * drop
					 */
					$output .= "\n\r"."# Drop table ".$row['Name']. " if exists"."\n\r"."DROP TABLE IF EXISTS `".$row['Name']."`;";
					/* 
					 * Haal structuur op
					 */
					$query = 'SHOW CREATE TABLE '.$row['Name'].'';
					$rown = array();
					$database->execute_query( $query, true, $rown);
					$out= current ($rown);
					$sql_backup ="\r\n# Create table ".$row['Name']."\r\n\r\n";
					$sql_backup .= $out['Create Table'].";\r\n\r\n";
					$sql_backup .= "# Dump data for ".$row['Name']."\r\n\r\n";
					/* 
					 *	Select everything
					 */
					$query = 'SELECT * FROM '.$row['Name'];
					$out = array(); 
					$database->execute_query( $query, true, $out);
					$sql_code = '';
					/* 
					 *	Loop through all collumns
					 */
					foreach ($out as $code) {
						$sql_code .= "INSERT INTO ".$row['Name']." SET ";
						foreach($code as $insert => $value) { $sql_code .= "`".$insert ."`='".addslashes( $value ?? "" )."',"; }
						$sql_code = substr($sql_code, 0, -1);
						$sql_code.= ";\r\n";
					}
					$output .= $sql_backup.$sql_code;
					/* 
					 * Sla data op
					 */
					ob_start();
					echo $output;
					$htmlStr = ob_get_contents();
					ob_end_clean(); 
					file_put_contents($filename_sql, $htmlStr);
					$row [ 'SQLout' ] = $run. ' : '. str_replace ( LEPTON_PATH , "", $filename_sql);
					$row [ 'SQLfile' ] = '<a href="'.str_replace ( LEPTON_PATH , "", $filename_sql).'" target="_href" > SQLfile </a>';	
					/* 
					 * start csv output
					 */	
					/* 
					 * Haal structuur op
					 */
					$query ="DESCRIBE `" . $row['Name']."`";
					$rown = array();
					if ( $database->execute_query( $query, true, $rown) || count($rown) > 0) {
						$fieldArray = array ();
						foreach( $rown as $row2 ) $fieldArray[$row2['Field']]= $row2['Type'];
						if (isset ($fieldArray['id'])) {  // als niet skip.  is ID is verplicht veld om op deze wijze te saven
							unset ($fieldArray['id']);
							unset ($fieldArray['zoek']);
							unset ($fieldArray['updated']);
							$csv_data = array ();
							$csv_out = "id";
							foreach ($fieldArray as $key => $value ) { $csv_out .= ";".$key; }
							$csv_data[] = explode(';',trim($csv_out));			  
							/* 
							* Haal data op
							*/
							$query = "SELECT * FROM `" . $row['Name']."` ORDER BY `id`";					
							$rown = array();
							if ( $database->execute_query( $query, true, $rown) || count($rown) > 0) {
								foreach( $rown as $row2 ) { 
									$csv_out = $row2 [ 'id' ];
									foreach ($fieldArray as $key  => $value ) { $csv_out .= ";".$row2[$key]; }
									$csv_data[] = explode(';',trim($csv_out));
							}	}				
							/* 
							 * Sla data op
							 */
							$run = date("Ymd_His");
							if ($func == 1 ) $filename_csv = $location.str_replace(".", "_", $row['Name'])."_00000000_000000t.csv";
							if ($func != 1 ) $filename_csv = $location.str_replace(".", "_", $row['Name'])."_".$run."t.csv";
							$handle = fopen($filename_csv, 'w');
							foreach ( $csv_data as $key=> $value ) fputcsv($handle, $value, ';', '"');
							fclose($handle);
							$row [ 'CSVout' ] = $run. ' : '. str_replace ( LEPTON_PATH , "", $filename_csv);
							$row [ 'CSVfile' ] = '<a href="'.str_replace ( LEPTON_PATH , "", $filename_csv).'" > CSVfile </a>';
						} 
					}	
				}
				$temp [ ] = $row;
			}	
		}
		return $temp;
	}
			
	public function gsm_preloadDataB ( 
		// ============================
		$filter, 
		$input=''
		// ============================
		) {  
		global $database;
		$this->version [ __FUNCTION__ ] = "20230820";  //= "20230828";
/* debug * / Gsm_debug ( array( $filter, $input ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		// check if a valid filter was supplied
		if ( !preg_match ( '#(b)#i', $filter, $match ) ) { 
			die ( sprintf ( 'Oeps unexpected case. Filter: <b>%s</b> is invalid ( %s-%s )</br>', 
				htmlentities($filter), 
				__LINE__, 
				$this->version ) );
		}
		if ( $input == '' ) { $temp=array ( ); } else { $temp = is_array ( $input ) ? $input : array ( $input ); } 
		$filter_type = strtolower ( $match [1] );
		switch ( $filter_type ) { 
		case 'b': 
			// check for correct string filter: s { key } 
			if ( !preg_match ( '#(b)\{(.+)\}#i', $filter, $match ) ) { 
				die ( sprintf ( 'Oeps unexpected case. String filter: <b>%s</b> is invalid ( %s-%s )</br>', 
					htmlentities($filter), 
					__LINE__, 
					$this->version ) );
			}
			// create array with list values from regular expression
			$filter_options = strtoupper ( $match [ 2 ] );
			// b { REKALN } 
			// $groupArr = $this->gsm_preloadDataB ( 'b { REKALN } ' );
			/* rekaln */
			if ( strpos ( $filter_options, 'REKALN' ) !== false ) { 
				$fields = array( "id" , "ref" );
				$results = array ( );
				$database->execute_query (
					sprintf ( "SELECT `%s` FROM `%s_schema` ORDER BY `ref`", implode ( "`, `", $fields ), LOAD_DBBASE),
					true, 
					$results );
				if ( count ( $results) > 0 )
					foreach ( $results as $row ) $temp [ $row [ 'id' ] ] = $row [ 'ref' ];
			} 
			/* rektype */
			if ( strpos ( $filter_options, 'REKTYPE' ) !== false ) { 
				$fields = array( "id" , "type" );
				$results = array ( );
				$database->execute_query (
					sprintf ( "SELECT `%s` FROM `%s_schema` ORDER BY `ref`", implode ( "`, `", $fields ), LOAD_DBBASE),
					true, 
					$results );
				if ( count ( $results) > 0 )
					foreach ( $results as $row ) $temp [ $row[ 'id' ] ] = $row [ 'type' ];
			 } 
			 /* rekk5 */
			if ( strpos ( $filter_options, 'REKK5' ) !== false ) { 
				$fields = array( "id" , "ref", "name" );
				$results = array ( );
				$database->execute_query (
					sprintf ( "SELECT `%s` FROM `%s_schema` ORDER BY `ref`", implode ( "`, `", $fields ), LOAD_DBBASE),
					true, 
					$results );
				if (  count ( $results ) > 0 ) { 
					foreach ( $results as $row ) { 
						if ( $this->gsm_sanitizeStrings ( $row[ 'ref' ], 's{KLASSE}' ) == "5" ) $temp[$row['id']] = $this->gsm_sanitizeStrings ( $row[ 'ref' ], 's{SCHEMA}' ) . " - " . $row[ 'name' ];
			}	}	} 
			 /* reka5 */
			if ( strpos ( $filter_options, 'REKA5' ) !== false ) { 
				$fields = array( "id" , "ref", "amtbalans" );
				$results = array ( );
				$database->execute_query (
					sprintf ( "SELECT `%s` FROM `%s_schema` ORDER BY `ref`", implode ( "`, `", $fields ), LOAD_DBBASE),
					true, 
					$results ) ;
				if ( count ( $results ) > 0 ) { 
					foreach ( $results as $row ) { 
						if ( $this->gsm_sanitizeStrings ( $row[ 'ref' ], 's{KLASSE}' ) == "5" ) $temp [ $row [ 'id' ] ] = $row [ 'amtbalans' ];
			}	}	} 
			/* rekact * /
			if ( strpos ( $filter_options, 'REKACT' ) !== false ) { 
				$query = "SELECT * FROM `" . LOAD_DBBASE . "_schema ` WHERE `active` = 1 ORDER BY `ref`";
				$results = array ( );
				if ( $database->execute_query ( $query, true, $results, true, false ) && count ( $results ) > 0 ) { 
					foreach ( $results as $row ) { 
						$temp[ $row[ 'id' ] ] = $this->gsm_sanitizeStrings ( $row[ 'ref' ], 's{SCHEMA}' ) . " - " . $row[ 'name' ];
			 }	 }	 } 
			 /* rekall */
			if ( strpos ( $filter_options, 'REKALL' ) !== false ) {
				$fields = array( "id" , "ref", "name" );
				$results = array ( );
				$database->execute_query (
					sprintf ( "SELECT `%s` FROM `%s_schema` ORDER BY `ref`", implode ( "`, `", $fields ), LOAD_DBBASE),
					true, 
					$results );
				if ( count($results) > 0 )
					foreach ( $results as $row ) $temp[ $row[ 'id' ] ] = $this->gsm_sanitizeStrings ( $row[ 'ref' ], 's{SCHEMA}' ) . " - " . $row[ 'name' ];
			 } 
			 /* proact */
			if ( strpos ( $filter_options, 'PROACT' ) !== false ) { 
				$this->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $this->user [ 'privileged' ], array() , "label" );
/* 		
				$results = array ( );
				$database->execute_query ( 
					sprintf ( "SELECT * FROM `%s_project` WHERE `active` = 1 ORDER BY `name` ", LOAD_DBBASE),
					true, 
					$results );
				if ( count($results) > 0 )
					foreach ( $results as $row ) $temp [ $row [ 'name' ] ] = $row [ 'name' ] . " - " . $row[ 'omschrijving' ];
 */
				$temp= array();
			 }
			 /* tempb * /
			if ( strpos ( $filter_options, 'TEMPB' ) !== false ) { 
				$query = "SELECT * FROM `" . LOAD_DBBASE . "_booking ` WHERE `booking_date` > '" . $this->datehigh . "' ORDER BY `booking_date`";
				$results = array ( );
				if ( $database->execute_query ( $query, true, $results, true, false ) && count ( $results ) > 0 ) { 
					foreach ( $results as $row ) { 
						if ( substr ( $row[ 'name' ],0,1 ) =="_" ) $temp[ $row[ 'id' ] ] = ucfirst ( substr ( $row [ 'name' ], 1 ) );
			 }	 }	 } 
			 /* rekyears */
			if ( strpos ( $filter_options, 'REKYEARS' ) !== false ) { 
				$results = array ( );
				$database->execute_query ( 
					sprintf ("SELECT * FROM `%s_booking` ", LOAD_DBBASE),
					true, 
					$results );
				if ( count ( $results ) > 0 ) { 
					foreach ( $results as $row ) { 
						$help = substr ( $row['booking_date'],0,4 );
						if ( !isset ( $temp[$help] ) ) $temp[$help]=$row['booking_date'].'|'.$row['id'];
					 }	
					if ( !empty ( $temp ) ) krsort ( $temp );
			 }	 } 
			 /* rekhist */
			if ( strpos ( $filter_options, 'REKHIST' ) !== false ) { 
				$query = "SHOW TABLES LIKE '" . str_replace ( "_go" , "_go_%" , LOAD_DBBASE ) . "'";
				$results = array ( );
				$database->execute_query ( 
					sprintf ( "SHOW TABLES LIKE '%s'", str_replace ( "_go" , "_go_%" , LOAD_DBBASE ) ),
					true, 
					$results ) ;
				if ( count ( $results ) > 0 ) { 
					foreach ( $results as $result ) { 
						foreach ( $result as $key=>$value ) { 
							$hulp=explode ( "_", $value );
							if ( isset ( $hulp[4] ) && strlen ( $hulp[3] ) == 4 && is_numeric ( $hulp[3] ) ) $temp[$hulp[3]] = $value;
					}	 } 
					if ( !empty ( $temp ) ) krsort ( $temp );
			 }	 } 
			 /* oldest */
			if ( strpos ( $filter_options, 'OLDEST' ) !== false ) { 
				$fields = array ( "booking_date" );
				$results = array ( );
				$database->execute_query ( 
					sprintf ( "SELECT `%s` FROM `%s_booking` ORDER BY `booking_date` ASC LIMIT 1" , implode ( "`, `", $fields ), LOAD_DBBASE),
					true, 
					$results );
				if ( count ( $results ) > 0 ) { 
					$result = current ( $results );
					$temp = strtotime ( $result [ 'booking_date' ] );
				} else { $temp = strtotime ( date ( "Y", time ( ) ) -1 . "-01-01"); }
			 } 
			break;
		 } 
/* debug * / Gsm_debug ( array( $filter_type, $filter_options, $temp ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		return $temp;
	 } 
	
	public function gsm_rekeningAfschriftDisplB (
		// ============================
		$func = 1
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20230828";
/* debug * / Gsm_debug ( array ( $func, $this->page_content, $this->memory, $this->setting ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$this->page_content [ 'REKENING_ID' ] = $this->memory [ 1 ];
		$this->page_content [ 'BOOK_DATE' ] = $this->memory [ 2 ];
		$this->page_content [ 'BOOK_AMOUNT' ] = $this->memory [ 3 ];
		$this->page_content [ 'START_DATE' ] = $this->memory [ 4 ];
		$this->page_content [ 'VORIG_AMOUNT' ] = $this->memory [ 5 ];
		$this->setting ['rekeningTypeArray'] = $this->gsm_preloadDataB ( "b{REKTYPE}" ); // preload data

		/* begin saldo */
		$this->setting ['rekeningSaldoArray']  = $this->gsm_preloadDataB ( "b{REKA5}" ); // preload data
		if ( count ( $this->setting [ 'rekeningSaldoArray']  ) > 0 
			&& isset ( $this->setting [ 'rekeningSaldoArray' ]  [ $this->page_content [ 'REKENING_ID' ] ] ) ) { 
				$saldo = $this->setting [ 'rekeningSaldoArray' ]  [ $this->page_content [ 'REKENING_ID' ] ]; 
		} else { 
			$saldo = 0; 
		} 

		// haal de records op 
		$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` 
			WHERE `debet_id`= '" . $this->page_content [ 'REKENING_ID' ] . "' 
			OR `tegen1_id`= '" . $this->page_content [ 'REKENING_ID' ] . "' 
			OR `tegen2_id`= '" . $this->page_content [ 'REKENING_ID' ] . "' 
			ORDER BY `booking_date`, `ref`";
			
		$switchValue = 1;
		$mutatie = 0;
		$this->setting ['switchDate'] = $this->page_content[ 'START_DATE' ];
		$temp = array ( );
		$results = array ( );
		$history = array ( 0, 0, 0 );
		$database->execute_query ( $query, true, $results, true );
		if ( count ( $results ) > 0 ) { 
			foreach ( $results as $row ) { 
				if ( $row[ 'debet_id' ] == $this->page_content[ 'REKENING_ID' ] ) { 
					$amount = $row[ 'amt_debet' ];
					$mutatie += $amount; } 	
				if ( $row[ 'tegen1_id' ] == $this->page_content[ 'REKENING_ID' ] ) { 
					$amount = $row['amt_tegen1'] 
						* $this->language['type_sign'][$this->setting ['rekeningTypeArray'][$row['debet_id']]]
						* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$row['tegen1_id']]]
						* -1;	
					$mutatie += $amount; } 
				if ( $row [ 'tegen2_id' ] == $this->page_content[ 'REKENING_ID' ] ) { 
					$amount = $row['amt_tegen2'] 
						* $this->language [ 'type_sign' ] [ $this->setting ['rekeningTypeArray'][$row['debet_id']]]
						* $this->language [ 'type_sign' ] [ $this->setting ['rekeningTypeArray'][$row['tegen2_id']]]
						* -1;
					$mutatie += $amount; 
				} 	

				$addOK = true;
				$row ['mutatie'] = $this->gsm_sanitizeStrings ( $mutatie, 's{KOMMA}' );
				$saldo += $mutatie; $mutatie=0;

				$row ['saldo'] = $this->gsm_sanitizeStrings ( $saldo, 's{KOMMA}' );
				$row ['remain'] = $this->gsm_sanitizeStringS ( ( $saldo - $this->page_content[ 'BOOK_AMOUNT' ] ) , 's{KOMMA}' );
				unset ( $row ['zoek'] );
				unset ( $row ['content_short'] );
				unset ( $row ['updated'] );

				/* no show voor start datum */
				if ( $row['booking_date'] < $this->page_content[ 'START_DATE' ] ) { 
					$addOK = false;
					$switchValue = 1;
					$row ['switch'] = $switchValue;	
				} 

				/* no show na to-day */
				if ( $row['booking_date'] > $this->page_content[ 'BOOK_DATE' ] ) { 
					$addOK = false;
					$switchValue = 2;
					$row ['switch'] = $switchValue;	
				} 

				/* at least tickeable */	
				if ( $addOK && $switchValue < 3 ) {
					$switchValue = 3;
					$row ['switch'] = $switchValue;
				}

				if ( $addOK ) $row ['switch'] = $switchValue;

				/* treshold reached */
				
				if ( $addOK && abs ( $saldo - $this->page_content [ 'VORIG_AMOUNT' ] ) < 0.002 ) { 
					$switchValue = 5;
					if ( $this->page_content [ 'SWITCH_DATE' ] == $this->page_content [ 'START_DATE' ] ) 
						$this->page_content [ 'SWITCH_DATE' ] = $row [ 'booking_date' ];
				} 
				
				if ( $addOK ) array_unshift ( $history , $saldo - $this->page_content [ 'VORIG_AMOUNT' ] );

				/* if amount 2  amount not modifyable */
				if ( $addOK && $switchValue == 5 && abs ( $row['amt_tegen2'] ) > 0.002 ) $row ['switch'] = 4;
				
				if ( $addOK ) $temp [] = $row;
			}
			if ( $this->page_content [ 'SWITCH_DATE' ] == $this->page_content [ 'START_DATE' ] 
				&& abs ( $this->memory [ 5 ] ) < 0.002 ) $this->memory [ 5 ] = $history [ 1 ];			
		}	
		return $temp;
	 } 

	public function gsm_rekeningAfschriftSelB (
		// ============================
		$func = 1, 
		$input = '' 
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20230828";  
		/* *
		 *  evaluatie selectie parameters en opmaak amounts
		 */
/* debug * / Gsm_debug ( array( $func, $input, $this->memory, $_POST ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( $input == '' ) { $temp = array ( ); } else { $temp = is_array ( $input ) ? $input : array ( $input ); } 
		$setting = array_shift ( $temp );
		$rekeningArray = $this->gsm_preloadDataB ( "b{REKK5}" ); 
		foreach ( $rekeningArray as $key=>$value ) { 
			$this->memory [ 1 ]= ( strpos ( $value, sprintf ( "%s", $setting ) ) !== false ) 
				? $key 
				: $this->memory [ 1 ];
		} 
		if ( isset ( $_POST[ 'gsmc_rekening_id' ] ) && $_POST[ 'gsmc_rekening_id' ] >0 ) $this->memory [ 1 ] = $_POST[ 'gsmc_rekening_id' ];
		if ( isset ( $_POST[ 'gsmc_book_date' ] ) ) $this->memory [ 2 ] = $this->gsm_sanitizeStringD ( $_POST[ 'gsmc_book_date' ],
			'y{'.date ( "Y-m-d", time ( ) ).';' . $this->page_content [ 'DATELOW' ] . ';'. date ( "Y-m-d", strtotime ( date ( "Y-m-d", time ( ) )."first day of +1 month" ) ).'}' ); //20210516
		if ( isset ( $_POST[ 'gsmc_start_date' ] ) ) $this->memory [ 4 ] = $this->gsm_sanitizeStringD ( $_POST[ 'gsmc_start_date' ],
			'y{'.$this->memory [ 2 ].';'.$this->page_content [ 'DATELOW' ].';'.$this->memory [ 2 ].'}' );
		if ( isset ( $_POST[ 'gsmc_book_amount' ] ) ) $this->memory [ 3 ] = $this->gsm_sanitizeStringv ( $_POST[ 'gsmc_book_amount' ], 
			'v{0;-1000000;1000000}' );
		if ( isset ( $_POST[ 'gsmc_vorig_amount' ] ) ) $this->memory [ 5 ] = $this->gsm_sanitizeStringv ( $_POST[ 'gsmc_vorig_amount' ], 
			'v{0;-1000000;1000000}' );
		// default waardes:
		$this->memory [ 2 ] = $this->gsm_sanitizeStringD ( $this->memory [ 2 ], 'y{'.date ( "Y-m-d", time ( ) ).';'.$this->page_content [ 'DATELOW' ].';'. date ( "Y-m-d", strtotime ( date ( "Y-m-d", time ( ) )."first day of +1 month" ) ).'} ' );
		$this->memory [ 4 ] = $this->gsm_sanitizeStringD ( $this->memory [ 4 ], 'y{'.date ( "Y-m-d", strtotime ( $this->memory [ 2 ]."first day of -1 month" ) ) .';'.$this->page_content [ 'DATELOW' ].';'.$this->memory [ 2 ].'}' );
		// opmaak
		$this->page_content [ 'REKENING_ID' ] = $this->memory [ 1 ];
		$this->page_content [ 'BOOK_DATE' ] = $this->memory [ 2 ];
		$this->page_content [ 'BOOK_AMOUNT' ] = $this->memory [ 3 ];
		$this->page_content [ 'START_DATE' ] = $this->memory [ 4 ];
		$this->page_content [ 'VORIG_AMOUNT' ] = $this->memory [ 5 ];
		$this->page_content [ 'SWITCH_DATE' ] = $this->page_content [ 'START_DATE' ];
		$this->page_content [ 'REKENING_IDS' ] = ( $this->memory [ 1 ] <1 ) 
			? $this->gsm_selectOption ( $rekeningArray, $setting , 2 ) 
			: $this->gsm_selectOption ( $rekeningArray, $this->memory [ 1 ] );

/* debug * / Gsm_debug ( array ( $this->page_content, $this->memory ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		return $temp ;
	 } 

	public function gsm_rekeningArchiveDocB (
		// ============================
		$func, 
		$year, 
		$project 
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20230316";  
		$dir_to = LEPTON_PATH . $this->setting [ 'mediadir' ] . "/" .$this->setting [ 'owner' ]. $year;
		$pdf = new PDF();
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$this->pdf_text = '';
		$this->pdf_data = array( );
		global $owner;
		$owner = $this->setting [ 'owner' ];
		global $run;
		$run = date ( DATE_FORMAT, time ( ) ) . " " . date ( TIME_FORMAT, time ( ) );
		global $title;
		$title = $project." ".$year ;
		switch ( $func ) { 
			case 1: // Begin Balans
				$formulier = sprintf( "Begin Balans   %s  Datum :  %s ", $year, $year."-01-01");
				$this->setting ['pdf_filename'] = sprintf('%s_%s_%s_%s.pdf', $this->setting ['owner'], $year."-01-01", strtolower(str_replace('.', ' ', $this->setting [ 'droplet' ] [ LANGUAGE . '2' ] ) ), "final begin balans");
				$this->gsm_RekeningDisplayB (1, '' ,false ,true , $year."-01-01", $year."-01-01", false );
				$this->cols = array(15, 90, 1, 20, 20, 30);
				$this->pdf_header = array( ucfirst( "rek"), ucfirst( "omschrijving"), ucfirst( ''), ucfirst( ' '), ucfirst( ' '), ucfirst( "bedrag" ) );
				break;
			case 2: // Eind balans
				$formulier = sprintf( "Eind Balans   %s  Datum  : %s ", $_GET['date'], $year."-12-31");
				$this->setting ['pdf_filename'] = sprintf('%s_%s_%s_%s.pdf', $this->setting ['owner'], $year."-12-31", strtolower(str_replace('.', ' ', $this->setting [ 'droplet' ] [ LANGUAGE . '2' ] )), "final eind balans");
				$this->gsm_RekeningDisplayB (1, '' ,false ,true , $year."-01-01", $year."-12-31", false );
				$this->cols=array(15, 90, 1, 20, 20, 30);
				$this->pdf_header = array( ucfirst( "rek"), ucfirst( "omschrijving"), ucfirst( ''), ucfirst( ' '), ucfirst( ' '), ucfirst( "bedrag" ) );
				break; 
			case 3: // Eind balans met details
				$formulier = sprintf( "Eind Balans  incl details  %s  Datum  : %s ", $_GET['date'], $year."-12-31");
				$this->setting ['pdf_filename'] = sprintf('%s_%s_%s_%s.pdf', $this->setting ['owner'], $year."-12-31", strtolower(str_replace('.', ' ', $this->setting [ 'droplet' ] [ LANGUAGE . '2' ] ) ), "final eind balans (details)");
				$this->gsm_RekeningDisplayB (1, '' ,true ,true , $year."-01-01", $year."-12-31", false );
				$this->cols=array(15, 90, 1, 20, 20, 30);
				$this->pdf_header = array( ucfirst( "rek"), ucfirst( "omschrijving"), ucfirst( ''), ucfirst( 'bedrag'), ucfirst( 'cum'), ucfirst( "totaal" ) );
				break; 
			case 4: // Resultaat
				$formulier = sprintf( "Resultaat  %s  Periode  : %s - %s", $_GET['date'], $year."-01-01", $year."-12-31");
				$this->setting ['pdf_filename'] = sprintf('%s_%s_%s_%s.pdf', $this->setting ['owner'], $year."-12-31", strtolower(str_replace('.', ' ', $this->setting [ 'droplet' ] [ LANGUAGE . '2' ] ) ), "final resultaat");
				$this->gsm_RekeningDisplayB (2, '' ,false ,true , $year."-01-01", $year."-12-31", false );
				$this->cols=array(15, 90, 1, 20, 20, 30);
				$this->pdf_header = array( ucfirst( "rek"), ucfirst( "omschrijving"), ucfirst( ''), ucfirst( ' '), ucfirst( ' '), ucfirst( "bedrag" ) );
				break;
			case 5: // Resultaat met details
			default:
				$formulier = sprintf( "Resultaat incl details  %s  Periode  : %s - %s", $_GET['date'], $year."-01-01", $year."-12-31");
				$this->setting ['pdf_filename'] = sprintf('%s_%s_%s_%s.pdf', $this->setting ['owner'], $year."-12-31", strtolower(str_replace('.', ' ', $this->setting [ 'droplet' ] [ LANGUAGE . '2' ] ) ), "final resultaat (details)");   
				$this->gsm_RekeningDisplayB (2, '' ,true ,true , $year."-01-01", $year."-12-31", false );
				$this->cols=array(15, 90, 1, 20, 20, 30);
				$this->pdf_header = array( ucfirst( "rek"), ucfirst( "omschrijving"), ucfirst( ''), ucfirst( 'bedrag'), ucfirst( 'cum'), ucfirst( "totaal" ) );
				break; 
		} 
		$pdf->ChapterTitle( $func, $formulier ); 
		$this->pdf_text .= "\n\n"  . $this->setting [ 'droplet' ] [ LANGUAGE . '2' ];
		$this->pdf_text .= "\n\n" . "Document";
		$this->pdf_text .= "\n" . $this->setting ['pdf_filename'] ;
		$this->pdf_text .= "\n" . "Created on: " . $run . "\n" ;
		$pdf->DataTable( $this->pdf_header, $this->pdf_data, $this->cols );
		$pdf->ChapterBody( $this->pdf_text );
		$pdf->Output( $dir_to . "/" .$this->setting ['pdf_filename'], 'F' );
		return $dir_to . "/" .$this->setting ['pdf_filename'];
	}  

public function gsm_rekeningDisplayB ( 
		// ============================	
		$func	=1,			// functie 1 = balans 2 = resultaat
		$search	= '', 		// zoek argument
		$det	=false, 	// details nodig
		$pdf	=false, 	// pdf nodig
		$van	='',   		// datum vanaf
		$tot	='', 		// datum tot en met 
		$budget = false		// met budet 0= geen budget , 1= budget a, 2= budget b
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20230916";
		$returnvalue = '';
		// check input
		$set [ 'van' ] = ( $van == '' ) ? $this->datelow : $van;
		$set [ 'tot' ] = ( $tot == '' ) ? $this->datehigh : $tot;
		$set [ 'det' ] = ( $det ) ? true : false;
		$set [ 'pdf' ] = ( $pdf ) ? true : false;
		$set [ 'budget' ] = ( $budget ) ? true : false;
		$set [ 'budgeta' ] = ( $budget > 1 ) ? true : false;
		$set [ 'search' ] = ( strlen ( trim ( $search ) ) >3 ) ? strtolower ( trim ( $search ) ) : '';
		$set [ 'search' ] = $this->gsm_sanitizeStringS ( $set [ 'search' ] , 's{WHOLE}');
		$set [ 'func' ] = ($func == "2") ? "2" : "1"; 
/* debug * / Gsm_debug ( array( $set ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		// start
		$query = "SELECT * FROM `" . $this->file_ref[ 98 ] . "`"; 
		if ( $set [ 'func' ] == "1" ) $query .= " WHERE `type` = '1B' OR `type` = '2B' ";
		if ( $set [ 'func' ] == "2" ) $query .= " WHERE `type` = '3R' OR `type` = '4R' ";
		$query .= " ORDER BY `type`, `ref`";
		$this->cal ['cum_rek'] = 0;
		$this->cal [ 'ref3' ] = ''; // vorige nummer
		$this->cal [ 'ref2' ] = ''; // vorige groep
		$this->cal [ 'ref1' ] = ''; // vorige klasse
		$this->cal [ 'ref0' ] = ''; // vorige type	
		$this->cal [ 'oke0' ] = false; //eerste ronde gedaan	
		$this->cal [ 'eind3' ] = false;
		$this->cal [ 'eind2' ] = false; 
		$this->cal [ 'eind1' ] = false; 	
		$this->cal [ 'eind0' ] = false; // er is type data
		$r_results = array();
		$database->execute_query ( $query, 
			true, 
			$r_results);	
		if ( count ( $r_results	) > 0){
			foreach ( $r_results as $r_row) {
				if ( $this->cal ['ref3'] !='' ) {
					if ($this->cal ['eind3'] || $this->cal ['cum_rek'] != 0 ) {
						// betreft het een echte subgroep zo nee skip it 
						if ( $this->gsm_sanitizeStringS ( $this->cal ['ref3'], 's{SCHEMA}') != $this->gsm_sanitizeStringS ( $this->cal ['ref3'], 's{WHOLE}' )) {
							// er is een afsluiting op dit niveau de onderliggende niveaus moeten dan ook
							$this->cal ['eind2'] = true; $this->cal ['eind1'] = true; $this->cal ['eind0'] = true;
							$returnvalue .= $this->gsm_RekeningDisplayRecB( 10 , $set);
						}
						$this->cal ['cum_rek'] = 0;	
					}
					if ( $this->cal ['ref2'] != $this->gsm_sanitizeStringS ( $r_row['ref'], 's{WHOLE}' ) && ( $this->cal ['eind2'] || $this->cal ['cum_group'] != 0)) {
						// afsluiten vorige  groep
						// er is een totaal op dit niveau de onderliggende niveaus moeten dan ook
						$this->cal ['eind1'] = true; $this->cal ['eind0'] = true;
						// toon totaal
						$returnvalue .= $this->gsm_RekeningDisplayRecB( 11, $set );
						$this->cal ['cum_group'] = 0;
					}
					if ( $this->cal ['ref1'] != $this->gsm_sanitizeStringS ( $r_row['ref'], 's{KLASSE}' ) && ( $this->cal ['eind1'] || $this->cal ['cum_klas'] != 0))  {
						// afsluiten vorige  klasse
						$this->cal ['eind0'] = true;
						$returnvalue .= $this->gsm_RekeningDisplayRecB( 12, $set);
						$this->cal ['cum_klas'] = 0;
					}
					if ( $this->cal ['ref0'] != $r_row['type'] ) {
						// afsluiten vorig type
						$returnvalue .= $this->gsm_RekeningDisplayRecB( 13, $set);
						$this->cal ['cum_totaal'] = $this->cal ['cum_type'];
						$this->cal ['cum_type'] = 0;
					}
				}
				if (!$this->cal [ 'oke0' ] ) {
					$this->cal [ 'cum_rek' ] = 0;
					$this->cal [ 'cum_group' ] = 0;
					$this->cal [ 'cum_klas' ] = 0;
					$this->cal [ 'cum_type' ] = 0;
					$this->cal [ 'cum_resultaat' ] = 0;
					$this->cal [ 'cum_totaal' ] = 0;
					$this->cal [ 'oke0' ] = true;
				}
				if ( $this->cal [ 'ref0' ] != $r_row [ 'type'] ) {
					// initialiseren velden
					$this->cal [ 'cum_type'] = 0;
					$this->cal [ 'ref0' ] = $r_row[ 'type' ];
					$this->cal [ 'ref1' ] = '';
					$this->cal [ 'eind0' ] = true;
					// output
					$returnvalue .= $this->gsm_RekeningDisplayRecB ( 1, $set );
				}
				if ( $this->cal ['ref1'] != $this->gsm_sanitizeStringS ( $r_row['ref'], 's{KLASSE}' ) ) {
					// initialiseren velden	
					$this->cal ['cum_klas'] = 0;
					$this->cal ['ref1'] = $this->gsm_sanitizeStringS ( $r_row['ref'], 's{KLASSE}' );	
					$this->cal ['ref2'] ='';
					$this->cal ['eind1'] = false;
				}
				if ( $this->cal ['ref2'] != $this->gsm_sanitizeStringS ( $r_row['ref'], 's{WHOLE}')){
					// initialiseren velden
					$this->cal ['ref2'] = $this->gsm_sanitizeStringS ( $r_row['ref'], 's{WHOLE}');
					$this->cal ['cum_group'] = 0;
					$this->cal ['ref3'] ='';
					$this->cal ['eind2'] = false;
				}
				// initialiseren velden
				$this->cal [ 'ref3' ] =  $this->gsm_sanitizeStringS ( $r_row['ref'], 's{SCHEMA}');
				$this->cal [ 'eind3' ] = false;
				// overnemen data van het record
				$this->cal ['id']= $r_row['id']; 
				$this->cal ['name3'] = $r_row['name']; 
				$this->cal ['balans']= $r_row['amtbalans']; 
				$this->cal ['balans_datum']= $r_row['date_balans'];
				$this->cal ['budget']= ($set['budgeta']) ? $r_row['amtbudget_a'] : $r_row['amtbudget_b']; 
				$this->cal ['budget']= ($set['budget']) ? $this->cal['budget'] : 0; 
				$this->cal ['budget3']= $this->cal['budget'];
				// subgroup ??
				$sub_group = ( $this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{SCHEMA}') == $this->gsm_sanitizeStringS ( $this->cal ['ref3'], 's{WHOLE}' )) ? false : true;
				if ( !$sub_group) {
					$this->cal ['name2'] = $r_row['name']; 
					$this->cal ['budget2']= $this->cal['budget']; 	
				}
				// verwerken beginbalans
				if ($r_row['amtbalans'] != 0 ) {
					$this->cal ['cum_rek'] += $r_row['amtbalans'];
					$this->cal ['cum_group'] += $r_row['amtbalans'];
					$this->cal ['cum_klas'] += $r_row['amtbalans'];
					$this->cal ['cum_type'] += $r_row['amtbalans'];
					if ( ($set['det'] )|| ( $set['func'] == "1" && $set[ 'search' ] == $this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{WHOLE}') ) ) {
					// output
						$this->cal ['datum'] = $r_row['date_balans'];
						$returnvalue .= $this->gsm_RekeningDisplayRecB( 20, $set );
						if ( $sub_group ) { $this->cal ['eind3'] = true;} else {$this->cal ['eind2'] = true;} 
					}
				}
				// verwerken budget
				if ($this->cal ['budget'] != 0 ) {
					if ( $sub_group ) { 
						$this->cal ['eind3'] = true;
						$this->cal ['budget3'] = $this->cal ['budget'];
					} else {
						$this->cal ['eind2'] = true;
						$this->cal ['budget2'] = $this->cal ['budget'];
					} 
				}
				// verwerken booking records         
				$msql_search = "`booking_date` >= '" . $set['van'] . "' AND `booking_date` <= '" . $set['tot'] . "' ";
				$msql_search .= " AND ( `debet_id`= '" . $r_row['id'] . "' OR `tegen1_id`= '" . $r_row['id'] . "' OR  `tegen2_id`= '" . $r_row['id'] . "' )";
				$query = "SELECT * FROM `" . $this->file_ref[ 99 ] . "` WHERE " . $msql_search . " ORDER BY `booking_date`, `ref`";
				$TEMPLATELINK= '<a href="'.LOAD_RETURN.'&command=Select&module=%1$s&recid=%2$s" >%3$s</a>';
				$results = array();
				$database->execute_query ( 
					$query, 
					true, 
					$results);
				if ( count($results) > 0){ // er zijn records
					foreach ($results as $row) {
						// algemeen
						$this->cal [ 'book_date'] = $row['booking_date'];
						$this->cal [ 'book_linkdate' ]	= sprintf ($TEMPLATELINK, (LOAD_MODE == "x" ) ? 'xbalans' : 'vbalans' , $row['id'], $row['booking_date']);
						$this->cal [ 'book_id' ] = $row['id'];
						$this->cal [ 'book_linkid' ]	= sprintf ($TEMPLATELINK,  (LOAD_MODE == "x" ) ? 'xbalans' : 'vbalans' , $row['id'], $row['id']);
						$this->cal [ 'book_name' ] = ( strlen ( $row [ 'name' ] ) > 2) ? $row [ 'name' ]  : '';
						$this->cal [ 'book_project' ] = ( strlen ( $row [ 'ref' ] ) > 2) ? strtoupper ( $row [ 'ref' ] ) . " - "  : '';
						if ( $sub_group ) { $this->cal ['eind3'] = true;} else {$this->cal ['eind2'] = true;} 
						if ($row['debet_id'] == $r_row['id']) {
							$this->cal ['dit_bedrag'] = $row['amt_debet'];
							if ($this->cal['dit_bedrag'] <> 0) {
								$this->cal ['cum_rek'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_group'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_klas'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_type'] += $this->cal ['dit_bedrag'];
								if ( $set [ 'det' ] || $set [ 'search' ] == $this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{WHOLE}') ) 
									$returnvalue .= $this->gsm_RekeningDisplayRecB( 21, $set );
							}
						}
						if ($row['tegen1_id'] == $r_row['id']) {
							$this->cal ['dit_bedrag'] = $row['amt_tegen1'] 
								* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$row['debet_id']]]
								* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$row['tegen1_id']]]
								* -1;
							if ($this->cal ['dit_bedrag'] <> 0) {
								$this->cal ['cum_rek'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_group'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_klas'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_type'] += $this->cal ['dit_bedrag'];
								if ( $set['det'] || $set['search'] == $this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{WHOLE}') ) 
									$returnvalue .= $this->gsm_RekeningDisplayRecB( 21, $set );
							}
						}
						if ($row['tegen2_id'] == $r_row['id']) {
							$this->cal ['dit_bedrag'] = $row['amt_tegen2'] 
								* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$row['debet_id']]]
								* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$row['tegen2_id']]]
								* -1;
							if ($this->cal['dit_bedrag'] <> 0) {
								$this->cal ['cum_rek'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_group'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_klas'] += $this->cal ['dit_bedrag'];
								$this->cal ['cum_type'] += $this->cal ['dit_bedrag'];
								if ( $set['det'] || $set['search'] == $this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{WHOLE}') ) 
									$returnvalue .= $this->gsm_RekeningDisplayRecB( 21, $set );
							}
						}
					}
				}
			}
			// laatse records zijn verwerkt
			if ($this->cal ['eind3'] || $this->cal ['cum_rek'] != 0 ) {
				if ( $this->gsm_sanitizeStringS ( $this->cal ['ref3'], 's{SCHEMA}') != $this->gsm_sanitizeStringS ( $this->cal ['ref3'], 's{WHOLE}' )) {
					// afsluiten laatste  groep
					$this->cal ['eind2'] = true; $this->cal ['eind1'] = true; $this->cal ['eind0'] = true;
					$returnvalue .= $this->gsm_RekeningDisplayRecB( 10, $set);
				}
			}
			if ( $this->cal ['eind2'] || $this->cal ['cum_group'] != 0 ){
				// afsluiten laatste  groep
				$this->cal ['eind1'] = true; $this->cal ['eind0'] = true;
				$returnvalue .= $this->gsm_RekeningDisplayRecB( 11, $set);
			}
			if ( $this->cal ['eind1'] ) {
				// afsluiten laatste  klasse
				$this->cal ['eind0'] = true;
				$returnvalue .= $this->gsm_RekeningDisplayRecB( 12, $set);
			}
			if ( $this->cal ['eind0'] ) {
				// afsluiten laatste type
				if ($set['func'] == 1) {
					$this->cal ['cum_resultaat'] = $this->cal ['cum_totaal'] - $this->cal ['cum_type'];		
					$returnvalue .= $this->gsm_RekeningDisplayRecB( 15, $set);
					$returnvalue .= $this->gsm_RekeningDisplayRecB( 14, $set);
				} else {
					$this->cal ['cum_resultaat'] =  $this->cal ['cum_type']- $this->cal ['cum_totaal'];		
					$this->cal ['cum_totaal'] = $this->cal ['cum_type'];	
					$returnvalue .= $this->gsm_RekeningDisplayRecB( 14, $set);
					$returnvalue .= $this->gsm_RekeningDisplayRecB( 15, $set);
				}
			}
		} else {
			$returnvalue .= $this->language ['TXT_ERROR_DATA'];
			$this->pdf_text .= $this->language ['TXT_ERROR_DATA'];
		}
/* debug * / Gsm_debug ( array( $returnvalue ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		return $returnvalue;
	}

	public function gsm_rekeningDisplayBookB ( 
		// ============================	
		$func=1, 
		$input = 0
		// ============================	
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20230828"; 
/* debug * / Gsm_debug ( array( $func , $input ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] ); /* debug */
		$projectArray = array ( " "=>" " );
		$projectArray = $this->gsm_preloadDataB ( "b{PROACT} " , $projectArray );
		$rekeningArray = array ( " "=>" -- " );
		$rekeningArray = $this->gsm_preloadDataB ( "b{REKALL} " , $rekeningArray );
/* debug * / Gsm_debug ( array( $projectArray , $rekeningArray ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] ); /* debug */
		$temp= array ( );
		
		$result = array ( );
		$database->execute_query ( 
			sprintf ("SELECT * FROM `%s`WHERE `id`='%s'", $this->file_ref [ 99 ], $input ),
			true, 
			$result); 
		// een record opgehaald 
		if ( count ( $result ) > 0 ) { 
			$row = current ( $result );
			$n=4;
			$row [ 'switchb' ] = ( $row [ 'id_root' ] < 1 || $row [ 'id' ] == $row [ 'id_root' ] ) ? 1 : 3;
			if ( $row [ 'switchb' ] == 1 ) {   // geen groep
				$row [ 'REF' ] = $this->Gsm_selectOption ( $this->setting [ 'label' ] , '', 5);
				$LocalHulpA = $this->gsm_scanDir ( LEPTON_PATH . $this->setting [ 'mediadir' ]. "/current/", 
					2, 
					0, 
					"all", 
					"all", 
					$this->setting ['owner'].
					$row [ 'id' ] );
				foreach ( $LocalHulpA as $pay => $load ) $LocalHulpB [ $pay ] = LEPTON_URL . $this->setting [ 'mediadir']. "/current/" . $pay;
				if ( isset ( $LocalHulpB ) ) $row [ 'BIJLAGE' ] = $LocalHulpB;
			} 
			if ( $row [ 'switchb' ] == 3 ) {  // wel een groep
			} 
			$row [ 'DEBET' ] = $this->gsm_selectOption ( $rekeningArray , $row[ 'debet_id' ] );
			$row [ 'TEGEN1' ] = $this->gsm_selectOption ( $rekeningArray , $row[ 'tegen1_id' ] );
			$row [ 'TEGEN2' ] = $this->gsm_selectOption ( $rekeningArray , $row[ 'tegen2_id' ] );
			$row [ 'amt_debet' ]	= $this->gsm_sanitizeStringS ( $row [ 'amt_debet' ], 's{KOMMA}' );
			$row [ 'amt_tegen1' ] 	= $this->gsm_sanitizeStringS ( $row [ 'amt_tegen1' ], 's{KOMMA}' );
			$row [ 'amt_tegen2' ] 	= $this->gsm_sanitizeStringS ( $row [ 'amt_tegen2' ], 's{KOMMA}' );
			$temp [ $row [ 'switchb' ] ] = $row;
			if ( $row [ 'id_root' ] > 0 ) { 
				$results = array ( );
				$query = "SELECT * FROM `" .$this->file_ref [ 99 ] . "`WHERE `id_root`='" . $row [ 'id_root' ] . "'";
				$database->execute_query ( 
					sprintf ( "SELECT * FROM `%s`WHERE `id_root`='%s'",	$this->file_ref [ 99 ], $row [ 'id_root' ]),
					true, 
					$results );
				if ( count ( $results ) > 0 ) { 
					foreach ( $results as $rowc ) { 
						if ( $row ['id'] != $rowc ['id'] ) { 
							$rowc [ 'switchb' ] = ( $rowc [ 'id_root' ] < 1 || $rowc [ 'id' ] == $rowc [ 'id_root' ] ) ? 2 : $n;
							if ( $rowc [ 'switchb' ] == 2 ) { 
//								$rowc [ 'REF' ] = $this->gsm_selectOption ( $projectArray, $rowc [ 'ref' ] );
//								$rowc [ 'BIJLAGE' ] = $this->gsm_scanDir ( LEPTON_PATH.$this->setting [ 'mediadir' ] , 2, 0, "all", "all", $this->setting ['owner'].$rowc [ 'id' ] );
								$LocalHulpA = $this->gsm_scanDir ( LEPTON_PATH. $this->setting [ 'mediadir' ]. "/current/",  2, 0, "all", "all", $this->setting ['owner'].$rowc [ 'id' ] );
								foreach ( $LocalHulpA as $pay => $load ) $LocalHulpB [ $pay ] = LEPTON_URL . $this->setting [ 'mediadir']. "/current/" . $pay;
								if ( isset ( $LocalHulpB ) ) $rowc [ 'BIJLAGE' ] = $LocalHulpB;
							} 
							$rowc [ 'DEBET' ]		= $this->gsm_selectOption ( $rekeningArray , $rowc [ 'debet_id' ] );
							$rowc [ 'TEGEN1' ] 	= $this->gsm_selectOption ( $rekeningArray , $rowc [ 'tegen1_id' ] );
							$rowc [ 'TEGEN2' ] 	= $this->gsm_selectOption ( $rekeningArray , $rowc [ 'tegen2_id' ] );
							$rowc [ 'amt_debet' ]	= $this->gsm_sanitizeStringS ( $rowc [ 'amt_debet' ], 's{KOMMA}' );
							$rowc [ 'amt_tegen1' ] = $this->gsm_sanitizeStringS ( $rowc [ 'amt_tegen1' ], 's{KOMMA}' );
							$rowc [ 'amt_tegen2' ] = $this->gsm_sanitizeStringS ( $rowc [ 'amt_tegen2' ], 's{KOMMA}' );
							if ( $rowc [ 'switchb' ] == $n ) $n++;
							if ( $rowc [ 'id' ] != $row [ 'id' ] ) $temp [ $rowc [ 'switchb' ] ] = $rowc;
						 } 
					 } 
				 } 
			 } else { 
				$this->description .=$this->language [ 'TXT_ERROR_DATABASE' ]. NL;
			 } 
		 } else { 
			$this->description .=$this->language [ 'TXT_ERROR_DATA' ]. NL;
		 } 
		if ( count ( $temp ) > 1 ) ksort ( $temp );
		return $temp;
	 } 

	public function gsm_rekeningDisplayRecB ( 
		// ============================	
		$func=0, 
		$set= array() 
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20230916";  
		$test= false;
		$layOut = array ( 
			1 => "%s;%s;%s;%s;%s;%s",
			2 => '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>', 
			3 => ";;;;;",
			4 => '<tr><td></td><td></td><td>--------</td><td></td><td></td><td></td></tr>', 
			5 => ";-------;;;;",
			44 => '<tr %1$s><td>%2$s</td><td>%3$s</td><td>%4$s</td><td class="right aligned">%5$s</td><td class="right aligned">%6$s</td><td class="right aligned">%7$s</td></tr>', //7
			45 => '<tr %1$s><td>%2$s</td><td>%3$s</td><td>%4$s</td><td>%5$s</td><td>%6$s</td><td>%7$s</td></tr>', //7
		);
		
		switch ( $func ) {
			case 1:  //open type
				$returnvalue = sprintf($layOut[45], '', ($test) ? '1' : '', '', 
					'<strong>' . $this->language ['type'][$this->cal ['ref0']] . '</strong>', '', '', '');
				if ($set['pdf']) {
					$pdf_line = sprintf($layOut[1],'', 
						$this->language ['type'][$this->cal ['ref0']], '', '', '', '');
					$this->pdf_data[] = explode(';', trim($pdf_line));
				}
				break;
			case 10:  //rekening nummer 
				$returnvalue = sprintf($layOut[44], '', ($test) ? '10' : '',
					$this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{SCHEMA}'),
					$this->cal ['name3'], 
					($this->cal ['budget3'] != 0 && !$set['det'] ) 
						? $this->gsm_sanitizeStringS ($this->cal ['budget3'], 's{KOMMA}')
						: '',
					$this->gsm_sanitizeStringS ($this->cal ['cum_rek'], 's{KOMMA|EURO}'), '-', '-', '-');
				if ($set['pdf']) {
					$pdf_line = sprintf($layOut[1], 
						$this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{SCHEMA}'),
						$this->cal ['name3'], '', '', 
						$this->gsm_sanitizeStringS ($this->cal ['cum_rek'], 's{KOMMA}'), '');
					$this->pdf_data[] = explode(';', trim($pdf_line));
				}
				break;
			case 11: //groep
				if ( $this->cal ['budget2'] != 0 ) $pct_hulp = '---';
				if ( $this->cal ['budget2'] != 0 && $this->cal ['cum_group'] != 0) $pct_hulp = $this->gsm_sanitizeStringS ($this->cal ['cum_group'] / $this->cal ['budget2'] *100 , 's{WHOLE}');
				$returnvalue = sprintf($layOut[44], '', ($test) ? '11' : '',
					$this->gsm_sanitizeStringS ( $this->cal ['ref2'] , 's{WHOLE}'), 
					$this->cal ['name2'], 
					($this->cal ['budget2'] != 0 && !$set['det'] ) 
						?  $this->gsm_sanitizeStringS ($this->cal ['budget2'], 's{KOMMA}')
						:'',
					($this->cal ['budget'] != 0 && !$set['det'] ) 
						? $pct_hulp
						: '',
					$this->gsm_sanitizeStringS ($this->cal ['cum_group'], 's{KOMMA|EURO}'), '', '', '');
				if ($set['det']) $returnvalue .= $layOut[2];
				if ($set['pdf']) {
					$pdf_line = sprintf($layOut[1],
						$this->gsm_sanitizeStringS ( $this->cal ['ref2'] , 's{WHOLE}'),
						$this->cal ['name2'], '', '', '',
						$this->gsm_sanitizeStringS ($this->cal ['cum_group'], 's{KOMMA}'));
					$this->pdf_data[] = explode(';', trim($pdf_line));
					if ($set['det']) $this->pdf_data[] = explode(';', trim($layOut[3]));
				}
				break;
			case 12: //klasse
				$returnvalue = sprintf($layOut[44], '', ($test) ? '12' : '', '',
					"<strong>".$this->language ['grootboek'][$this->cal ['ref1']]."</strong>", '', '',
					"<strong>".$this->gsm_sanitizeStringS ($this->cal ['cum_klas'], 's{KOMMA|EURO}')."</strong>", '', '', '');
				$returnvalue .= $layOut[2];
				if ($set['pdf']) {
					$pdf_line = sprintf($layOut[1], '',
						$this->language ['grootboek'][$this->cal ['ref1']], '', '', '',
						$this->gsm_sanitizeStringS ($this->cal ['cum_klas'], 's{KOMMA}'));
					$this->pdf_data[] = explode(';', trim($pdf_line));
					$this->pdf_data[] = explode(';', trim($layOut[3]));
				}
				break;
			case 13: //type 
				$returnvalue = $layOut[2];
				$returnvalue .= sprintf($layOut[44], '', ($test) ? '13' : '', '',
					($set['func'] == 1)
						? "<strong>".$this->language ['grootboek'][11]."</strong>"
						: "<strong>".$this->language ['grootboek'][13]."</strong>",
					'', '',
					"<strong>".$this->gsm_sanitizeStringS ($this->cal ['cum_type'], 's{KOMMA|EURO}')."</strong>", '', '', '');
				$returnvalue .= $layOut[4];
				if ($set['pdf']) {
					$pdf_line = sprintf($layOut[1], '',
						($set['func'] == 1)
							? $this->language ['grootboek'][11]
							: $this->language ['grootboek'][13], '', '', '', 
						$this->gsm_sanitizeStringS ($this->cal ['cum_type'], 's{KOMMA|EURT}'));
					$this->pdf_data[] = explode(';', trim($pdf_line));
					$this->pdf_data[] = explode(';', trim($layOut[5]));
				}
				break;
			case 14: //type totaal
				$returnvalue = $layOut[2];
				$returnvalue .= sprintf($layOut[44], '', ($test) ? '14' : '', '',
					($set['func'] == 1)
						? "<strong>".$this->language ['grootboek'][12]."</strong>"
						: "<strong>".$this->language ['grootboek'][14]."</strong>",
					'', '',
					"<strong>".$this->gsm_sanitizeStringS ($this->cal ['cum_totaal'], 's{KOMMA|EURO}')."</strong>", '', '', '');
				$returnvalue .= $layOut[4];
				if ($set['pdf']) {
					$this->pdf_data[] = explode(';', trim($layOut[3]));
					$pdf_line = sprintf($layOut[1],'',
						($set['func'] == 1) 
							? $this->language ['grootboek'][12]
							: $this->language ['grootboek'][14],
						'', '', '',
						$this->gsm_sanitizeStringS ($this->cal ['cum_totaal'], 's{KOMMA|EURT}'));
					$this->pdf_data[] = explode(';', trim($pdf_line));
					$this->pdf_data[] = explode(';', trim($layOut[5]));
				}
				break;
			case 15: // en resultaat
				$returnvalue = $layOut[2];
				$returnvalue .= sprintf($layOut[44], '', ($test) ? '14' : '', '',
					"<strong>".$this->language ['grootboek'][15]."</strong>", '', '',
					"<strong>".$this->gsm_sanitizeStringS ($this->cal ['cum_resultaat'], 's{KOMMA|EURO}')."</strong>", '', '', '');
				$returnvalue .= $layOut[2];
				if ($set['pdf']) {
					$pdf_line = sprintf($layOut[1],'', 
						$this->language ['grootboek'][15],
						'','','',					
						$this->gsm_sanitizeStringS ($this->cal ['cum_resultaat'], 's{KOMMA|EURT}'));
					$this->pdf_data[] = explode(';', trim($pdf_line));
				}
				break;
			case 20: // begin balans
				$returnvalue = $layOut[2];
				$returnvalue .= sprintf($layOut[44], '', ($test) ? '20' : '', '',
					''. ' - ' . $this->language ['TXT_ACC'][1] . " (". $this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{SCHEMA}').")", '--', 
					$this->gsm_sanitizeStringS ($this->cal ['cum_rek'], 's{KOMMA}'), '', '', '');
				if ($set['pdf']) {
					$pdf_line = sprintf($layOut[1],'', 
						'' . ' - ' . $this->language ['TXT_ACC'][1] . " ". $this->gsm_sanitizeStringS ( $this->cal ['ref3'] , 's{SCHEMA}'), '', '--',
						$this->gsm_sanitizeStringS ( $this->cal ['cum_rek'], 's{KOMMA}'), '');
					$this->pdf_data[] = explode(';', trim($pdf_line));
				}
				break;
			case 21: // detail regel
				$LocalHulp = $this->cal [ 'book_date' ]  . ' - ' . $this->cal [ 'book_project' ] . $this->cal [ 'book_name' ];
				if ( strlen ( $LocalHulp) > 55 ) $LocalHulp = substr ( $LocalHulp, 0, 53) . "...";
				$returnvalue = sprintf($layOut[44], '', ($test) ? '21' : '', "(".$this->cal ['book_linkid'].")",
					$this->cal [ 'book_date' ]. ' - ' .  $this->cal ['book_project'] . $this->cal ['book_name'], 
					$this->gsm_sanitizeStringS ($this->cal ['dit_bedrag'], 's{KOMMA}'), 
					$this->gsm_sanitizeStringS ($this->cal ['cum_rek'], 's{KOMMA}'), '', '', '');
				if ($set['pdf']) {
					$pdf_line = sprintf($layOut[1],"(".$this->cal ['book_id'].")", 
						$LocalHulp, '', 
						$this->gsm_sanitizeStringS ($this->cal ['dit_bedrag'], 's{KOMMA}'),
						$this->gsm_sanitizeStringS ( $this->cal ['cum_rek'], 's{KOMMA}'), '');
					$this->pdf_data[] = explode(';', trim($pdf_line));
				}
				break;	
			default:
				$returnvalue = "";
				break;
		}
/* debug * / Gsm_debug (array ("cal"=>$this->cal, "func"=>$func, $set), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */

		return $returnvalue;
	} 

	public function gsm_rekeningMoveAttB (
		// ============================
		$func=1, 
		$year="", 
		$project="", 
		$dir_to="", 
		$dir_from="" 
		// ============================
		) { 
		// func=1 aanpassen data aan bookings file en owner en jaarselectie
		// func=2 move alles	
		global $database;
		$this->version [ __FUNCTION__ ] = "20230919";  ;
		$oke = true;
		$returnvalue ='';
		if  ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ("func"=>$func, "year"=>$year, "project"=>$project, "to"=> $dir_to, "from"=> $dir_from), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );
		if ($dir_from == "") {
			$dir_from .= ($func > 1) ? $this->setting ['owner'].$_GET['date'] : "current" ;
		}
		$dir_from = $this->setting ['mediadir'] . "/". $dir_from;
		$this->description .= "From : " .$dir_from.NL;
		$dir_from = LEPTON_PATH. $dir_from;
		if ($dir_to == "") {
			$dir_to .= ($func > 1) ? "current" : $this->setting ['owner'].$_GET['date'] ;
		}
		$dir_to = $this->setting ['mediadir'] . "/". $dir_to;
		$this->description .= "To : " .$dir_to.NL;
		$dir_to = LEPTON_PATH. $dir_to;
		if ( !file_exists( $dir_to . "/" ) ) mkdir( $dir_to, 0777 );  
		if  ( $this->setting [ 'debug' ] == "yes" ) gsm_debug (array ($dir_from, $dir_to), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  

		$date_begin = ($year=="") ? $this->datelow : $year.'-01-01';
		$date_end   = ($year=="") ? $this->datehigh: $year.'-12-31';	
		$fileArray = $this->gsm_scanDir ($dir_from, 2, 10000 );	
		$cnt1=0; $cnt2=0; $cnt3=0;		
		foreach ( $fileArray as $key => $value ) {
			if ($func == 1) {
				$part=explode ("_", $key); 
				$part1= substr($part[0], 2);
				$extention = strtolower( substr( $value, strrpos( $value, '.' ) + 1 ));			
				$query = "SELECT * FROM `" . $this->file_ref [99]  . "`  WHERE `id` = '" . $part1 . "'";
				$r_results = array();  // the output buffer
				if ( $database->execute_query( $query, true, $r_results) && count($r_results) > 0) { 
					$r_row = current($r_results); 
					if ($r_row['booking_date'] >= $date_begin && $r_row['booking_date'] <= $date_end ) {
						$newname= sprintf ("%s%s_%s%s_%s_%s_%s.%s",
							$this->setting ['owner'],
							$r_row['id'],
							$r_row['booking_date'],
							strtolower( substr( $this->gsm_guid(), 0, 6 ) ),
							strtolower($r_row['name']),
							strtolower($r_row['ref']),
							strtolower($r_row['content_short']),
							$extention);
						// sanitize the filename  
						$newname = $this->gsm_sanitizeStringS( $newname, 's{FILE|UNDER}');
						if ($value != $dir_to .'/'. $newname) {
							rename ($value ,$dir_to .'/'. $newname);
							$returnvalue  .= ( $dir_from != $dir_to) ?  "verplaatst : ". $newname.NL : "renamed : ". $newname. NL;	
							$cnt1++;
						} else  {
							$returnvalue  .=  "checked : ". $key.NL;
							$cnt2++;
						}
					}
				} else {
					$returnvalue  .=  " not matching : ". $key. NL;
					$cnt3++;
				}
			} else {
				$newname = str_replace($dir_from, "", $value);
				if ($value != $dir_to . $newname) {
					rename ($value ,$dir_to .'/'. $newname);
					$returnvalue  .= "verplaatst : ". $newname.NL ;
					$cnt1++;					
				} else  {
					$returnvalue  .=  "checked : ". $key.NL;	
					$cnt2++;
				}
			}
		}
		$cnt0=$cnt1+$cnt2+$cnt3;
		$returnvalue  .=  ($cnt1>0) ? "attachments processed : ". $cnt0.NL : "";
		$returnvalue  .=  ($cnt1>0) ? "attachments changed : ". $cnt1.NL : "";
		$returnvalue  .=  ($cnt2>0) ? "attachments checked : ". $cnt2.NL : "";
		$returnvalue  .=  ($cnt3>0) ? "attachments not matching : ". $cnt3.NL : "";	
		return $returnvalue ;
	} 

	public function gsm_rekeningOpeningBalB (
		// ============================
		$func=1, 
		$year = "", 
		$project ="" 
		// ============================
		) {	
		global $database;
		$this->version [ __FUNCTION__ ] = "20230919";  
		$returnvalue  ='';
		$oke = false;
		$subtotals = array();
		$year_next=$year+1;
		$upd_arr = array();
		$Arr_local = array(
			'cum_rek' => 0,
			'cum_group1' => 0,
			'cum_group2' => 0); 
		$query = "SELECT * FROM `" . $this->file_ref [98] . "` 
			WHERE `type` = '1B' OR `type` = '2B' ORDER BY `type`, `ref`";
		$r_results = array();  // the output buffer
		if ( $database->execute_query( $query, true, $r_results) && count($r_results) > 0) { 
			foreach ($r_results as $r_row) { 
				$Arr_local['cum_rek'] = $r_row['amtbalans'];
				$query = "SELECT * FROM `" . $this->file_ref [99] . "` WHERE `booking_date` <= '" . $year."-12-31' 
					AND ( `debet_id`= '" . $r_row['id'] . "' OR `tegen1_id`= '" . $r_row['id'] . "' OR  `tegen2_id`= '" . $r_row['id'] . "' ) 
					ORDER BY `booking_date`, `ref`"; 
				$b_results = array();  // the output buffer	
				if ( $database->execute_query( $query, true, $b_results) && count($b_results) > 0) {
					foreach ($b_results as $b_row) {
						if ($b_row['debet_id'] == $r_row['id'])  $Arr_local['cum_rek'] 
							+= $b_row['amt_debet'];
						if ($b_row['tegen1_id'] == $r_row['id']) $Arr_local['cum_rek'] 
							+= $b_row['amt_tegen1'] 
							* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$b_row['debet_id']]]
							* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$b_row['tegen1_id']]]
							* -1;
						if ($b_row['tegen2_id'] == $r_row['id']) $Arr_local['cum_rek'] 
							+= $b_row['amt_tegen2'] 
							* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$b_row['debet_id']]]
							* $this->language ['type_sign'][$this->setting ['rekeningTypeArray'][$b_row['tegen2_id']]]
							* -1;
					}		  
					// opslaan
					$hulpArr = array(
						'date_balans'=> $year_next.'-01-01',
						'amtbalans'=> $Arr_local['cum_rek']);
					$returnvalue .= "balans waarde id (".$r_row[ 'id' ]. ") :" . $Arr_local['cum_rek'].NL;
					$upd_arr[] = "UPDATE `" . $this->file_ref [98] . "` SET " . $this->gsm_accessSql ( $hulpArr, 2 ) . "  WHERE `id`='" . $r_row[ 'id' ] . "'";
				
				}
				if ($r_row ['type'] == "1B" ) $Arr_local['cum_group1'] += $Arr_local['cum_rek'];
				if ($r_row ['type'] == "2B" ) $Arr_local['cum_group2'] += $Arr_local['cum_rek'];
				$Arr_local['cum_rek']= 0;
			}
			// test resultaat
			$this->cal['cum_bal_update'] = (abs($Arr_local['cum_group1'] - $Arr_local['cum_group2'] )<0.001) ? true : false;
			$errors = array( );

			foreach ($upd_arr as $key=>$query) {
				if($this->cal['cum_bal_update']) { 
					$database->simple_query( $query);
				} else { 
					$returnvalue .= __LINE__ . $query.NL;
				}
			}
			if ( isset($errors) && count( $errors ) > 0 ) $admin->print_error( implode( "<br />n", $errors ), 'javascript: history.go(-1);' );
		} else {  $returnvalue .= __LINE__ . " " . $this->language ['SUR_NDATA'].NL; }
		return $returnvalue;
	} 
	
	public function gsm_rekeningUpdateB ( 
		// ============================
		$func = 1, 
		$low = "-", 
		$high = "-", 
		$Rek_id = 0 
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20230921"; 
/* debug * / gsm_debug (array ($func, $low, $high, $Rek_id, $_POST ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
						
		$returnvalue = "";
		switch ( $func ) {
			case 1:  //disconnect
				foreach ($_POST as $key => $value ) { 
					// x id 1
					$localHulpA = explode ("|", $key);
					if ( isset($localHulpA[0]) && $localHulpA[0] == "x" && isset($localHulpA[2]) && $localHulpA[2] == "1") { //disconnect
						$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` WHERE `id` = '" . $localHulpA[1] . "'";
						$result = array ();
						if ( $database->execute_query( $query, true, $result) && count( $result) > 0){
							$row = current ( $result );
							$hulpArr = array('id_root' => $row ['id']);
							$database->build_and_execute (
								"update",
								$this->file_ref [ 99 ],
								$hulpArr, 
								"`id` = '" . $row ['id'] . "'");
							$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language  [ 'DATABASE UPDATE' ]. $localHulpA[1]. NL;
						} else {
							$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language  [ 'TXT_ERROR_DATABASE' ].$key. NL;
						}
					}
				}
				break;
			case 2:  //connect
				foreach ($_POST as $key => $value ) { 
					$localHulpA = explode ("|", $key);
					// y id oldvalue -> newvalue
					if ( isset($localHulpA[0]) && $localHulpA[0] == "y" && isset($localHulpA[2]) && $localHulpA[2] != $value) { 
						$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` WHERE `id_root` = '" . $localHulpA[1] . "'";
						$result = array ();
						if ( $database->execute_query( $query, true, $result) && count( $result) > 0){
							foreach( $result as $row ) {
								$hulpArr = array('id_root' => $value);
								$database->build_and_execute (
									"update",
									$this->file_ref [ 99 ],
									$hulpArr, 
									"`id` = '" . $row ['id'] . "'");
								$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language  [ 'DATABASE UPDATE' ]. $localHulpA[1]. NL;
							}
						} else {
							$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language  [ 'TXT_ERROR_DATABASE' ].$key. NL;
						}
					}
				}
				break;
			case 3:  
				$this->cal = array ( );
				$this->page_content [ 'save_project' ] = ( isset ( $_POST [ 'edit_project' ] ) && strlen ( $_POST [ 'edit_project' ] ) > 2 ) 
					? $_POST [ 'edit_project' ] 
					: "bijl";
				$this->page_content [ 'save_recid' ] = ( isset ( $_POST [ 'edit_recid' ] ) ) 
					? $_POST [ 'edit_recid' ] 
					: "";
				if (isset ( $_POST [ 'edit_name' ] ) ) {
					$temp = explode ( " ", $_POST [ 'edit_name' ] );
					$this->page_content [ 'save_name' ] = (isset ( $temp [ 0 ] ) && strlen ( $temp [ 0 ] ) > 2 ) 
						? $temp [ 0 ] 
						: $this->page_content [ 'save_recid' ];
				}
				foreach ($_POST as $key => $value ) { 
					$localHulpA = explode ("|", $key);
					// v/w id oldvalue -> newvalue Name/project
					// v id oldvalue -> newvalue Project
					if ( isset($localHulpA[0]) && $localHulpA[0] == "v" && isset($localHulpA[2]) && $localHulpA[2] != $value) {
						$this->cal [$localHulpA[1]] ['name'] = $value;
						$temp = explode (" ", $value );
						$this->page_content [ 'save_name' ] = (isset ( $temp [ 0 ] ) && strlen ( $temp [ 0 ] ) > 2 ) 
							? $temp [ 0 ] 
							: $this->page_content [ 'save_recid' ];
					}
					if ( isset($localHulpA[0]) && $localHulpA[0] == "w" && isset($localHulpA[2]) && $localHulpA[2] != $value) {
						$this->cal [$localHulpA[1]] ['ref'] = $value;
						$this->page_content [ 'save_project' ] = ( strlen ( $value ) > 2 ) 
							? $value 
							: "bijl";
					}
				}
				foreach ( $this->cal as $key => $value ) {
					$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` WHERE `id_root` = '" . $key . "'";
					$result = array ();
					$database->execute_query ( $query, true, $result );							
					if ( count ( $result ) > 0 ){
						foreach ($result as $row) {
							$hulpArr = array ();
							foreach ( $value as $pay => $load ) { if ( $row [ $pay ] != $load ) $hulpArr [$pay] = $load;}
							if ( count ( $hulpArr ) >0 ) {
								$database->build_and_execute (
									"update",
									$this->file_ref [ 99 ],
									$hulpArr, 
									"`id` = '" . $row ['id'] . "'");
								$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language  [ 'DATABASE UPDATE' ]. $row ['id']. NL;
							}
						}
					} else {
						$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language  [ 'TXT_ERROR_DATABASE' ] . $key . NL;
					}
				}
				break;
			case 4: 
				$this->cal = array ( );
				$Date_low = ( $low == "-" ) ? $this->page_content [ 'DATELOW' ] : $low;
				$Date_high = ( $high == "-" ) ? $this->page_content [ 'DATEHIGH' ] : $high;
	
				foreach ($_POST as $key => $value ) { 
					$localHulpA = explode ("|", $key);
					// d/p/q/r/s/t/u/ id oldvalue -> newvalue Name
					if ( isset($localHulpA[0]) 
						&& $localHulpA[0] == "d" 
						&& isset($localHulpA[2]) 
						&& $localHulpA[2] != $this->gsm_sanitizeStringD ($value, 'y{' . date ( "Y-m-d", time ( ) ) . ';' . $Date_low . ';' . $Date_high. '}') )
						$this->cal [$localHulpA[1]] ['booking_date'] = $this->gsm_sanitizeStringD ($value, 'y{' . date ( "Y-m-d", time ( ) ) . ';' . $Date_low . ';' . $Date_high. '}');
					if ( isset($localHulpA[0]) && $localHulpA[0] == "p" && isset($localHulpA[2]) && $localHulpA[2] != $value) 
						$this->cal [$localHulpA[1]] ['debet_id'] = is_numeric ($value) ? $value : 0 ;
					if ( isset($localHulpA[0]) && $localHulpA[0] == "q" && isset($localHulpA[2]) && $localHulpA[2] != $value) {
						$this->cal [$localHulpA[1]] ['tegen1_id'] = is_numeric ($value) ? $value : 0 ;
					}
					if ( isset($localHulpA[0]) && $localHulpA[0] == "r" && isset($localHulpA[2]) && $localHulpA[2] != $value) 
						$this->cal [$localHulpA[1]] ['tegen2_id'] = is_numeric ($value) ? $value : 0 ;
					if ( isset($localHulpA[0]) && $localHulpA[0] == "s" && isset($localHulpA[2]) && $this->gsm_sanitizeStringV ($localHulpA[2], "v{0;-100000;1000000}") != $this->gsm_sanitizeStringv ($value, "v{0;-100000;1000000}")) 
						$this->cal [$localHulpA[1]] ['amt_debet'] = $this->gsm_sanitizeStringV ($value, "v{0;-100000;1000000}");
					if ( isset($localHulpA[0]) && $localHulpA[0] == "t" && isset($localHulpA[2]) && $this->gsm_sanitizeStringV ($localHulpA[2], "v{0;-100000;1000000}") != $this->gsm_sanitizeStringv ($value, "v{0;-100000;1000000}")) 
						$this->cal [$localHulpA[1]] ['amt_tegen2'] = $this->gsm_sanitizeStringV ($value, "v{0;-100000;1000000}");
					if ( isset($localHulpA[0]) && $localHulpA[0] == "u" && isset($localHulpA[2]) && $localHulpA[2] != $value) 
						$this->cal [$localHulpA[1]] ['content_short'] = $value;
				}
				$check1Arr = $this->gsm_preloadDataB ( 'b{REKALN}', array ( 0=>0.00) ) ;
				foreach ($this->cal as $key => $value) {
					$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` WHERE `id` = '" . $key . "'";
					$result = array ();
					$database->execute_query( $query, true, $result);
					if ( count( $result) > 0){
						$row = current ( $result);
						$hulpArr = array ();
						foreach ( $value as $pay => $load ) {
							if ($row [ $pay ] != $load) {
								$row [ $pay ] = $load;
								$hulpArr [$pay] = $load;
							}
						}

						// zorgen dat redundant data (rekening nummer) juist is 
						if ( $row [ 'debet_rekening'] != $check1Arr [ $row [ 'debet_id' ] ] ) { 
							$row [ 'debet_rekening'] = $check1Arr [ $row ['debet_id' ] ]; 
							$hulpArr [ 'debet_rekening'] = $row [ 'debet_rekening']; 
						}
						if ( $row [ 'tegen1_rekening'] != $check1Arr [ $row [ 'tegen1_id' ] ] ) { 
							$row [ 'tegen1_rekening'] = $check1Arr [ $row ['tegen1_id' ] ]; 
							$hulpArr [ 'tegen1_rekening'] = $row [ 'tegen1_rekening']; 
						}
						if ( $row [ 'tegen2_rekening'] != $check1Arr [ $row [ 'tegen2_id' ] ] ) { 
							$row [ 'tegen2_rekening'] = $check1Arr [ $row ['tegen2_id' ] ]; 
							$hulpArr [ 'tegen2_rekening'] = $row [ 'tegen2_rekening']; 
						}
						// correctie som incorrect
						$LocalHulp = $row [ 'amt_debet'] - $row [ 'amt_tegen2' ];
						if ( abs ( $row ['amt_tegen1' ] - $LocalHulp ) > 0.002 ) {
							$row [ 'amt_tegen1'] = $LocalHulp; 
							$hulpArr [ 'amt_tegen1'] = $row [ 'amt_tegen1']; 
						}
						// zoek correct ??
						$arout = ( isset ( $this->language  [ 'SETzoek' ][ 'booking' ] ) ) ? $this->language  [ 'SETzoek' ][ 'booking' ] : "|id|name|";
						foreach ( $row as $pay => $load ) $arout = str_replace ( "|".$pay."|", "|".$load."|", $arout );
						$arout = str_replace ( "||", "|", $arout );
						$arout = strtolower($this->gsm_sanitizeStringS ( $arout, 's{TOASC}' ) ); 
						if ( $row [ 'zoek' ] != $arout ) {
							$row [ 'zoek' ] = $arout;
							$hulpArr ['zoek'] = $row ['zoek'];
						}
						if (isset ( $row [ 'id_root' ] ) && $row [ 'id_root' ] < 1 ) {
							$row [ 'id_root' ] = $row [ 'id' ];
							$hulpArr [ 'id_root' ] = $row [ 'id_root' ];
						}
						if (count ( $hulpArr ) >0 ) {
							$database->build_and_execute (
								"update",
								$this->file_ref [ 99 ],
								$hulpArr, 
								"`id` = '" . $row ['id'] . "'");
							$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language [ 'DATABASE UPDATE' ] . $row ['id']. NL;
						}
					} else {
						$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language [ 'TXT_ERROR_DATABASE' ] . $key. NL;
					}
				}
				break;
			case 5:  //duplicate 
				$Date_high = ( $high == "-" ) ? $this->page_content [ 'DATEHIGH' ] : $high;
				$hulp = array(); //groepjes eruithalen
				if ( isset ( $_POST [ 'vink' ] ) ) {
					foreach ($_POST[ 'vink' ] as $key => $value ) {
						$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "`WHERE `id`='" . $value . "'";
						$results=array();
						if ( $database->execute_query( $query, true, $results, true) && count($results) > 0){
							$row = current ( $results );
							$hulp [ $row [ 'id_root' ] ] = $row [ 'id' ];
						}
					}
				} 
				foreach ($hulp as $key => $value) {
					$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "`WHERE `id_root`='" . $key . "'";
					$results=array();
					if ( $database->execute_query( $query, true, $results, true) && count($results) > 0){
						foreach ( $results as $row) {
							// hoofdrecord van een groepje weg te schrijven
							if ($row[ 'id' ] == $row[ 'id_root' ]) {
								// hoofdrecord van een groepje weg te schrijven
								$unique = strtolower( substr( $this->gsm_guid(), 0, 6 ) );
								$hulpArr = array(
									'name' => $row['name'],
									'booking_date' => $Date_high,
									'ref' => $row['ref' ], 
									'amt_debet' => $row['amt_debet'], 
									'debet_id' => $row['debet_id'],
									'debet_rekening' => $row['debet_rekening'],
									'amt_tegen1' => $row['amt_tegen1'],
									'tegen1_id' => $row['tegen1_id' ], 
									'tegen1_rekening' => $row['tegen1_rekening'],
									'amt_tegen2' => $row['amt_tegen2'],
									'tegen2_id' => $row['tegen2_id'],
									'tegen2_rekening' => $row['tegen2_rekening'],
									'content_short' => $unique);
								$database->build_and_execute (
									"insert",
									$this->file_ref [ 99 ],
									$hulpArr );
								// get id
								$result = array();
								$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` WHERE `content_short` = '" . $unique . "' ORDER BY `updated` DESC LIMIT 1";
								if ( $database->execute_query( $query, true, $result) && count( $result) > 0){
									$rowc = current ( $result );
									// update root bookstuk remove
									$saved_root = $rowc ['id'];
									$hulpArr = array('id_root' => $rowc ['id'], 'content_short' => '');
									// update zoek correct 
									$arout = ( isset ( $this->language  [ 'SETzoek' ][ 'booking' ] ) ) ? $this->language  [ 'SETzoek' ][ 'booking' ] : "|id|name|";
									foreach ( $rowc as $pay => $load ) $arout = str_replace ( "|".$pay."|", "|".$load."|", $arout );
									$arout = str_replace ( "||", "|", $arout );
									$arout = strtolower($this->gsm_sanitizeStringS ( $arout, 's{TOASC}' ) ); 
									$hulpArr ['zoek'] = $arout;
									$database->build_and_execute (
										"update",
										$this->file_ref [ 99 ],
										$hulpArr, 
										"`id` = '" . $rowc ['id'] . "'");
								}
							}
						}
						foreach ( $results as $row) {
							if ($row[ 'id' ] != $row[ 'id_root' ]) {
								// nevenrecords wegschrijven
								$hulpArr = array(
									'name' => $row['name'],
									'booking_date' => $Date_high,
									'ref' => $row['ref' ], 
									'amt_debet' => $row['amt_debet'], 
									'id_root' => (isset ( $saved_root )) ? $saved_root : 0,
									'debet_id' => $row['debet_id'],
									'debet_rekening' => $row['debet_rekening'],
									'amt_tegen1' => $row['amt_tegen1'],
									'tegen1_id' => $row['tegen1_id' ], 
									'tegen1_rekening' => $row['tegen1_rekening'],
									'amt_tegen2' => $row['amt_tegen2'],
									'tegen2_id' => $row['tegen2_id'],
									'tegen2_rekening' => $row['tegen2_rekening'],
									'content_short' => '');
								$arout = ( isset ( $this->language  [ 'SETzoek' ][ 'booking' ] ) ) ? $this->language  [ 'SETzoek' ][ 'booking' ] : "|id|name|";
								foreach ( $hulpArr as $pay => $load ) $arout = str_replace ( "|".$pay."|", "|".$load."|", $arout );
								$arout = str_replace ( "||", "|", $arout );
								$arout = strtolower($this->gsm_sanitizeStringS ( $arout, 's{TOASC}' ) ); 
								$hulpArr ['zoek'] = $arout;
								$database->build_and_execute (
									"insert",
									$this->file_ref [ 99 ],
									$hulpArr );
							}
						}
					} else {
						$returnvalue .= date ( "H:i:s " ) . __LINE__ . " er gaat iets fout".NL;
					}
				}
				break;
			case 6:  //date change
				$Date_low = ( $low == "-" ) ? $this->page_content [ 'DATELOW' ] : $low;
				$Date_high = ( $high == "-" ) ? $this->page_content [ 'DATEHIGH' ] : $high;
				foreach ($_POST as $key => $value ) { 
					$localHulpA = explode ("|", $key);
						if ( isset($localHulpA[0]) && $localHulpA[0] == "a" && isset($localHulpA[2]) && $localHulpA[2] != $value) { 
						$localHulpB = $this->gsm_sanitizeStringD ($value, 'y{' . $Date_low . ';' . $Date_low . ';' . $Date_high. '}');
						$hulpArr = array ('booking_date' => $localHulpB);
						$database->build_and_execute (
							"update",
							$this->file_ref [ 99 ],
							$hulpArr,
							"`id`='" . $localHulpA[1] . "'");
						$returnvalue .= date ( "H:i:s " ) . __LINE__ . " " . $localHulpA[2].' => '.$localHulpB.' Boekdatum aangepast'.NL; 
					}
				}
				break;
			case 7:  //amount change
				foreach ($_POST as $key => $value ) { 
					$localHulpA = explode ("|", $key);
					if ( isset($localHulpA[0]) && $localHulpA[0] == "b" && isset($localHulpA[2]) && $localHulpA[2] != $value) { 
						$localHulpB = $this->gsm_sanitizeStringv ($value, 'v{0;-1000000;1000000}');
						$results = array ();
						$database->execute_query( 
							sprintf ("SELECT * FROM `%s`WHERE `id`='%s'", $this->file_ref [ 99 ],$localHulpA[1]), 
							true, 
							$results); 
						if ( count($results) > 0){
							$row = current ($results);
							$ratio = 1;
							if ($row ['amt_debet'] != 0 )
							$ratio = $this->gsm_sanitizeStringv ($localHulpA[2], 'v{0;-1000000;1000000}') / $row ['amt_debet'];
							$hulpArr = array ();
							if ($row['debet_id'] == $Rek_id ) {
								$hulpArr['amt_debet'] = $localHulpB;
								$hulpArr['amt_tegen1'] = $localHulpB;
							} elseif ($row['tegen1_id'] ==  $Rek_id ) {
								$hulpArr['amt_debet'] = $localHulpB * $ratio;
								$hulpArr['amt_tegen1'] = $localHulpB * $ratio;
							}
							$database->build_and_execute (
								"update",
								$this->file_ref [ 99 ],
								$hulpArr,
								"`id`='" . $localHulpA[1] . "'");
							$returnvalue .= $localHulpA[2].' => '.$localHulpB.' Bedrag aangepast'.NL; 
						}
					}
				}
				break;
			case 8:  //duplicate en edit
				$BoekstukSave = "";
				if (isset ( $_POST [ 'edit_name' ] ) 
					&& isset ( $_POST [ 'edit_recid' ] )
					&& isset ( $_POST [ 'edit_project' ] ) ) {
					$Date_low = ( $low == "-" ) ? $this->page_content [ 'DATELOW' ] : $low;
					$Date_high = ( $high == "-" ) ? $this->page_content [ 'DATEHIGH' ] : $high;
					$recarr = array();
					foreach ($_POST as $key => $value ) { 
						$localHulpA = explode ("|", $key);  
						// zijn er meerdere records gewijzigd dan in de array
						if ( isset( $localHulpA[1] ) && isset ( $localHulpA[2] ) && is_numeric ($localHulpA[1]) && $localHulpA[2] != $value) $recarr [$localHulpA[1]] = $localHulpA[1];
					}
					foreach ( $recarr as $pay => $load) {
						$unique = strtolower ( substr( $this->gsm_guid(), 0, 6 ) );
						$hulpArr = array(
							'name' => $_POST [ 'edit_name' ],
							'ref' =>  $_POST [ 'edit_project' ],
							'content_short' => $unique);
						foreach ($_POST as $key => $value ) { 
							$localHulpA = explode ("|", $key);  // d/p/q/r/s/t/u/ id oldvalue -> newvalue Name
							// zijn er meerdere records gewijzigd dan in de array
							if ( isset($localHulpA[0]) && $localHulpA[0] == "d" )
								$hulpArr ['booking_date' ] = $this->gsm_sanitizeStringD ($value, 'y{' . date ( "Y-m-d", time ( ) ) . ';' . $Date_low . ';' . $Date_high. '}');
							if ( isset($localHulpA[0]) && $localHulpA[0] == "p" )
								$hulpArr ['debet_id'] = is_numeric ($value) ? $value : 0 ;
							if ( isset($localHulpA[0]) && $localHulpA[0] == "q" )
								$hulpArr ['tegen1_id'] = is_numeric ($value) ? $value : 0 ;
							if ( isset($localHulpA[0]) && $localHulpA[0] == "r" ) 
								$hulpArr  ['tegen2_id'] = is_numeric ($value) ? $value : 0 ;
							if ( isset($localHulpA[0]) && $localHulpA[0] == "s" ) 
								$hulpArr  ['amt_debet'] = $this->gsm_sanitizeStringv ($value, "v{0;-100000;1000000}");
							if ( isset($localHulpA[0]) && $localHulpA[0] == "t" ) 
								$hulpArr ['amt_tegen2'] = $this->gsm_sanitizeStringv ($value, "v{0;-100000;1000000}");
							if ( isset($localHulpA[0]) && $localHulpA[0] == "u" ) $BoekstukSave = $value;
							if ( isset($localHulpA[0]) && $localHulpA[0] == "v" ) $hulpArr ['name'] = $value;
							if ( isset($localHulpA[0]) && $localHulpA[0] == "w" ) $hulpArr ['ref'] = $value;
						}
						$database->build_and_execute (
							"insert",
							$this->file_ref [ 99 ],
							$hulpArr );
						$check1Arr = $this->gsm_preloadDataB ( 'b{REKALN}', array ( 0=>0.00) ) ;
						$result = array();
						$query = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` WHERE `content_short` = '" . $unique . "' ORDER BY `updated` DESC LIMIT 1";
						$database->execute_query( $query, true, $result);
						if ( count( $result) > 0){
							$rowc = current ( $result );
							$hulpArr = array('id_root' => $rowc ['id'], 'content_short' => $BoekstukSave);
							// zorgen dat redundant data (rekening nummer) juist is 
							if ( $rowc [ 'debet_rekening'] != $check1Arr [ $rowc [ 'debet_id' ] ] ) { 
								$rowc [ 'debet_rekening'] = $check1Arr [ $rowc ['debet_id' ] ]; 
								$hulpArr [ 'debet_rekening'] = $rowc [ 'debet_rekening']; 
							}
							if ( $rowc [ 'tegen1_rekening'] != $check1Arr [ $rowc [ 'tegen1_id' ] ] ) { 
								$rowc [ 'tegen1_rekening'] = $check1Arr [ $rowc ['tegen1_id' ] ]; 
								$hulpArr [ 'tegen1_rekening'] = $rowc [ 'tegen1_rekening']; 
							}
							if ( $rowc [ 'tegen2_rekening'] != $check1Arr [ $rowc [ 'tegen2_id' ] ] ) { 
								$rowc [ 'tegen2_rekening'] = $check1Arr [ $rowc ['tegen2_id' ] ]; 
								$hulpArr [ 'tegen2_rekening'] = $rowc [ 'tegen2_rekening']; 
							}
							// correctie som incorrect
							$LocalHulp = $rowc [ 'amt_debet'] - $rowc [ 'amt_tegen2' ];
							if ( abs ( $rowc ['amt_tegen1' ] - $LocalHulp ) > 0.002 ) {
								$rowc [ 'amt_tegen1'] = $LocalHulp; 
								$hulpArr [ 'amt_tegen1'] = $rowc [ 'amt_tegen1']; 
							}
							// update zoek correct 
							$arout = ( isset ( $this->language  [ 'SETzoek' ][ 'booking' ] ) ) ? $this->language  [ 'SETzoek' ][ 'booking' ] : "|id|name|";
							foreach ( $rowc as $pay => $load ) $arout = str_replace ( "|".$pay."|", "|".$load."|", $arout );
							$arout = str_replace ( "||", "|", $arout );
							$arout = strtolower($this->gsm_sanitizeStringS ( $arout, 's{TOASC}' ) ); 
							$hulpArr ['zoek'] = $arout;
							$this->recid = $rowc ['id'];
							$database->build_and_execute (
								"update",
								$this->file_ref [ 99 ],
								$hulpArr, 
								"`id` = '" . $rowc ['id'] . "'");
						}
						$returnvalue .= date ( "H:i:s " ) . __LINE__ . $this->language [ 'DATABASE UPDATE' ] . $rowc ['id']. NL;
					}
				}
				break;
		}
		return $returnvalue;
	 } 
 
	public function gsm_A_scanCastor ( 
		// ============================
		$prefix, 
		$project 
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "202300811";
		$temp = '';
		/* check  precense of castor file */
		$results = array ( );
		$database->execute_query ( sprintf ("SHOW TABLE STATUS LIKE '%s'", LOAD_DBBASE . '_castor' ) , 
			true, 
			$results );
		if ( count ( $results ) == 0 ) return $temp; // not existing exit
		
		$query = "SELECT `id`, `name`, `area`, `type`, `ref`, `location`, `filetype`, `keywords`, `date`, `hash` 	FROM `" . LOAD_DBBASE . '_castor' . "`";
		$query .= " WHERE `type` = '".$prefix."' AND `ref` = '".$project."'";
		$query .= " ORDER BY `date`";
		$results = array ( ); // the output buffer
		$database->execute_query ( $query, 
			true, 
			$results );
		if ( count ( $results ) > 0 ) { 
			$nodisplay = true; $height = " "; $prev = ''; $clear ="."; $full = false; if ( $this->setting [ 'jpgwidth' ] < 401 ) $full = true;
			$temp .= ( isset ( $this->language [ 'layout' ] [ 'castor0' ] ) ) ? $this->language [ 'layout' ] [ 'castor0' ] : '<h1>Achtergrond Informatie</h1>';
			foreach ( $results as $row ) { 
				if ( $row [ 'filetype' ] =="pdf" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL;
					$temp .= '<p><a class="pdf" href="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .'" target="_123">' . $row [ 'date' ] . " " . $row [ 'keywords' ] .'</a></p>';
					$nodisplay=false; $prev = "pdf"; // skip no data message
				 } elseif ( $row [ 'filetype' ] =="zip" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL;
					$temp .= '<p><a class="zip" href="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .'" target="_123">' . $row [ 'date' ] . " " . $row [ 'keywords' ] .'</a></p>';
					$nodisplay=false; $prev = "zip"; // skip no data message
				 } elseif ( $row [ 'filetype' ] =="mp4" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL;
					$temp .= '<p><a class="mp4" href="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .'" target="_123">' . $row [ 'date' ] . " " . $row [ 'keywords' ] .'</a></p>';
					$nodisplay=false; $prev = "mp4"; // skip no data message
				 } elseif ( $row [ 'filetype' ] =="html" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL; 
					ob_start ( );
					$value= LEPTON_PATH . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ];
					include $value;
					$temp .= ob_get_clean ( );
					$temp .= NL.NL;
					$nodisplay=false; $prev = "html"; // skip no data message
				 } elseif ( $row [ 'filetype' ] == "jpg" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL; 
					if ( $full ) { // opt to display full image
						$temp .= '<a href="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .'" target="_123"><img src="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .' " width="'.$this->setting [ 'jpgwidth' ].'" height="'.$height.'" /></a>';
					 } else { // only in displayed size
						$temp .= '<p><img src="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .' " width="'.$this->setting [ 'jpgwidth' ].'" height="'.$height.'" vspace="5"/></p>';
					 } 
					$nodisplay=false; $prev = "jpg"; // skip no data message
				 } 
			 } 
			$temp .= ( isset ( $this->language  [ 'layout' ] [ 'castor9' ] ) ) ? $this->language  [ 'layout' ] [ 'castor9' ] : ' ';
		 } 
/* debug * / Gsm_debug ( array ( $temp  ), __LINE__ . __FUNCTION__ ); /* debug */
		return $temp; 
	 } 	 
	
} //end class
