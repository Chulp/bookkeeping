# 
# Office tools  Database Backup
# Version: tool v20200427  T EN v20200201  T NL v20200125 tbackupsql v20200125  class v20200427 
# Run 20201112_121628
# 

# Drop table lep_mod_go_booking if exists

DROP TABLE IF EXISTS `lep_mod_go_booking`;
# Create table lep_mod_go_booking

CREATE TABLE `lep_mod_go_booking` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `zoek` varchar(255) NOT NULL DEFAULT '',
  `booking_date` date NOT NULL DEFAULT '0000-00-00',
  `debet_id` int(11) NOT NULL DEFAULT '0',
  `debet_rekening` decimal(8,2) NOT NULL DEFAULT '0.00',
  `amt_debet` decimal(9,2) NOT NULL DEFAULT '0.00',
  `tegen1_id` int(11) NOT NULL DEFAULT '0',
  `tegen1_rekening` decimal(8,2) NOT NULL DEFAULT '0.00',
  `amt_tegen1` decimal(9,2) NOT NULL DEFAULT '0.00',
  `tegen2_id` int(11) NOT NULL DEFAULT '0',
  `tegen2_rekening` decimal(8,2) NOT NULL DEFAULT '0.00',
  `amt_tegen2` decimal(9,2) NOT NULL,
  `project` varchar(12) NOT NULL DEFAULT '0',
  `id_root` int(11) NOT NULL DEFAULT '0',
  `boekstuk` varchar(255) NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

# Dump data for lep_mod_go_booking

INSERT INTO `lep_mod_go_booking` (`id`, `name`, `zoek`, `updated`, `booking_date`, `debet_id`, `debet_rekening`, `amt_debet`, `tegen1_id`, `tegen1_rekening`, `amt_tegen1`, `tegen2_id`, `tegen2_rekening`, `amt_tegen2`, `project`, `id_root`, `boekstuk`) VALUES
(1, 'eerste record', '|1|eerste record|1000.00|5600.00|0.00|', '2024-01-26 13:20:58', '2024-01-01', 18, '1000.00', '1.00', 2, '5600.00', '1.00', 0, '0.00', '0.00', 'WEBSITE', 1, 'initieel');