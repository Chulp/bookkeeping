-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 18, 2023 at 10:34 AM
-- Server version: 5.6.51
-- PHP Version: 8.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lepton`
--

-- --------------------------------------------------------

--
-- Table structure for table `lep_mod_go_schema`
--

CREATE TABLE `lep_mod_go_schema` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `zoek` varchar(255) NOT NULL DEFAULT '',
  `rekeningnummer` decimal(8,2) NOT NULL DEFAULT '1000.00',
  `rekening_type` char(3) NOT NULL DEFAULT '2B',
  `active` int(7) NOT NULL DEFAULT '0',
  `amtbalans` decimal(9,2) NOT NULL DEFAULT '0.00',
  `date_balans` date NOT NULL DEFAULT '2022-01-01',
  `amtbudget_a` decimal(9,2) NOT NULL DEFAULT '0.00',
  `amtbudget_b` decimal(9,2) NOT NULL DEFAULT '0.00',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lep_mod_go_schema`
--

INSERT INTO `lep_mod_go_schema` (`id`, `name`, `zoek`, `rekeningnummer`, `rekening_type`, `active`, `amtbalans`, `date_balans`, `amtbudget_a`, `amtbudget_b`, `updated`) VALUES
(1, 'Kas', '1kas5700.00', '5700.00', '1B', 1, '0.00', '2020-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(2, 'Bank R/C', '2bank r/c5600.00', '5600.00', '1B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(3, 'Bank spaar', '3bank spaar5501.00', '5501.00', '1B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(4, 'Oprichtingskosten', '4oprichtingskosten2001.00', '2001.00', '1B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(5, 'Te Ontvangen facturen', '5te ontvangen facturen4100.00', '4100.00', '2B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(6, 'Af te dragen BTW', '6af te dragen btw4510.00', '4510.00', '2B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(7, 'Kosten', '7kosten6020.00', '6020.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(8, 'Kosten Inkoop t.b.v. verkoop', '8kosten inkoop t.b.v. verkoop6040.00', '6040.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(9, 'Promotiekosten', '9promotiekosten6450.00', '6450.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(10, 'Kantoorkosten', '10kantoorkosten6460.00', '6460.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(11, 'IT kosten', '11it kosten6470.00', '6470.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(12, 'Kosten externe diensten', '12kosten externe diensten6480.00', '6480.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(13, 'Bank- en rentekosten', '13bank- en rentekosten6490.00', '6490.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(14, 'Inkomsten uit diensten', '14inkomsten uit diensten7020.00', '7020.00', '4R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(15, 'Inkomsten uit verkoop', '15inkomsten uit verkoop7010.00', '7010.00', '4R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(16, 'Kosten inkoop t.b.v diensten', '16kosten inkoop t.b.v diensten6030.00', '6030.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(17, 'Overige inkomsten', '17overige inkomsten7690.00', '7690.00', '4R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(18, 'Geplaatst kapitaal', '18geplaatst kapitaal1000.00', '1000.00', '2B', 1, '0.00', '2020-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(19, 'Leningen op vaste termijn of annuiteit', '19leningen op vaste termijn of annuiteit4301.00', '4301.00', '2B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(20, 'Reserves', '20reserves1320.00', '1320.00', '2B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(21, 'Belasting op bedrijfsresultaat', '21belasting op bedrijfsresultaat6700.00', '6700.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(22, 'Resultaat verwerking', '22resultaat verwerking6900.00', '6900.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(23, 'Kasgeld', '23kasgeld5604.00', '5604.00', '1B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(24, 'Te Ontvangen betalingen', '24te ontvangen betalingen4001.00', '4001.00', '1B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(25, 'Vennootschapsbelasting', '25vennootschapsbelasting4120.00', '4120.00', '1B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(26, 'Overlopende kosten', '26overlopende kosten4600.00', '4600.00', '2B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(27, 'Geraamde belasting op bedrijfsresultaat', '27geraamde belasting op bedrijfsresultaat6702.00', '6702.00', '3R', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59'),
(28, 'R/C verhouding met Moeder Maatschappij', '28r/c verhouding met moeder maatschappij5601.00', '5601.00', '1B', 1, '0.00', '2019-01-01', '0.00', '0.00', '2023-02-17 22:09:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lep_mod_go_schema`
--
ALTER TABLE `lep_mod_go_schema`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lep_mod_go_schema`
--
ALTER TABLE `lep_mod_go_schema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
