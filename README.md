# gsmoffb / Bookkeeping

 Lepton based bookkeeping system
 
This document is relevant for `Gsmoffb` / `Bookkeeping`  version 7.0.0 and up.

## Download
The released stable `Gsmoffb` / `Bookkeeping` application It is recommended to install/update to the latest available version listed. Older versions may contain bugs, lack newer functions or have security issues. 

## License
`Gsmoffb` / `Bookkeeping` is licensed under the [GNU General Public License (GPL) v3.0](http://www.gnu.org/licenses/gpl-3.0.html). The module is available "as is"

### You are free to:
Share — copy and redistribute the material in any medium or format
Adapt — remix, transform, and build upon the material
for any purpose, even commercially. 

### Under the following terms:
Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

## Pre-conditions
The minimum requirements to get this application running on your LEPTON installation are as follows:

- LEPTON *** 7.0 *** of higher depending on the version
- the module uses Twig support
- the module uses Fomantic
- the taxonomy module installed.

The module is tested in combination with the Office Tegel template

## Installation

This description assumes you have a standard installation of Lepton 7.0. All tests are done exclusively with the TFA during installation selected off (not ticked) *)  

1. download the installation package
2. install the downloaded zip archive via the LEPTON module installer
3. create a page which uses the indicated module
4. start the backend functions setupb/instellingen and optionally activate one of the following functions where needed 

  * d REMOVE 	remove data (ref = weg or keyword = recycle).
  * d IMAGE 	image directory copied from frontend
  * d LOGGING 	empty the logging
  * d DETAIL 	detailed data
  * d INSTALL 	frontend files are created: customized values are overwritten bij default values !!
enter ? for other functions

The function can be started by selecting d_...._ where ... is a number of the function names can be concatenated.

The system will automatically install the file upon the first start of this module. 
 
### the backend functions

- setupa / instellingen	to setup and maintenace of the data

the other functions are to be used as administration functions
 
 - afschrift		changing the keywords for the entries
 - balans	  		transfer data from the media/archive directory to the database
 - schema			maintenance of rekeningschema
 - jaarafsluiting	jaarafsluiting
 
 
  Depending on the contents of the SET file modules are made available it may be needed to modify it to get the required functionality
  
 ### frontend menu

 - afschrift		changing the keywords for the entries
 - balans	  		transfer data from the media/archive directory to the database

the other functions are to be used as administration functions
 
 - schema			maintenance of rekeningschema
 - jaarafsluiting	jaarafsluiting
  
### frontend menu  SET_menu

$FC_SET [ 'SET_function' ] 	= 'setupb|dummy'; 	// backend menu ';
$FC_SET [ 'SET_menu' ] 		= 'dummy'; 	// backend menu ';

// for the administrator and the editor
if ( isset ($_SESSION [ 'GROUPS_ID' ] ) && ( $_SESSION [ 'GROUPS_ID' ] == 1 || 	$_SESSION [ 'GROUPS_ID' ] == 4 ) )  {
	$FC_SET [ 'SET_menu' ] 		= 'afschrift|balans'; /* debug */ 
	$FC_SET [ 'SET_function' ] 	= 'setupb|balans|schema|jaarafsluiting'; /* debug */
}
// on the screen there may appear a module name to select.
$FC_SET [ 'SET_txt_menu' ] [ 'xsetupb' ]		= 'Instellingen';
$FC_SET [ 'SET_txt_menu' ] [ 'xdummy' ]			= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vdummy' ]			= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vschema' ]		= 'Rekeningschema';
$FC_SET [ 'SET_txt_menu' ] [ 'vafschrift' ]		= 'Afschriften';
$FC_SET [ 'SET_txt_menu' ] [ 'vbalans' ]		= 'Resultaat';
$FC_SET [ 'SET_txt_menu' ] [ 'vjaarafsluiting' ]= 'Jaar afsluiting';
$FC_SET [ 'SET_txt_menu' ] [ 'xschema' ]		= 'Rekeningschema';
$FC_SET [ 'SET_txt_menu' ] [ 'xafschrift' ]		= 'Afschriften';
$FC_SET [ 'SET_txt_menu' ] [ 'xbalans' ]		= 'Resultaat';
$FC_SET [ 'SET_txt_menu' ] [ 'xjaarafsluiting' ]= 'Jaar afsluiting';

- castor	the default function to search for documents

the other functions are to be used as administration functions
 
 - edit	  changing the keywords for the entries
 - load	  transfer data from the media/archive directory to the database
 
  Depending on the contents of the SET file modules are made available it may be needed to modify it to get the required functionality
  
### droplets related to functionality of this module 
no droplets
	 
## error messages
 * When started for the first time an error message :*Oeps system not initialised and/or empty database* can be seen. 
 The database tables are to be initialised:-> backend -> setupb 
 The menusystem is installed: -> backend -> setup with parameter d_install detailed_

