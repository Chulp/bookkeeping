<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$this->version ['data1'] = '20240222';	

/* input */
$title = ucfirst ( str_replace ( "_", " ", str_replace ( "onderhoud", "overzicht", $project ) ) );
$keuze = strtolower ( $query ) ;
$sw_plus = false;
$sw_eentje = $this->recid; 
$search_mysql = "";
$prmodea = 8;
$prmodeb = 9;
$prmode = $prmodea;
$Lhulp0 = "Schema overzicht";
$Lhulp1 = "Overzicht schema";

/* elvaluate keywords */
$keuze= str_replace ("print", "", $keuze);
if ( strstr ( $keuze,  'plus' ) ) {
	$sw_plus = true;
	$keuze= str_replace ("plus", "", $keuze);
	$prmode = $prmodeb;
}
/* elvaluate keywords * /
if ( strstr ( strtoupper ( $keuze ),  'all' ) ) {
	$sw_paid = true;
	$keuze= str_replace ("all", "", $keuze);
} elseif ( strstr ( strtoupper ( $keuze ),  'paid' ) ) {
	$sw_paid = true; 
	$keuze= str_replace ("paid", "", $keuze);
} else {
	if ($sw_eentje > 0) {
	} else {
		$Lhulp0 = "Overzicht Contributie openstaand";
		$sw_paid = true; 
	}
} 
/* einde elvaluate keywords */

if ($this->setting [ 'debug' ] == "yes" ) gsm_debug (array (
	'query' => $query,
	'project' => $project,
	'mode' => $prmode,
	'selection' => $selection,
	'func' => $func,
	'run' => $run,
	'this' => $this ), __LINE__ . __FUNCTION__ ); /* debug */ 
	
/* elvaluate query */
if ( strlen ( $keuze ) >1 ) {
	$help = "%" . $keuze . "%";
	$search_mysql .= " WHERE `zoek` LIKE '" . $help . "'";
} 
/* einde elvaluate keywords */	

/* initialise page  */
if ( $this->recid >0 ) { 
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf_cols = array( 10, 40, 120, 0, 0, 0 );
	$pdf_header = array( '', '', '', '', '', '' );
	$pdf_data = array( );
	$pdf_text = '';
	$query  = "SELECT * FROM `" . $this->file_ref [ 99 ] . "`";
	$query  .= " WHERE `id`= '" . $this->recid . "' ";
	$results = array();
	if ( $database->execute_query( $query, true, $results) && count ( $results ) > 0 ) { 
		$result = current ( $results );
		$first = true;
		unset ( $result [ 'zoek' ] );
		if ( in_array ( $prmode, array ( 1,2,3,4,5,6,7,8 ) ) ) unset ( $result [ 'updated' ] );
		foreach ( $result as $pay => $load ) {
			if ( $first ) {
				$Lhulp1 = sprintf ("%s overzicht : %s", ucwords ( $project ) , $result [ 'name' ] ) ;
				$pdf->ChapterTitle ( 1, $Lhulp1 );
				$first = false;
			}
			$pdf_data[ ] = explode( ';', trim( sprintf( " ;%s;%s;%s;%s;%s", 
				$pay, 
				strip_tags( html_entity_decode( $load ) ), 
				'', '', '') ) );
		}
	}
} else { 
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->ChapterTitle( 1, $Lhulp1 );
	$pdf_cols = array( 20, 80, 60, 30, 0, 0 );
	$pdf_header = array( '', '', '', '', '', '' );
	$pdf_data = array( );
	$pdf_text = '';
	//lijst
	$query  = "SELECT * FROM `" . $this->file_ref[ 99 ] . "`";
	$query  .= $search_mysql;
	$query  .= " ORDER BY `type` , `ref`";
	$results = array();
	if ( $database->execute_query( $query, true, $results) && count ( $results ) > 0 ) { 
		foreach ( $results as  $result ) {
			unset ( $result [ 'zoek' ] );
			if ( in_array ( $prmode, array ( 1,2,3,4,5,6,7,8 ) ) ) unset ( $result [ 'updated' ] );
			if ( in_array ( $prmode, array ( 9 ) ) ) {
				foreach ( $result as $pay => $load ) {
					$pdf_data[ ] = explode( ';', trim( sprintf( " ;%s;%s;%s;%s;%s", 
						$pay, 
						strip_tags( html_entity_decode( $load ) ), 
						'', '', '') ) );
				}
				$pdf_data[ ] = explode( ';', trim( sprintf( " ;%s;%s;%s;%s;%s", 
					'', 
					'----', 
					'', 
					'', 
					'') ) );
			}
			if ( in_array ( $prmode, array ( 1,2,3,4,5,6,7,8 ) ) ) {
				if ( isset ( $result [ 'active' ] ) && $result [ 'active' ] >0 ) {
					$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s", 
						$result [ 'ref' ], 
						strip_tags( html_entity_decode( $result [ 'name' ] ) ), 
						$this->language [ 'type' ][$result [ 'type' ]] , 
						$result [ 'active' ], 
						'', '' ) ) );
					}
	
			}
		}
	}
}

$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
$pdf_data = array( );
$pdf_text = '';

// footer
$pdf_text .= "\n\n" . $this->setting [ 'droplet' ] [ LANGUAGE . '0' ];
$pdf_text .= "\n\n" . $this->setting [ 'pdf_filename' ] . "\n";
$pdf_text .= $this->language [ 'pdf' ][0] . str_replace( "_", " ", $run ) . "\n";
if ( strlen( $selection ) > 1 ) $pdf_text .= sprintf ( "Options %s : %s %s"\n"" , $project, $selection, $this->recid);

if ($this->setting [ 'debug' ] == "yes" )
	foreach ($this->version as $key => $value) $pdf_text .= $key . "_" . $value . "\n";

// pdf output
$pdf->ChapterBody( $pdf_text );