<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
/* module id*/ 
$module_name 	= 'vafschrift';
$version		= '20240922';
$project		= "Afschriften afhandeling";
$main_file 		= "booking";
$sub_file 		= "schema";
$default_template = '/afschrift.lte';

/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffb::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;


/* 4 file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref  [ 98 ] = LOAD_DBBASE . "_".$sub_file;

/* 5 settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity" );

/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* 8 Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* 9 extra default values */
$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = false;
$oFC->page_content [ "REFERENCE_ACTIVE2" ]= true;
$oFC->page_content [ 'REFERENCE_ACTIVE3' ] = false;

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 saved values */ 
$oFC->gsm_memorySaved ( ); 

if ($oFC->setting [ 'debug' ] == "yes" )  Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC , $selection ), __LINE__ . __FUNCTION__ ); 

// datelimits
$oFC->page_content [ 'DATEHIGH' ] = ( date ( "Y", time ( ) ) ) . "-12-31";
$oFC->page_content [ 'DATELOW' ] = ( date ( "Y", time ( ) ) )-1 . "-01-01"; // bug correction v20240108';
$oFC->page_content [ 'OUDSTE' ] = ( date ( "Y", $oFC->gsm_preloadDataB ('b{OLDEST}') ) ) . "-01-01";
if ( $oFC->page_content [ 'DATELOW' ] < $oFC->page_content [ 'OUDSTE' ]) $oFC->page_content [ 'DATELOW' ] = $oFC->page_content [ 'OUDSTE' ] ;


if ( LOAD_MODE == "x" ) $oFC->page_content [ 'DATELOW' ]= $oFC->page_content [ 'OUDSTE' ] ;

/* default afschrift rekening nummer */
$oFC->gsm_RekeningAfschriftSelB ( 1, $oFC->setting [ 'rekening' ] ?: "-" );

/* selection */
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `" . $oFC->file_ref [ 99 ] . "` . `zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}
$oFC->page_content [ 'PARAMETER' ] = trim ( $selection );
$oFC->page_content [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content [ 'PARAMETER' ] );

/* some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "Bookingen":
			if ($oFC->sips) {
				$oFC->description .= date ( "H:i:s " ) . __LINE__ . $oFC->language [ 'TXT_ERROR_SIPS' ]. NL;
				break;
			}

			/* attachment name */
			$save_project = ( isset ( $_POST [ 'edit_project' ] ) 
				&& strlen ( $_POST [ 'edit_project' ] ) > 2 ) 
					? $_POST [ 'edit_project' ] 
					: "bijl";
			$save_recid = ( isset ( $_POST [ 'edit_recid' ] ) ) 
				? $_POST [ 'edit_recid' ] 
				: "";
			if (isset ( $_POST [ 'edit_name' ] ) ) {
				$temp = explode ( " ", $_POST [ 'edit_name' ] );
				$save_name = (isset ( $temp [ 0 ] ) 
					&& strlen ( $temp [ 0 ] ) > 2 ) 
						? $temp [ 0 ] 
						: $save_recid;
			}	
			
			/* processing */
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 1 ); //disconnect
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 2 ); //connect
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 3 ); //name/project ->calc
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 4 , $oFC->page_content['SWITCH_DATE'], $oFC->page_content['BOOK_DATE']); // booking ->calx

			// attachment
			$oFC->description .= $oFC->gsm_uploadFile ( $oFC->setting ['mediadir']."/current/", "%s%s%s%s%s", $save_name ?? "-o-", $oFC->page_content [ 'save_recid' ], $save_project ?? "-oo-" );
			$oFC->recid = $_POST [ 'edit_recid' ];
			if ($oFC->recid > 0)	$oFC->page_content["REFERENCE_ACTIVE3"] = true;
			break;
		case "Afschrift":
			if ($oFC->sips) {
				$oFC->description .= date ( "H:i:s " ) . __LINE__ .$oFC->language [ 'TXT_ERROR_SIPS' ]. NL;
				break;
			}
			// duplicate
			if ( isset( $_POST[ 'vink' ] ) ) $oFC->description .= $oFC->gsm_RekeningUpdateB ( 5, "-", $oFC->page_content['BOOK_DATE'] ); //duplicate
				
			/* date change */	
			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 6 , $oFC->page_content [ 'SWITCH_DATE' ], $oFC->page_content [ 'BOOK_DATE'] ); // date change

			$oFC->description .= $oFC->gsm_RekeningUpdateB ( 7 , "-", "-", $oFC->page_content['REKENING_ID'] );
			
			break; 
		default:
/* debug * / Gsm_debug( $_POST, __LINE__ . __FUNCTION__. " post ");  /* debug */
			$oFC->description .= __LINE__ . " post: " . $_POST[ 'command' ] . NL;
			break;
	}
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		case "Select":
			if ($oFC->recid > 0) $oFC->page_content["REFERENCE_ACTIVE3"]= true;
			break;
		case 'Rmfile':
			$LocalHulpC = sprintf ("%s%s/current/%s" , LEPTON_PATH, $oFC->setting [ 'mediadir' ], $_GET[ 'file' ]);
			if ( file_exists ( $LocalHulpC ) ) {
				unlink ( $LocalHulpC );
				$oFC->description .= date ( "H:i:s " ) . __LINE__ . " Removed: " . $_GET[ 'file' ] . NL;
			}
			if ($oFC->recid > 0) $oFC->page_content["REFERENCE_ACTIVE3"]= true;
			break;
		default:
/* debug * / Gsm_debug( $_GET, __LINE__ . __FUNCTION__. " get ");  /* debug */
			$oFC->description .= __LINE__ . " get: " . $_GET[ 'command' ] . NL;
			break;
	}
} else {
	$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = true;
	$oFC->page_content [ 'REFERENCE_ACTIVE2' ] = false;
	$oFC->page_content [ 'REFERENCE_ACTIVE3' ] = false;
}
 
Switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'SELECTION' ] = "";
		$oFC->page_content [ 'TOEGIFT' ] = ""; 
		foreach ($oFC->language [ 'DUMMY' ] as $pay => $load ) $oFC->page_content [ 'TOEGIFT' ] .=  $load . NL; 
		break;
	default: 
		$oFC->page_content [ "REFERENCE_ACTIVE2" ]= true;
		$oFC->page_content [ "AFSCHRIFTEN" ] = $oFC->gsm_RekeningAfschriftDisplB ( 1 );
		
		if ( $oFC->page_content [ "REFERENCE_ACTIVE3" ] ) $oFC->page_content [ "BOOKINGEN" ] = $oFC->gsm_RekeningDisplayBookB ( 1, $oFC->recid );
		break;
} 

/* output processing */
$oFC->page_content [ 'REKENING_ID' ] = $oFC->memory [ 1 ];
$oFC->page_content [ 'BOOK_DATE' ] = $oFC->memory [ 2 ];
$oFC->page_content [ 'START_DATE' ] = $oFC->memory [ 4 ];
$oFC->page_content [ 'BOOK_AMOUNT' ] = $oFC->gsm_sanitizeStrings ( $oFC->memory [ 3 ], "s{KOMMA}" );
$oFC->page_content [ 'VORIG_AMOUNT' ] = $oFC->gsm_sanitizeStrings ( $oFC->memory [ 5 ], "s{KOMMA}" );
/* memory save */
$oFC->page_content [ 'MEMORY' ] = $oFC->gsm_memorySaved ( 3 ); 
/* end save values */

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

/* output processing */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION [ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
//		$template_name = '@'.LOAD_MODULE.LOAD_SUFFIX.'/back.lte';
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>