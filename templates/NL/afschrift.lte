{#
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2017-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 #}
{% autoescape false %}
<div id="top"><button class="ui right floated button" ><a href="#bottom"><i class="caret square down icon"></i>BOTTOM</a></button></div>
<h2 class="ui blue header">
<i class="{{ICON}}"></i>
<div class="content">
	{{FORMULIER}}
	<div class="sub header">{{SUB_HEADER}}</div>
</div></h2>
<div class="container">
	<div class="{{MESSAGE_CLASS}}">{{STATUS_MESSAGE}}</div>
	{{MESSAGE}}
</div>
<div class="ui segment">
	<form class="ui form" name="{{PAGE_ID}}|{{SECTION_ID}}|{{MODE}}|{{OWNER}}" enctype="multipart/form-data" method="post" action="{{RETURN}}">
		<input name="module" type="hidden" value="{{MODULE}}" />
		<input name="timestamp" type="hidden" value="{{DATE}} {{TIME}}"/>
		<input name="sips" type="hidden" value="{{HASH}}" />
		<input name="recid" type="hidden" value="{{RECID}}" />
		<input name="memory" type="hidden" value="{{MEMORY}}" />
		<input name="switch_date" type="hidden" value="{{SWITCH_DATE}}" />
		{{DESCRIPTION}}
			
{% if LOAD == "v" %} <div class="ui styled fluid accordion">{% endif %}
	<div class="{% if REFERENCE_ACTIVE1 == "0,00" %} active {% endif %} title"><i class="dropdown icon"></i>Afschrift parameters.</div>
	<div class="ui large form {% if REFERENCE_ACTIVE1 == "0,00" %} active {% endif %} content">
		<div class="ui stacked secondary segment">
			<div class="inline fields">
				<div class="nine wide field">
					<select id="afdeling" name="gsmc_rekening_id">{{ REKENING_IDS }}</select>
					<label>Rekening</label>
				</div>
				<div class="two wide field"></div>
			</div>
			<div class="inline fields">
				<div class="four wide field">
					<input type="text" name="gsmc_book_date" value="{{ BOOK_DATE }}" placeholder="yyyy-mm-dd" />
				</div>
				<div class="three wide field">
					<label>Boek Datum</label>
				</div>
				<div class="four wide field">
					<input type="text" name="gsmc_book_amount" value="{{ BOOK_AMOUNT }}" placeholder="Bedrag" />
				</div>
				<label>Saldo boekdatum</label>
			</div>
			<div class="inline fields">
				<div class="four wide field">
					<input type="text" name="gsmc_start_date" value="{{ START_DATE }}" placeholder="yyyy-mm-dd" />
				</div>
				<div class="three wide field">
					<label>Begin datum</label>
				</div>
				<div class="four wide field">
					<input type="text" name="gsmc_vorig_amount" value="{{ VORIG_AMOUNT }}" placeholder="Bedrag" />
				</div>
				<label>Vorig Saldo</label>
			</div>
			<div class="ui basic segment">
				<button class="ui primary button" name="command" value="Afschrift" type="submit" tabindex="{{tabindex}}" >
					<i class="file outline icon"></i>
					Verwerken
				</button>
			</div>
		</div>
	</div>
{% if LOAD == "v" %} </div>{% endif %}

{% if REFERENCE_ACTIVE2 %}
{% if LOAD == "v" %} <div class="ui styled fluid accordion">{% endif %}
	<div class="{% if not REFERENCE_ACTIVE3 %} active {% endif %} title"><i class="dropdown icon"></i>Afschriften</div>
	<div class="ui large form {% if not REFERENCE_ACTIVE3 %} active {% endif %} content">
		<div class="ui stacked secondary segment">
			<div class="ui grid">
				<div class="four wide column">Datum</div>
				<div class="five wide column">Omschrijving</div>
				<div class="three wide column right aligned">Mutatie</div>
				<div class="three wide column right aligned">Saldo</div>
				<div class="one wide column"></div>
			</div>
	{% for afschrift in AFSCHRIFTEN %}
		{% if afschrift.switch > 4 %}
{#  #}
			<div class="ui grid">
				<div class="four wide column">
					<input type="text" name="a|{{ afschrift.id }}|{{ afschrift.booking_date }}" value="{{afschrift.booking_date}}" />
				</div>
				<div class="five wide column">
					<a href="{{ RETURN }}&command=Select&module={{ MODULE }}&recid={{ afschrift.id }}" >
						{{ afschrift.ref|upper }} {{ afschrift.name}}
					</a>
				</div>
				<div class="three wide column">
					<input type="text" name="b|{{ afschrift.id }}|{{ afschrift.mutatie }}" value="{{ afschrift.mutatie }}" />
				</div>
				<div class="three wide column right aligned">{{ afschrift.saldo }}</div>
				<div class="one wide column right aligned"><span class="ui tiny red text">{{ afschrift.remain }}</span>4</div>
			</div>
		{% elseif afschrift.switch > 3 %}
{#  #}
			<div class="ui grid">
				<div class="four wide column">
					<input type="text" name="a|{{ afschrift.id }}|{{ afschrift.booking_date }}" value="{{afschrift.booking_date}}" />
				</div>
				<div class="five wide column">
					<a href="{{ RETURN }}&command=Select&module={{ MODULE }}&recid={{ afschrift.id }}" >
						{{ afschrift.ref|upper}} {{ afschrift.name}}
					</a>
				</div>
				<div class="three wide column right aligned">{{ afschrift.mutatie }}</div>
				<div class="three wide column right aligned">{{ afschrift.saldo }}</div>
				<div class="one wide column right aligned"><span class="ui tiny red text">{{ afschrift.remain }}</span>3</div>
			</div>
		{% elseif afschrift.switch > 2 %}
{#  #}
			<div class="ui grid">
				<div class="four wide column">
					{{afschrift.booking_date}}&nbsp;&nbsp;
					<input type="checkbox" name="vink[]" value="{{ afschrift.id }}">
				</div>
			{% if LOAD == "v" %} 
				<div class="five wide column">
						{{ afschrift.ref|upper}} {{ afschrift.name}}

				</div>
			{% else %} 
				<div class="five wide column">
					<a href="{{ RETURN }}&command=Select&module={{ MODULE }}&recid={{ afschrift.id }}" >
						{{ afschrift.ref|upper}} {{ afschrift.name}}
					</a>
				</div>
			{% endif %}
				<div class="three wide column right aligned">{{ afschrift.mutatie }}</div>
				<div class="three wide column right aligned">{{ afschrift.saldo }}</div>
				<div class="one wide column right aligned"><span class="ui tiny red text">{{ afschrift.remain }}</span>2</div>
			</div>
		{% elseif afschrift.switch > 1 %}
{#  #}
			<div class="ui grid">
				<div class="four wide column">
					{{afschrift.booking_date}}
				</div>
				<div class="five wide column">
					<a href="{{ RETURN }}&command=Select&module={{ MODULE }}&recid={{ afschrift.id }}" >
						{{ afschrift.ref|upper }} {{ afschrift.name}}
					</a>
				</div>
				<div class="three wide column right aligned">{{ afschrift.mutatie }}</div>
				<div class="three wide column right aligned">{{ afschrift.saldo }}</div>
				<div class="one wide column right aligned"><span class="ui tiny red text">{{ afschrift.remain }}</span>1</div>
			</div>
{#  #}
		{% else %}
			<div class="ui grid">
				<div class="four wide column">
					{{afschrift.booking_date}}
				</div>
				<div class="five wide column">
					<a href="{{ RETURN }}&command=Select&module={{ MODULE }}&recid={{ afschrift.id }}" >
						{{ afschrift.ref|upper }} {{ afschrift.name}}
					</a>
				</div>
				<div class="three wide column right aligned">{{ afschrift.mutatie }}</div>
				<div class="three wide column right aligned">-</div>
				<div class="one wide column right aligned"><span class="ui tiny red text">-</span></div>
			</div>
		{% endif %}
	{% endfor %}
			<button class="ui primary button" name="command" value="Afschrift" type="submit" >
				<i class="file outline icon"></i>
				Verwerken
			</button>
		</div>
	</div>
{% if LOAD == "v" %} </div>{% endif %}
{% endif %}

{% if REFERENCE_ACTIVE3 %}
{% if LOAD == "v" %} <div class="ui styled fluid accordion">{% endif %}
	<div class="{% if REFERENCE_ACTIVE3 %} active {% endif %} title"><i class="dropdown icon"></i>Boekingen</div>
	<div class="ui large form {% if REFERENCE_ACTIVE3 %} active {% endif %} content">
	{% for booking in BOOKINGEN %}
		{% if loop.first %}
		<input name="edit_recid" type="hidden" value="{{booking.id}}" />
		<input name="edit_name" type="hidden" value="{{booking.name}}" />
		<input name="edit_project" type="hidden" value="{{booking.ref}}" />
		<div class="ui stacked secondary segment">
			<div class="inline fields">
				<div class="ten wide field">
					<input type="text" name="v|{{booking.id }}|{{booking.name}}" value="{{booking.name}}" placeholder=" omschrijving" />
				</div>
				<label>Omschrijving</label>
			</div>
			<div class="inline fields">
				<div class="twelve wide field focus">
					<div class="ui fluid clearable multiple selection dropdown">
						<input type="hidden" name="w|{{booking.id }}|{{booking.ref}}" value="{{ booking.ref }}">
						<i class="dropdown icon"></i>
						<div class="default text">Project</div>
						{{ booking.REF }}
					</div>
					<label>Project</label>
				</div>
			</div>		
		</div>
		{% endif %}
		
		<div class="ui stacked secondary segment">
			<div class="inline fields">
				<div class="five wide field">
					<input type="text" name="d|{{booking.id }}|{{booking.booking_date}}" value="{{booking.booking_date}}" />
				</div>
				<label>Datum </label>
				<div class="four wide field"></div>
			{% if booking.id != booking.id_root %}
				<div class="four wide field">
					<label>joined {{booking.id }} / {{booking.id_root}} </label>
				</div>
				<div class="two wide field">
					<input type="checkbox" name="x|{{booking.id }}|1" >
					<label>Detach</label>

			{% else %}
					<div class="two wide field">
					<label>{{booking.id }} join to </label>
				</div>
				<div class="two wide field">
					<input type="text" name="y|{{booking.id }}|{{booking.id_root}}" value="{{booking.id }}" placeholder="###" />
			{% endif %}
				</div>
				
			</div>
			<div class="inline fields">
				<div class="ten wide field"><label>Rekening</label></div>
				<div class="four wide field"><label>Debet</label></div>
				<div class="two wide field"><label>Credit</label></div>
			</div>
			<div class="inline fields">
				<div class="eight wide field focus">
					<select name="p|{{booking.id }}|{{booking.debet_id}}" >{{ booking.DEBET }}</select>
				</div>
				<div class="four wide field">
					<input type="text" name="s|{{booking.id }}|{{booking.amt_debet}}"  value="{{ booking.amt_debet }}" placeholder="Bedrag" />
				</div>
				<div class="four wide field"></div>
			</div>
			<div class="inline fields">
				<div class="one wide field">aan:</div>
				<div class="seven wide field focus">
					<select name="q|{{booking.id }}|{{booking.tegen1_id}}" >{{ booking.TEGEN1 }}</select>
				</div>
				<div class="four wide field"></div>
				<div class="four wide field">{{booking.amt_tegen1}}</div>
			</div>
			<div class="inline fields">
				<div class="one wide field">aan:</div>
				<div class="seven wide field focus">
					<select name="r|{{booking.id }}|{{booking.tegen2_id}}" >{{ booking.TEGEN2 }}</select>
				</div>
				<div class="four wide field">
					<input type="text" name="t|{{booking.id }}|{{booking.amt_tegen2}}" value="{{ booking.amt_tegen2 }}" placeholder="Bedrag" />
				</div>
			</div>	
			<div class="inline fields">
				<div class="ten wide field">
					<input name="u|{{booking.id }}|{{booking.content_short}}" type="text"  value="{{booking.content_short}}" placeholder="Opmerking" />
				</div>
				<label>Opmerking</label>
			</div>
			<div class="title">Updated : {{booking.updated}} ( rec {{booking.id}} )</div>
		</div>
	{% endfor %}
	
	{% for booking in BOOKINGEN %}
		{% if loop.first %}
		<div class="ui stacked secondary segment">
			{% for key, item in booking.BIJLAGE %}
			{% if loop.first %}
			<div class="inline fields">
				<div class="ten wide field"><label>Attachments</label></div>
			</div>
			{% endif %}
			<div class="inline fields">
				<div class="sixteen wide field">
					<a href="{{ RETURN }}&command=Rmfile&module={{ MODULE }}&recid={{booking.id }}&file={{ key }}" ><i class="trash icon"></i></a>
					<a href="{{ item }}" target="_blank"><i class="download icon"></i>&nbsp;<strong>{{ key }}</strong></a>
				</div>
			</div>
			{% endfor %}
			<div class="inline fields">
				<div class="twelve wide field">
					<input type="file" name="doc_uploaded" />
				</div>
				<label>Bijlage toevoegen</label>
			</div>
		</div>
		{% endif %}
	{% endfor %}
		<button class="ui primary button" name="command" value="Bookingen" type="submit" >
			<i class="file outline icon"></i>
			Wijzigen
		</button>
	</div>
{% if LOAD == "v" %} </div>{% endif %}
{% endif %}

{#  tail of the form  #}
	</form>
	{{RAPPORTAGE}}
	{{TOEGIFT}}
</div>
<div id="bottom"><button class="ui right floated button" ><a href="#top"><i class="caret square up icon"></i>TOP</a></button></div>
{% endautoescape %}